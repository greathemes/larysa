<?php
/**
 * Functions and definitions.
 */

/**
 * Load the parent and child css files.
 */
function wpstarter_enqueue_styles() {

	// CSS.
	$parent_style = 'wpstarter-styles-min';
	wp_enqueue_style( $parent_style, get_template_directory_uri() . '/styles.min.css' );

	wp_enqueue_style( 'wpstarter-style-child', get_stylesheet_directory_uri() . '/style.css', [ $parent_style ], wp_get_theme()->get( 'Version' ) );

}
