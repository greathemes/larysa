<?php
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * @since 1.0.0
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 * @package wpstarter
 */

if ( ! function_exists( 'wpstarter_setup' ) ) :

	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function wpstarter_setup() {

		// Make theme available for translation.
		load_theme_textdomain( 'TRANSLATE', WPSTARTER_THEME_DIR . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/**
		 * Let WordPress manage the document title.
		 *
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/**
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		add_theme_support( 'woocommerce' );

		add_theme_support( 'responsive-embeds' );

		// Add custom image sizes.
		add_image_size( 'wpstarter_front_page_slider', 1050, 9999 );
		add_image_size( 'wpstarter_front_page_testimonials', 280, 9999 );
		add_image_size( 'wpstarter_post', 1110, 9999 );
		add_image_size( 'wpstarter_post_retina', 2100, 9999 );
		add_image_size( 'wpstarter_widget', 270, 9999 );
		add_image_size( 'wpstarter_avatar', 200, 200, true );
		add_image_size( 'wpstarter_portfolio', 500, 9999 );
		add_image_size( 'wpstarter_portfolio_medium', 700, 9999 );
		add_image_size( 'wpstarter_portfolio_large', 900, 9999 );
		add_image_size( 'wpstarter_portfolio_very_large', 1200, 9999 );
		add_image_size( 'wpstarter_portfolio_pswp', 1500, 9999 );
		add_image_size( 'wpstarter_portfolio_pswp_large', 2200, 9999 );

		// Switch default core markup for search form, comment form, and comments to output valid HTML5.
		add_theme_support( 'html5', [ 'search-form', 'comment-form', 'comment-list', 'gallery', 'caption' ] );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support(
			'custom-logo',
			[
				'height'      => 250,
				'width'       => 250,
				'flex-width'  => true,
				'flex-height' => true,
			]
		);

		/**
		 * Register all menus.
		 */
		register_nav_menus(
			[
				'primary' => esc_html__( 'Primary Navigation', 'TRANSLATE' ),
			]
		);

	}

endif;

add_action( 'after_setup_theme', 'wpstarter_setup' );

if ( ! function_exists( 'wpstarter_modify_max_srcset_image_width' ) ) :

	/**
	 * Modifies the maximum width of the srcset attribute.
	 *
	 * @param int $max_width Width of the image.
	 */
	function wpstarter_modify_max_srcset_image_width( $max_width ) {

		$max_width = 2200;

		return $max_width;

	}

endif;

add_filter( 'max_srcset_image_width', 'wpstarter_modify_max_srcset_image_width' );

if ( ! function_exists( 'wpstarter_enqueue_styles' ) ) :

	/**
	 * Enqueue styles.
	 */
	function wpstarter_enqueue_styles() {

		wp_enqueue_style( 'wpstarter-style', get_stylesheet_uri(), [], '1.0.0', 'all' );

		wp_enqueue_style( 'wpstarter-styles-min', get_stylesheet_directory_uri() . '/styles.min.css', [], '1.0.0', 'all' );

	}

endif;

add_action( 'wp_enqueue_scripts', 'wpstarter_enqueue_styles' );

if ( ! function_exists( 'wpstarter_enqueue_assets' ) ) :

	/**
	 * Enqueue assets.
	 */
	function wpstarter_enqueue_assets() {

		// JS.
		wp_enqueue_script( 'wpstarter-scripts-min', WPSTARTER_THEME_URI . '/scripts.min.js', [ 'jquery' ], '1.0.0', true );

		// Original snippet from Contact Form 7 plugin (It's all because of combine plugin script with the theme's script to make one request less).
		if ( class_exists( 'WPCF7' ) ) :

			$wpcf7 = array(
				'apiSettings' => array(
					'root' => esc_url_raw( rest_url( 'contact-form-7/v1' ) ),
					'namespace' => 'contact-form-7/v1',
				),
			);

			if ( defined( 'WP_CACHE' ) and WP_CACHE ) {
				$wpcf7['cached'] = 1;
			}

			wp_localize_script( 'wpstarter-scripts-min', 'wpcf7', $wpcf7 );

		endif;

		// Loads extra JS.
		if ( is_singular( 'post' ) && comments_open() && get_option( 'thread_comments' ) ) :
			wp_enqueue_script( 'comment-reply' );
		endif;

	}

endif;

add_action( 'wp_enqueue_scripts', 'wpstarter_enqueue_assets' );

if ( ! function_exists( 'wpstarter_enqueue_admin_assets' ) ) :

	/**
	 * Enqueue admin assets.
	 */
	function wpstarter_enqueue_admin_assets( $hook ) {

		// Media.
		! did_action( 'wp_enqueue_media' ) ? wp_enqueue_media() : '';

		// CSS.
		wp_enqueue_style( 'wpstarter-admin-styles-min', WPSTARTER_THEME_URI . '/admin-styles.min.css', false );

	}

endif;

add_action( 'admin_enqueue_scripts', 'wpstarter_enqueue_admin_assets' );

if ( ! function_exists( 'wpstarter_register_widgets' ) ) :

	/**
	 * Register all widgets.
	 */
	function wpstarter_register_widgets() {

		// Primary Sidebar.
		register_sidebar(
			[
				'name'          => esc_html__( 'Primary Sidebar', 'TRANSLATE' ),
				'id'            => 'primary',
				'description'   => esc_html__( 'Add widgets here.', 'TRANSLATE' ),
				'before_widget' => '<section id="%1$s" class="widget %2$s">',
				'after_widget'  => '</section>',
				'before_title'  => '<h3 class="widget__title"><span>',
				'after_title'   => '</span></h3>',
			]
		);

	}

endif;

add_action( 'widgets_init', 'wpstarter_register_widgets' );

if ( ! function_exists( 'wpstarter_content_width' ) ) :

	/**
	 * Set the content width in pixels, based on the theme's design and stylesheet.
	 *
	 * Priority 0 to make it available to lower priority callbacks.
	 *
	 * @global int $content_width
	 */
	function wpstarter_content_width() {

		$blog = wpstarter_get_options_blog();

		if ( is_page_template( 'templates/post-sidebar-left.php' ) || is_page_template( 'templates/post-sidebar-right.php' ) ) :
			$content_width = 790;
		elseif ( is_singular( 'post' ) ) :
			$content_width = 810;
		elseif ( is_page_template( 'templates/page-sidebar-left.php' ) || is_page_template( 'templates/page-sidebar-right.php' ) || is_page_template( 'templates/page-narrow.php' ) ) :
			$content_width = 810;
		else :
			$content_width = 1110;
		endif;

		$GLOBALS['content_width'] = apply_filters( 'wpstarter_content_width', $content_width );

	}

endif;

add_action( 'template_redirect', 'wpstarter_content_width', 0 );

if ( ! function_exists( 'wpstarter_pingback_header' ) ) :

	/**
	 * Add a pingback url auto-discovery header for single posts, pages, or attachments.
	 */
	function wpstarter_pingback_header() {

		if ( is_singular() && pings_open() ) :

			echo '<link rel="pingback" href="', esc_url( get_bloginfo( 'pingback_url' ) ), '">';

		endif;

	}

endif;

add_action( 'wp_head', 'wpstarter_pingback_header' );

if ( ! function_exists( 'wpstarter_register_recommended_plugins' ) ) :

	/**
	 * TGMPA notices about register recommended plugins.
	 */
	function wpstarter_register_recommended_plugins() {

		$plugins = [
			[
				'name'             => esc_html__( 'Wpstarter Portfolio ( bundled with the theme )', 'TRANSLATE' ), // The plugin name.
				'slug'             => 'wpstarter-portfolio', // The plugin slug (typically the folder name).
				'source'           => get_template_directory() . '/plugins/wpstarter-portfolio.zip', // The plugin source.
				'required'         => true, // If false, the plugin is only 'recommended' instead of required.
				'force_activation' => false,
			],
			[
				'name'             => esc_html__( 'Wpstarter Widgets ( bundled with the theme )', 'TRANSLATE' ), // The plugin name.
				'slug'             => 'wpstarter-widgets', // The plugin slug (typically the folder name).
				'source'           => get_template_directory() . '/plugins/wpstarter-widgets.zip', // The plugin source.
				'required'         => false,
				'force_activation' => false,
			],
			[
				'name'             => esc_html__( 'Wpstarter Author ( bundled with the theme )', 'TRANSLATE' ), // The plugin name.
				'slug'             => 'wpstarter-author', // The plugin slug (typically the folder name).
				'source'           => get_template_directory() . '/plugins/wpstarter-author.zip', // The plugin source.
				'required'         => false,
				'force_activation' => false,
			],
			[
				'name'             => esc_html__( 'Advanced Custom Fields', 'TRANSLATE' ), // The plugin name.
				'slug'             => 'advanced-custom-fields', // The plugin slug (typically the folder name).
				'required'         => true,
				'force_activation' => false,
			],
			[
				'name'             => esc_html__( 'Advanced Custom Fields Gallery', 'TRANSLATE' ), // The plugin name.
				'slug'             => 'navz-photo-gallery', // The plugin slug (typically the folder name).
				'required'         => true,
				'force_activation' => false,
			],
			[
				'name'             => esc_html__( 'Kirki', 'TRANSLATE' ),
				'slug'             => 'kirki',
				'required'         => true,
				'force_activation' => false,
			],
		];

		$config = [
			'id'           => 'tgmpa',                 // Unique ID for hashing notices for multiple instances of TGMPA.
			'default_path' => '',                      // Default absolute path to bundled plugins.
			'menu'         => 'tgmpa-install-plugins', // Menu slug.
			'parent_slug'  => 'themes.php',            // Parent menu slug.
			'capability'   => 'edit_theme_options',    // Capability needed to view plugin install page, should be a capability associated with the parent menu used.
			'has_notices'  => true,                    // Show admin notices or not.
			'dismissable'  => true,                    // If false, a user cannot dismiss the nag message.
			'dismiss_msg'  => '',                      // If 'dismissable' is false, this message will be output at top of nag.
			'is_automatic' => false,                   // Automatically activate plugins after installation or not.
			'message'      => '',                      // Message to output right before the plugins table.
		];

		tgmpa( $plugins, $config );

	}

endif;

add_action( 'tgmpa_register', 'wpstarter_register_recommended_plugins' );

function wp_dequeue_gutenberg_styles() {

	wp_dequeue_style( 'wp-block-library' );
	// wp_dequeue_style( 'wp-block-library-theme' );

}

add_action( 'wp_print_styles', 'wp_dequeue_gutenberg_styles', 100 );
