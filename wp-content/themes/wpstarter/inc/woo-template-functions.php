<?php
/**
 * Custom template & tag functions.
 *
 * @since 1.0.0
 *
 * @package wpstarter
 */

if ( ! function_exists( 'wpstarter_woo_product_thumbnail' ) ) :

	/**
	 * Displays the product thumbnail.
	 */	 
	function wpstarter_woo_product_thumbnail( $class = '', $image_args = [] ) { ?>

		<a href='<?php echo esc_url( get_the_permalink() ); ?>' class='<?php echo esc_attr( "{$class}__img-link" ); ?>'>
			<?php wpstarter_post_thumbnail( $id, $image_args ); ?>
		</a>

	<?php }

endif;