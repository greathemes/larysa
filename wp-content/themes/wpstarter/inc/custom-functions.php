<?php
/**
 * Helper functions & filters.
 *
 * @since 1.0.0
 *
 * @package wpstarter
 */

if ( ! function_exists( 'wpstarter_additional_body_classes' ) ) :

	/**
	 * Adds custom classes to the array of body classes.
	 *
	 * @param array $classes Classes for the body element.
	 * @return array
	 */
	function wpstarter_additional_body_classes( $classes ) {

		$id      = get_the_id();
		$options = wpstarter_get_all_options();

		// Static Front Page.
		if ( wpstarter_is_static_front_page() ) :
			$classes[] = 'front-page no-sidebar content-full-width';
		endif;

		// Page.
		if ( is_page() && ! is_front_page() && ! is_single() && ! is_page_template( 'portfolio.php' ) ) :

			$sidebar_left   = is_page_template( 'templates/page-sidebar-left.php' );
			$sidebar_right  = is_page_template( 'templates/page-sidebar-right.php' );
			$sidebar_layout = $sidebar_left ? 'sidebar sidebar_left' : ( $sidebar_right ? 'sidebar sidebar_right' : '' );

			$classes[] = $sidebar_layout && is_active_sidebar( 'primary' ) ? $sidebar_layout : 'no-sidebar';
			$classes[] = is_page_template( 'templates/page-narrow.php' ) ? 'narrow-container-width' : 'content-container-width';

		endif;

		// Blog ( Home and index.php ) and archive ( Categories, tags, months, author ).
		if ( ( is_home() || is_archive() || is_attachment() || wpstarter_is_non_static_front_page() ) && ! wpstarter_is_static_front_page() ) :

			$layout    = $options['blog']['layout'];
			$classes[] = $layout === 'default' || $layout === 'narrow' || ! is_active_sidebar( 'primary' ) ? 'no-sidebar blog-posts' : "sidebar $layout blog-posts";
			$classes[] = $layout === 'narrow' ? 'narrow-container-width' : 'content-container-width';

		endif;

		// Single blog post.
		if ( is_singular( 'post' ) ) :

			$is_sidebar_left       = is_page_template( 'templates/post-sidebar-left.php' );
			$is_sidebar_right      = is_page_template( 'templates/post-sidebar-right.php' );
			$is_sidebar_customizer = get_page_template_slug() === '' && $options['blog']['single_layout'];
			$is_sidebar            = is_active_sidebar( 'primary' );

			if ( $is_sidebar && ( $is_sidebar_left || $is_sidebar_right ) ) :
				$classes[] = $is_sidebar_left ? 'sidebar sidebar_left' : 'sidebar sidebar_right';
			elseif ( $is_sidebar && $is_sidebar_customizer ) :
				$classes[] = 'sidebar ' . $options['blog']['single_layout'];
			else :
				$classes[] = 'no-sidebar narrow-container-width';
			endif;

		endif;

		// Search.
		$classes[] = is_search() ? 'no-sidebar narrow-container-width' : '';

		// 404.
		$classes[] = is_404() ? 'no-sidebar narrow-container-width' : '';

		// Portfolio.
		if ( class_exists( 'Wpstarter_Portfolio' ) && is_page_template( 'portfolio.php' ) ) :

			$classes[] = $options['portfolio']['layout'] === 'narrow' ? 'narrow-container-width no-sidebar' : 'content-container-width no-sidebar';

		endif;

		// Single portfolio post.
		if ( is_singular( 'portfolio' ) ) :
			$classes[] = 'content-container-width no-sidebar';
		endif;

		// Non-singular pages.
		$classes[] = ( ! is_singular() ) ? 'hfeed' : '';

		// No featured image.
		if ( ! wpstarter_is_static_front_page() ) :
			if ( is_search() || is_archive() || wpstarter_is_non_static_front_page() ) :
				$classes[] = 'no-featured-image';
			elseif ( ! wpstarter_is_static_blog() && ! has_post_thumbnail() ) :
				$classes[] = 'no-featured-image';
			elseif ( wpstarter_is_static_blog() && ! has_post_thumbnail( get_option( 'page_for_posts' ) ) ) :
				$classes[] = 'no-featured-image';
			endif;
		endif;

		return $classes;

	}

endif;

add_filter( 'body_class', 'wpstarter_additional_body_classes' );

if ( ! function_exists( 'wpstarter_additional_admin_body_classes' ) ) :

	/**
	 * Adds custom classes to the array of admin body classes.
	 *
	 * @param array $classes Classes for the admin body element.
	 * @return array
	 */
	function wpstarter_additional_admin_body_classes( $classes ) {

		if ( ! is_active_sidebar( 'primary' ) ) :
			$classes .= 'sidebar-is-not-active';
		elseif ( wpstarter_get_all_options()['blog']['single_layout'] ) :
			$classes .= 'sidebar-is-active customizer-template-' . wpstarter_get_all_options()['blog']['single_layout'];
		else :
			$classes .= 'customizer-template-no-sidebar';
		endif;

		return $classes;

	}

endif;

add_filter( 'admin_body_class', 'wpstarter_additional_admin_body_classes' );

if ( ! function_exists( 'wpstarter_add_class_to_archive_description' ) ) :

	/**
	 * Adds custom class to the archive description paragraph.
	 *
	 * @param string $archive_description The value describes the entire archive description of the html code.
	 * @return string With the class added to the paragraph.
	 */
	function wpstarter_add_class_to_archive_description( $archive_description ) {

		return str_replace( '<p>', '<p class="page-header-description">', $archive_description );

	}

endif;

add_filter( 'get_the_archive_description', 'wpstarter_add_class_to_archive_description' );

if ( ! function_exists( 'wpstarter_modify_tag_widget_sizes' ) ) :

	/**
	 * Modify the font size of tag widget.
	 *
	 * @param array $args Array of tag widget parameters.
	 * @return array With modified font size parameters.
	 */
	function wpstarter_modify_tag_widget_sizes( $args ) {

		$args['unit']     = 'rem';
		$args['smallest'] = 0.8;
		$args['default']  = 1;
		$args['largest']  = 1.2;

		return $args;

	}

endif;

add_filter( 'widget_tag_cloud_args', 'wpstarter_modify_tag_widget_sizes' );

if ( ! function_exists( 'wpstarter_modify_wp_list_categories_count' ) ) :

	/**
	 * Wrap the post count in a span tag.
	 *
	 * @param string $links Links.
	 * @return string Links with added span tag.
	 */
	function wpstarter_modify_wp_list_categories_count( $links ) {

		$links = str_replace( '</a> (', '</a> <span>(', $links );
		$links = str_replace( ')', ')</span>', $links );

		return $links;

	}

endif;

add_filter( 'wp_list_categories', 'wpstarter_modify_wp_list_categories_count' );

if ( ! function_exists( 'wpstarter_modify_custom_logo' ) ) :

	/**
	 * Remove default link from custom logo.
	 *
	 * @return string Default logo with removed default link.
	 */
	function wpstarter_modify_custom_logo() {

		$args = [
			'class' => 'custom-logo',
			'alt'   => esc_html__( 'Site logo', 'TRANSLATE' )
		];

		return wp_get_attachment_image( get_theme_mod( 'custom_logo' ), 'full', false, $args );

	}

endif;

add_filter( 'get_custom_logo', 'wpstarter_modify_custom_logo' );

if ( ! function_exists( 'wpstarter_set_custom_excerpt_length' ) ) :

	/**
	 * Sets the length of the text.
	 *
	 * @see wpstarter_excerpt
	 * @global $wpstarter_custom_excerpt_length
	 * @param int $length Length of the excerpt.
	 * @return int
	 */
	function wpstarter_set_custom_excerpt_length( $length ) {

		global $wpstarter_custom_excerpt_length;

		return ! isset( $wpstarter_custom_excerpt_length ) ? $length : $wpstarter_custom_excerpt_length;

	}

endif;

if ( ! function_exists( 'wpstarter_set_custom_excerpt_more' ) ) :

	/**
	 * Sets the custom excerpt more.
	 *
	 * @see wpstarter_excerpt
	 * @global $wpstarter_custom_excerpt_more
	 * @param string $more Excerpt more html code.
	 * @return string
	 */
	function wpstarter_set_custom_excerpt_more( $more ) {

		global $wpstarter_custom_excerpt_more;

		return ! isset( $wpstarter_custom_excerpt_more ) ? $more : $wpstarter_custom_excerpt_more;

	}

endif;

if ( ! function_exists( 'wpstarter_excerpt' ) ) :

	/**
	 * The custom post excerpt.
	 *
	 * @see wpstarter_set_custom_excerpt_length
	 * @see wpstarter_set_custom_excerpt_more
	 * @param int  $length Length of the excerpt. Default 50.
	 * @param int  $post_id Id of the post. Default ''.
	 * @param bool $link Add a link or not. Accepts true, false. Default true.
	 * @return void
	 */
	function wpstarter_excerpt( $length = 50, $post_id = '' ) {

		global $wpstarter_custom_excerpt_length, $wpstarter_custom_excerpt_more, $post;
		$wpstarter_custom_excerpt_length = $length;
		$wpstarter_custom_excerpt_more   = '...';

		$post_id = ( '' !== $post_id ) ? $post_id : $post->ID;

		add_filter( 'excerpt_length', 'wpstarter_set_custom_excerpt_length', 999 );
		add_filter( 'excerpt_more', 'wpstarter_set_custom_excerpt_more', 999 );

		$excerpt = get_the_excerpt( $post_id ); 

		if ( ! $excerpt ) :
			$excerpt = wp_trim_words( get_the_content(), $length );
		endif;

		return $excerpt;

	}

endif;

if ( ! function_exists( 'wpstarter_loop_content_template' ) ) :

	/**
	 * Checks which template is currently in use and returns an adequate string.
	 *
	 * @return string
	 */
	function wpstarter_loop_content_template() {

		if ( is_search() ) :
			return 'search';
		elseif ( is_attachment() ) :
			return 'attachment';
		elseif ( is_single() ) :
			return 'single';
		elseif ( is_front_page() ) :
			return 'front-page';
		else :
			return get_post_type();
		endif;

	}

endif;

if ( ! function_exists( 'wpstarter_change_default_get_search_form' ) ) :

	/**
	 * Modify the default search form.
	 *
	 * @link https://developer.wordpress.org/reference/functions/get_search_form/
	 * @param string $form Default WordPress search form.
	 * @return string Modified version of default search form.
	 */
	function wpstarter_change_default_get_search_form( $form ) {

		$unique_id = esc_attr( uniqid( 'search-form-' ) );

		ob_start(); ?>

		<form role='search' method='get' class='search-form' action='<?php echo esc_url( home_url( '/' ) ); ?>'>
			<label class='search-form__label' for='<?php echo esc_attr( $unique_id ); ?>'><span class='screen-reader-text'><?php echo esc_html_x( 'Search for:', 'Label', 'TRANSLATE' ); ?></span></label>
			<input type='search' id='<?php echo esc_attr( $unique_id ); ?>' placeholder='...' class='search-form__field' value='<?php echo esc_attr( get_search_query() ); ?>' name='s' />
			<button type='submit' class='search-form__button search-form__button--button fas fa-search'><span class='screen-reader-text'><?php echo esc_html_x( 'Search', 'Submit button text', 'TRANSLATE' ); ?></span></button>
		</form>

		<?php return ob_get_clean();

	}

endif;

add_filter( 'get_search_form', 'wpstarter_change_default_get_search_form' );

if ( ! function_exists( 'wpstarter_post_author' ) ) :

	/**
	 * Post author.
	 */
	function wpstarter_post_author() {

		$author_id         = get_the_author_meta( 'ID' ) ? get_the_author_meta( 'ID' ) : get_post_field( 'post_author', get_the_ID() );
		$name              = get_the_author_meta( 'display_name', $author_id );
		$nickname          = get_the_author_meta( 'nickname', $author_id );
		$author_url        = get_author_posts_url( $author_id );
		$author_title_attr = sprintf(
			// translators: %s: The name of the author.
			esc_html__( 'Show all posts by %s', 'TRANSLATE' ),
			$name ? $name : $nickname
		);

		return [
			'id'         => $author_id,
			'name'       => $name ? $name : $nickname,
			'url'        => $author_url,
			'title_attr' => $author_title_attr,
		];

	}

endif;

if ( ! function_exists( 'wpstarter_post_comments' ) ) :

	/**
	 * Post comment number.
	 */
	function wpstarter_post_comments() {

		$html = '<a href="' . esc_url( get_comments_link() ) . '" title="' . esc_attr__( 'Go to the comments section', 'TRANSLATE' ) . '">';
		$html .= esc_html( get_comments_number() );
		$html .= '</a>';

		return $html;

	}

endif;

if ( ! function_exists( 'wpstarter_post_categories' ) ) :

	/**
	 * Post categories.
	 *
	 * @param int $post_id Id of the post. Default ''.
	 */
	function wpstarter_post_categories( $post_id = null, $args = [] ) {

		$class         = isset( $args['class'] ) ? $args['class'] : '';
		$is_unstyled          = isset( $args['is_unstyled'] ) ? $args['is_unstyled'] : '';
		$is_unstyled          = $is_unstyled ? "{$class}__categories-list--unstyled" : '';
		$categories_are_links = isset( $args['categories_are_links'] ) ? $args['categories_are_links'] : '';
		$category_slug        = isset( $args['category_slug'] ) ? $args['category_slug'] : 'category';
		$max_number           = isset( $args['max_number'] ) ? $args['max_number'] : '';

		$post_id        = $post_id ? $post_id : get_the_id();
		$terms          = $category_slug === 'category' ? wp_get_post_categories( $post_id ) : wp_get_post_terms( $post_id, $category_slug );
		$categories_num = count( $terms );
		$i              = 0;

		if ( $categories_num > 0 ) : ?>

			<ul class='<?php echo esc_attr( "{$class}__categories-list $is_unstyled" ); ?>'>

				<?php foreach ( $terms as $term ) :

					$name            = $category_slug === 'category' ? get_cat_name( $term ) : $term->name;
					$link            = $category_slug === 'category' ? get_category_link( $term ) : get_category_link( $term->term_id );
					$link_title_attr = sprintf(
						// translators: %s: The name of the selected category.
						esc_html__( 'Show all posts in category %s', 'TRANSLATE' ),
						$name
					); ?>

					<li class='<?php echo esc_attr( "{$class}__categories-item" ); ?>'>
						<?php if ( $categories_are_links ): ?>
							<a class='<?php echo esc_attr( "{$class}__categories-link" ); ?>' href='<?php echo esc_url( $link ) ?>' title='<?php echo esc_attr( $link_title_attr ) ?>'>
						<?php endif; ?>
						<span class='<?php echo esc_attr( "{$class}__categories-text" ); ?>'><?php echo esc_html( $name ); ?></span>
						<?php if ( $categories_are_links ): ?>
							</a>
						<?php endif; ?>
					</li>

					<?php $i++;
					if ( $max_number && $i === $max_number ) :
						break;
					endif;

				endforeach; ?>

			</ul>

		<?php endif;

	}

endif;



if ( ! function_exists( 'wpstarter_post_tags' ) ) :

	/**
	 * Post tags.
	 *
	 * @param int $post_id Id of the post. Default ''.
	 */
	function wpstarter_post_tags( $post_id = null, $args = [] ) {

		$class   = isset( $args['class'] ) ? $args['class'] : '';
		$is_unstyled    = isset( $args['is_unstyled'] ) ? $args['is_unstyled'] : '';
		$is_unstyled    = $is_unstyled ? "{$class}__tags-list--unstyled" : '';
		$tags_are_links = isset( $args['tags_are_links'] ) ? $args['tags_are_links'] : '';
		$post_id        = $post_id ? $post_id : get_the_id();

		if ( ! get_the_tags( $post_id ) ) :
			return;
		endif; ?>

		<ul class='<?php echo esc_attr( "{$class}__tags-list $is_unstyled" ); ?>'>

			<?php foreach ( get_the_tags( $post_id ) as $id => $tag_id ) :

				$link            = get_tag_link( $tag_id );
				$name            = esc_html( get_tag( $tag_id )->name );
				$link_title_attr = sprintf(
					// translators: %s: The name of the tag.
					esc_html__( 'Show all posts containing the tag %s', 'TRANSLATE' ),
					esc_attr( $name )
				); ?>

				<li class='<?php echo esc_attr( "{$class}__tags-item" ); ?>'>
					<?php if ( $tags_are_links ): ?>
						<a class='<?php echo esc_attr( "{$class}__tags-link" ); ?>' href='<?php echo esc_url( $link ) ?>' title='<?php echo esc_attr( $link_title_attr ) ?>'>
					<?php endif; ?>
					<span class='<?php echo esc_attr( "{$class}__tags-hash" ); ?>' aria-hidden='true'>&#35;</span>
					<?php echo esc_html( $name ); ?>
					<?php if ( $tags_are_links ): ?>
						</a>
					<?php endif; ?>
				</li>

			<?php endforeach; ?>

		</ul>

	<?php }

endif;

if ( ! function_exists( 'wpstarter_custom_srcset' ) ) :

	/**
	 * Returns the srcset attribute based on the given image size.
	 *
	 * @param int   $img_id Post thumbnail ID. Default ''.
	 * @param array $sizes An array with the expected sizes. Default [].
	 * @return array An array with the expected srcset values.
	 */
	function wpstarter_custom_srcset( $img_id = '', $sizes = [] ) {

		// Get an array with image size names.
		$img_size_names = get_intermediate_image_sizes();

		// Convert how many size names we have.
		$img_sizes_length = count( $img_size_names );

		// Get an array with image sizes.
		$img_metadata = wp_get_attachment_metadata( $img_id );

		if ( empty( $img_metadata ) ) : return; endif;

		$img_sizes = $img_metadata['sizes'];

		// We start with an empty array.
		$srcset_arr = [];

		for ( $i = 0; $i < $img_sizes_length; $i++ ) :

			// We check whether there is a name in the array $img_size_names from the $sizes array we have given, and whether there is a picture of a given width in the $img_sizes array.
			if ( in_array( $img_size_names[ $i ], $sizes, true ) && isset( $img_sizes[ $img_size_names[ $i ] ]['width'] ) ) :

				array_push( $srcset_arr, wp_get_attachment_image_url( $img_id, $img_size_names[ $i ] ) . " {$img_sizes[ $img_size_names[ $i ] ][ 'width' ]}w" );

			endif;

		endfor;

		return $srcset_arr;

	}

endif;

if ( ! function_exists( 'wpstarter_post_thumbnail' ) ) :

	/**
	 * Returns the srcset attribute based on the given image size.
	 *
	 * @param int   $img_id Post thumbnail ID. Default ''.
	 * @param array $sizes An array with the expected sizes. Default [].
	 * @return array An array with the expected srcset values.
	 */
	function wpstarter_post_thumbnail( $id = '', array $args = [] ) {

		$img_id                 = $id ? $id : get_post_thumbnail_id();
		$img_alt                = get_post_meta( $img_id, '_wp_attachment_image_alt', true );
		$size                   = isset( $args['size'] ) ? $args['size'] : '';
		$max_width              = isset( $args['max_width'] ) ? $args['max_width'] : '';
		$sizes                  = isset( $args['sizes'] ) ? $args['sizes'] : [];
		$class                  = isset( $args['class'] ) ? $args['class'] : '';
		$lazy                   = isset( $args['lazy'] ) ? $args['lazy'] : false;
		$is_lazy_image_excluded = isset( $args['is_lazy_image_excluded'] ) ? $args['is_lazy_image_excluded'] : false;
		$display                = isset( $args['display'] ) ? $args['display'] : true;

		if ( $size && is_array( $sizes ) && ! empty( $sizes ) ) :

			array_push( $sizes, 'medium' ); // Wordpress default size.
			array_push( $sizes, 'large' ); // Wordpress default size.

			$img_srcset = wpstarter_custom_srcset( $img_id, $sizes );

			if ( ! $max_width ) :
				// Get the image width and divide by 2 if it's a retina screen.
				if ( mb_strpos( $size, 'retina' ) !== false ) :
					$max_width = wp_get_attachment_image_src( $img_id, $size )[1] / 2;
				else :
					$max_width = wp_get_attachment_image_src( $img_id, $size )[1];
				endif;
			endif;

			if ( $lazy ) :

				$img_args = [
					'src'              => 'data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7',
					'data-src'         => wp_get_attachment_image_src( $img_id, $size )[0],
					'srcset'           => 'data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7',
					'data-srcset'      => $img_srcset ? esc_attr( implode( ',', $img_srcset ) ) : '',
					'alt'              => esc_attr( $img_alt ),
					'sizes'            => "(max-width: {$max_width}px) 100vw, {$max_width}px",
				];

			else :
				
				$img_args = [
					'srcset'           => $img_srcset ? esc_attr( implode( ',', $img_srcset ) ) : '',
					'alt'              => esc_attr( $img_alt ),
					'sizes'            => "(max-width: {$max_width}px) 100vw, {$max_width}px",
				];

			endif;

			if ( $img_id ) :
				$img_src = wp_get_attachment_image_src( $img_id, 'wpstarter_portfolio_pswp_large' );
				if ( $img_src[1] && $img_src[2] ) :
					$img_aspect_ratio = $img_src[1] / $img_src[2];
				else :
					$img_aspect_ratio = 1;
				endif;
			else :
				$img_aspect_ratio = 1;
			endif;

			$img_args['aspect-ratio'] = $img_aspect_ratio;
			$img_args['data-orientation'] = wpstarter_get_img_orientation( $img_id, 'medium' );
			$class ? $img_args['class'] = $class : '';
			$lazy ? $img_args['class'] .= ' lazy' : '';

			$thumbnail  = get_the_post_thumbnail( $img_id, ( $max_width ? [ intval( $max_width ), 9999 ] : $size ), $img_args );
			$attachment = wp_get_attachment_image( $img_id, ( $max_width ? [ intval( $max_width ), 9999 ] : $size ), false, $img_args );

			$allowed_html = [
				'img' => [ 'width' => [], 'height' => [], 'srcset' => [], 'data-srcset' => [], 'aspect-ratio' => [], 'data-orientation' => [], 'data-src' => [], 'class' => [], 'src' => [], 'alt' => [], 'sizes' => [] ]
			];

			if ( $display ) :
				echo wp_kses( ( $thumbnail ? $thumbnail : $attachment ), $allowed_html );
			else :
				return wp_kses( ( $thumbnail ? $thumbnail : $attachment ), $allowed_html );
			endif;

		endif;

	}

endif;

if ( ! function_exists( 'wpstarter_textarea_output' ) ) :

	/**
	 * Textarea output with span when enter is used.
	 */
	function wpstarter_textarea_output( $text, $echo = true ) {

		$arr     = preg_split( '/\n+/', $text );
		$content = '';

		foreach ( $arr as $v ) :

			$content .= "<span>$v</span>";

		endforeach;

		if ( ! $echo ) : return wp_kses( $content, [ 'span' => [] ] ); endif;

		echo wp_kses( $content, [ 'span' => [] ] );

	}

endif;

if ( ! function_exists( 'wpstarter_wc_hex_is_light' ) ) :

	/**
	 * Determine whether a hex color is light.
	 *
	 * @param mixed $color Color.
	 * @return bool  True if a light color.
	 */
	function wpstarter_wc_hex_is_light( $color ) {

		$hex        = str_replace( '#', '', $color );
		$c_r        = hexdec( substr( $hex, 0, 2 ) );
		$c_g        = hexdec( substr( $hex, 2, 2 ) );
		$c_b        = hexdec( substr( $hex, 4, 2 ) );
		$brightness = ( ( $c_r * 299 ) + ( $c_g * 587 ) + ( $c_b * 114 ) ) / 1000;

		return $brightness > 155;

	}

endif;

if ( ! function_exists( 'wpstarter_brightness' ) ) :

	/**
	 * Makes color lighten od darken.
	 */
	function wpstarter_brightness( $hex, $percent, $darken = null ) {

		if ( $hex ) :

			if ( $darken === null ) :
				$brightness = wpstarter_wc_hex_is_light( $hex ) ? -255 : 255;
			elseif ( $darken !== null && $darken ) :
				$brightness = 255;
			else :
				$brightness = -255;
			endif;

			// Steps should be between -255 and 255. Negative = darker, positive = lighter.
			$steps = $percent * $brightness / 100;

			// Normalize into a six character long hex string.
			$hex = str_replace( '#', '', $hex );

			if ( strlen( $hex ) === 3 ) :
				$hex = str_repeat( substr( $hex, 0, 1 ), 2 ) . str_repeat( substr( $hex, 1, 1 ), 2 ) . str_repeat( substr( $hex, 2, 1 ), 2 );
			endif;

			// Split into three parts: R, G and B.
			$color_parts = str_split( $hex, 2 );
			$return      = '#';

			foreach ( $color_parts as $color ) :

				$color   = hexdec( $color ); // Convert to decimal.
				$color   = max( 0, min( 255, $color + $steps ) ); // Adjust color.
				$return .= str_pad( dechex( $color ), 2, '0', STR_PAD_LEFT ); // Make two char hex code.

			endforeach;

			return $return;

		else :

			return '';

		endif;

	}

endif;

if ( ! function_exists( 'wpstarter_comment_field_placeholder' ) ) :

	/**
	 * Adds placeholder to comment form field.
	 */
	function wpstarter_comment_field_placeholder( $comment_field ) {

		$comment_field = '<p class="comment-form-comment">
			<textarea required placeholder="'. esc_attr_x( 'Comment', 'comment form placeholder', 'TRANSLATE' ) . '*" id="comment" name="comment" cols="25" rows="1" aria-required="true"></textarea>
		</p>';

		return $comment_field;

	}

endif;

add_filter( 'comment_form_field_comment', 'wpstarter_comment_field_placeholder' );

if ( ! function_exists( 'wpstarter_comment_fields_placeholders' ) ) :

	/**
	 * Adds placeholders to comment form fields.
	 */
	function wpstarter_comment_fields_placeholders( $fields ) {

		$commenter    = wp_get_current_commenter();
		$req          = get_option( 'require_name_email' );
		$aria_req     = ( $req ? " aria-required='true'" : '' );
		$placeholders = [
			'0' => esc_html_x( 'Name', 'comment form placeholder', 'TRANSLATE' ),
			'1' => esc_html_x( 'Email', 'comment form placeholder', 'TRANSLATE' ),
			'2' => esc_html_x( 'Website', 'comment form placeholder', 'TRANSLATE' ),
		];

		$fields['author'] = '<p class="comment-form-author">
			<input required minlength="3" maxlength="30" placeholder="' . esc_attr( $placeholders[0] ) . '*" id="author" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) . '" size="30"' . $aria_req . '/>
		</p>';

		$fields['email'] = '<p class="comment-form-email">
			<input required placeholder="' . esc_attr( $placeholders[1] ) . '*" id="email" name="email" type="email" value="' . esc_attr( $commenter['comment_author_email'] ) . '" size="30"' . $aria_req . '/>
		</p>';

		$fields['url'] = '<p class="comment-form-url">
			<input placeholder="' . esc_attr( $placeholders[2] ) . '" id="url" name="url" type="url" value="' . esc_attr( esc_url( $commenter['comment_author_url'] ) ) . '" size="30" />
		</p>';

		return $fields;

	}

endif;

add_filter( 'comment_form_default_fields','wpstarter_comment_fields_placeholders' );

if ( ! function_exists( 'wpstarter_custom_reply_title' ) ) :

	function wpstarter_custom_reply_title( $defaults ) {

		$defaults['title_reply'] = esc_html__( 'Leave a comment', 'TRANSLATE' );
		return $defaults;

	}

endif;

add_filter( 'comment_form_defaults', 'wpstarter_custom_reply_title' );

if ( ! function_exists( 'wpstarter_avatar' ) ) :

	function wpstarter_avatar( $user_id, $size, $custom_size, array $custom_sizes ) {

		$custom_avatar_id = get_user_meta( $user_id, 'wpstarter_custom_avatar', true );

		if ( $custom_avatar_id ) :
			$avatar = wpstarter_post_thumbnail( $custom_avatar_id,
				[
					'size'    => $custom_size,
					'sizes'   => $custom_sizes,
					'display' => false
				]
			);
		else :
			$avatar = get_avatar( $user_id , $size );
		endif;

		return $avatar;

	}

endif;

if ( ! function_exists( 'wpstarter_get_social_media_arr' ) ) :

	function wpstarter_get_social_media_arr( $value = '' ) {

		$social_media = [
			0 => esc_html__( 'Please Select', 'TRANSLATE' ),
			'angellist'      => 'AngelList',
			'bandcamp'       => 'Bandcamp',
			'behance'        => 'Behance',
			'codepen'        => 'Codepen',
			'delicious'      => 'Delicious',
			'deviantart'     => 'deviantART',
			'digg'           => 'Digg',
			'dribbble'       => 'Dribbble',
			'etsy'           => 'Etsy',
			'facebook-f'     => 'Facebook',
			'flickr'         => 'Flickr',
			'foursquare'     => 'Foursquare',
			'google-plus-g'  => 'Google+',
			'houzz'          => 'Houzz',
			'instagram'      => 'Instagram',
			'imdb'           => 'IMDB',
			'lastfm'         => 'Last.fm',
			'linkedin-in'    => 'LinkedIn',
			'medium-m'       => 'Medium',
			'meetup'         => 'Meetup',
			'mixcloud'       => 'Mixcloud',
			'odnoklassniki'  => 'Odnoklassniki',
			'pinterest-p'    => 'Pinterest',
			'product-hunt'   => 'Product Hunt',
			'quora'          => 'Quora',
			'reddit'         => 'Reddit',
			'soundcloud'     => 'SoundCloud',
			'spotify'        => 'Spotify',
			'stack-exchange' => 'Stack Exchange',
			'stack-overflow' => 'Stack Overflow',
			'stumbleupon'    => 'StumbleUpon',
			'telegram'       => 'Telegram',
			'tripadvisor'    => 'TripAdvisor',
			'tumblr'         => 'Tumblr',
			'twitter'        => 'Twitter',
			'vimeo-v'        => 'Vimeo',
			'vine'           => 'Vine',
			'vk'             => 'VK',
			'weibo'          => 'Weibo',
			'wordpress'      => 'WordPress',
			'yelp'           => 'Yelp',
			'youtube'        => 'Youtube',
		];

		return $value ? $social_media[ $value ] : $social_media;

	}

endif;

if ( ! function_exists( 'wpstarter_main_query_sticky_posts_fix' ) ) :

	function wpstarter_main_query_sticky_posts_fix( $query ) {

		if ( $query->is_main_query() && wpstarter_is_blog_posts_page() ) :

			 // Set the number of posts per page and sticky posts array.
			$posts_per_page = get_option( 'posts_per_page' );
			$posts_per_page = ! $query->is_paged() && wpstarter_get_options_blog()['featured'] ? $posts_per_page + 1 : $posts_per_page;
			$sticky_posts   = get_option( 'sticky_posts' );

			// If we have any sticky posts and we are at the first page.
			if ( is_array( $sticky_posts ) && ! $query->is_paged() ) :

				// Array flip to reduce the time taken by using isset and not in_array.
				$posts = array_flip(
					get_posts( [
						'post_type'     => 'post',
						'post_per_page' => $posts_per_page,
						'paged'         => 1,
						'fields'        => 'ids'
					] )
				);

				$sticky_count = count( $sticky_posts );

				// Loop the posts from the 2nd query to see if the ID's of the sticky posts sit inside it.
				foreach ( $sticky_posts as $sticky_post ) :
					if ( isset( $posts[ $sticky_post ] ) ) :
						$sticky_count--;
					endif;
				endforeach;

				// And if the number of sticky posts is less than the number we want to set:
				if ( $sticky_count < $posts_per_page ) :
					$query->set( 'posts_per_page', $posts_per_page - $sticky_count );
				else :
					// If the number of sticky posts is greater than or equal the number of pages we want to set:
					$query->set( 'posts_per_page', 1 );
				endif;

			else :
				$query->set( 'posts_per_page', $posts_per_page );
			endif;

		endif;

	}

endif;

add_action( 'pre_get_posts', 'wpstarter_main_query_sticky_posts_fix' );

if ( ! function_exists( 'wpstarter_custom_query_sticky_posts_fix' ) ) :

	function wpstarter_custom_query_sticky_posts_fix( $query ) {

			if ( isset( $query->query['sticky_posts_fix'] ) ) :

				// Set the number of posts per page and sticky posts array.
				$posts_per_page = $query->query['posts_per_page'];
				$sticky_posts   = get_option( 'sticky_posts' );

				// If we have any sticky posts and we are at the first page.
				if ( is_array( $sticky_posts ) && ! $query->is_paged() ) :

					// Array flip to reduce the time taken by using isset and not in_array.
					$posts = array_flip(
						get_posts( [
							'post_type'     => 'post',
							'post_per_page' => $posts_per_page,
							'paged'         => 1,
							'fields'        => 'ids'
						] )
					);

					$sticky_count = count( $sticky_posts );

					// Loop the posts from the 2nd query to see if the ID's of the sticky posts sit inside it.
					foreach ( $sticky_posts as $sticky_post ) :
						if ( isset( $posts[ $sticky_post ] ) ) :
							$sticky_count--;
						endif;
					endforeach;

					// And if the number of sticky posts is less than the number we want to set:
					if ( $sticky_count < $posts_per_page ) :
						$query->set( 'posts_per_page', $posts_per_page - $sticky_count );
					else :
						// If the number of sticky posts is greater than or equal the number of pages we want to set:
						$query->set( 'posts_per_page', 1 );
					endif;

				else :
					$query->set( 'posts_per_page', $posts_per_page );
				endif;

			endif;

	}

endif;

if ( ! function_exists( 'wpstarter_kirki_repeater_check_required_fields' ) ) :

	function wpstarter_kirki_repeater_check_required_fields( $fields, $required_fields ) {

		if ( is_array( $fields ) && is_array( $required_fields ) && ! empty( $fields ) && ! empty( $required_fields ) ) :

			$items_number           = 0;
			$fields_number          = count( $fields );
			$required_fields_number = count( $required_fields );

			for ( $i = 0; $i < $fields_number; $i++ ) :

				$initial_fields_number = 0;

				foreach ( $required_fields as $required_field ) :

					isset( $fields[ $i ][ $required_field ] ) && $fields[ $i ][ $required_field ] ? $initial_fields_number++ : '';

				endforeach;

				$initial_fields_number === $required_fields_number ? $items_number++ : '';

			endfor;

			return $items_number;

		else :

			return false;

		endif;

	}

endif;

if ( ! function_exists( 'wpstarter_is_static_front_page' ) ) :

	function wpstarter_is_static_front_page() {

		return ( is_front_page() && is_page() );

	}

endif;

if ( ! function_exists( 'wpstarter_is_static_blog' ) ) :

	function wpstarter_is_static_blog() {

		return ( is_home() && ! is_front_page() );

	}

endif;

if ( ! function_exists( 'wpstarter_is_non_static_front_page' ) ) :

	function wpstarter_is_non_static_front_page() {

		return ( is_home() && is_front_page() );

	}

endif;

if ( ! function_exists( 'wpstarter_is_blog_posts_page' ) ) :

	function wpstarter_is_blog_posts_page() {

		return ( wpstarter_is_static_blog() || is_archive() || wpstarter_is_non_static_front_page() );

	}

endif;

if ( ! function_exists( 'wpstarter_get_img_orientation' ) ) :

	/**
	 * Get image orientation.
	 */
	function wpstarter_get_img_orientation( $id, $size = 'medium' ) {

		if ( $id ) :

			$img_src = wp_get_attachment_image_src( $id, $size );

			if ( $img_src ) :

				$img_width  = $img_src[1];
				$img_height = $img_src[2];

				return $img_width === $img_height ? 'square' : ( $img_width > $img_height ? 'horizontal' : 'vertical' );

			endif;

		endif;

	}

endif;

/**
 * Delete all transients associated with the Customizer after saving.
 */
add_action( 'customize_save_after', function() {

	delete_transient( 'wpstarter_transient_instagram_feed' );

} );

if ( ! function_exists( 'wpstarter_woo_ajax_add_to_cart' ) ) :

	/**
	 * Add AJAX to .
	 */	 
	function wpstarter_woo_ajax_add_to_cart() {

		$product_id        = apply_filters( 'woocommerce_add_to_cart_product_id', absint( $_POST['product_id'] ) );
		$quantity          = empty( $_POST['quantity'] ) ? 1 : wc_stock_amount( $_POST['quantity'] );
		$variation_id      = absint( $_POST['variation_id'] );
		$passed_validation = apply_filters( 'woocommerce_add_to_cart_validation', true, $product_id, $quantity );
		$product_status    = get_post_status( $product_id );

		if ( $passed_validation && WC()->cart->add_to_cart( $product_id, $quantity, $variation_id ) && 'publish' === $product_status ) :

			do_action( 'woocommerce_ajax_added_to_cart', $product_id );

			if ( 'yes' === get_option( 'woocommerce_cart_redirect_after_add' ) ) :
				wc_add_to_cart_message( [ $product_id => $quantity ], true );
			endif;

			WC_AJAX :: get_refreshed_fragments();

		else :

			$data = [
				'error'       => true,
				'product_url' => apply_filters( 'woocommerce_cart_redirect_after_error', get_permalink( $product_id ), $product_id )
			];

			echo wp_send_json( $data );

		endif;

		wp_die();

	}

endif;

add_action( 'wp_ajax_wpstarter_woo_ajax_add_to_cart', 'wpstarter_woo_ajax_add_to_cart' );
add_action( 'wp_ajax_nopriv_wpstarter_woo_ajax_add_to_cart', 'wpstarter_woo_ajax_add_to_cart' );

if ( ! function_exists( 'wpstarter_woo_remove_breadcrumbs' ) ) :

/**
* Remove the WooCommerce breadcrumbs.
*/
function wpstarter_woo_remove_breadcrumbs() {

	remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20, 0 );

}

endif;

add_action( 'init', 'wpstarter_woo_remove_breadcrumbs' );

if ( ! function_exists( 'wpstarter_woo_comment_rating_field' ) ) :

	/**
	* Create WooCommerce product rating stars.
	*/
	function wpstarter_woo_comment_rating_field () {

		if ( class_exists( 'WooCommerce' ) && is_product() ) : ?>

			<label for='rating'><?php esc_html_e( 'Rating', 'TRANSLATE' ); ?><span class='required'>*</span></label>
			<fieldset class='comments-rating'>
				<span class='rating-container'>
					<?php for ( $i = 5; $i >= 1; $i-- ) : ?>
						<input type='radio' id='rating-<?php echo esc_attr( $i ); ?>' name='rating' value='<?php echo esc_attr( $i ); ?>' /><label for='rating-<?php echo esc_attr( $i ); ?>'><?php echo esc_html( $i ); ?></label>
					<?php endfor; ?>
					<input type='radio' id='rating-0' class='star-cb-clear' name='rating' value='0' /><label for='rating-0'>0</label>
				</span>
			</fieldset>

		<?php endif;

	}

endif;

if ( ! function_exists( 'wpstarter_woo_comment_rating_save' ) ) :

	/**
	* Save the rating submitted by the user.
	*/
	function wpstarter_woo_comment_rating_save( $comment_id ) {

		if ( class_exists( 'WooCommerce' ) && is_product() ) :

			if ( ( isset( $_POST['rating'] ) ) && ( '' !== $_POST['rating'] ) ) :
				$rating = intval( $_POST['rating'] );
				add_comment_meta( $comment_id, 'rating', $rating );
			endif;

		endif;

	}

endif;

if ( ! function_exists( 'wpstarter_woo_comment_rating_require' ) ) :

	/**
	* Make the rating required.
	*/
	function wpstarter_woo_comment_rating_require( $commentdata ) {

		if ( class_exists( 'WooCommerce' ) && is_product() ) :

			if ( ! is_admin() && ( ! isset( $_POST['rating'] ) || 0 === intval( $_POST['rating'] ) ) ) :
				wp_die( __( 'Error: You did not add a rating. Hit the Back button on your Web browser and resubmit your comment with a rating.' ) );
				return $commentdata;
			endif;

		endif;

	}

endif;

if ( ! function_exists( 'wpstarter_woo_comment_rating_display' ) ) :

	/**
	* Display the rating on a submitted comment.
	*/
	function wpstarter_woo_comment_rating_display( $comment_text ) {

		if ( class_exists( 'WooCommerce' ) && is_product() ) :

			if ( $rating = get_comment_meta( get_comment_ID(), 'rating', true ) ) :
				$stars = '<p class="stars">';
				for ( $i = 1; $i <= $rating; $i++ ) :
					$stars .= '<span class="dashicons dashicons-star-filled"></span>';
				endfor;
				$stars .= '</p>';
				$comment_text = $comment_text . $stars;
				return $comment_text;
			else :
				return $comment_text;
			endif;

		endif;

	}

endif;

if ( ! function_exists( 'wpstarter_woo_comment_rating_require' ) ) :

	/**
	* Make the rating required.
	*/
	function wpstarter_woo_comment_rating_require( $commentdata ) {

		if ( class_exists( 'WooCommerce' ) && is_product() ) :

			if ( ! is_admin() && ( ! isset( $_POST['rating'] ) || 0 === intval( $_POST['rating'] ) ) ) :
				wp_die( __( 'Error: You did not add a rating. Hit the Back button on your Web browser and resubmit your comment with a rating.' ) );
				return $commentdata;
			endif;

		endif;

	}

endif;

if ( ! function_exists( 'wpstarter_comment_rating_get_average_ratings' ) ) :

	/**
	* Get the average rating of a post.
	*/
	function wpstarter_comment_rating_get_average_ratings( $id ) {

		if ( class_exists( 'WooCommerce' ) && is_product() ) :

			$comments = get_approved_comments( $id );

			if ( $comments ) {
				$i = 0;
				$total = 0;
				foreach( $comments as $comment ){
					$rate = get_comment_meta( $comment->comment_ID, 'rating', true );
					if( isset( $rate ) && '' !== $rate ) {
						$i++;
						$total += $rate;
					}
				}

				if ( 0 === $i ) {
					return false;
				} else {
					return round( $total / $i, 1 );
				}
			} else {
				return false;
			}

		endif;

	}

endif;

if ( ! function_exists( 'wpstarter_woo_comment_rating_display_average' ) ) :

	/**
	* Display the average rating above the content.
	*/
	function wpstarter_woo_comment_rating_display_average( $content ) {

		if ( class_exists( 'WooCommerce' ) && is_product() ) :

			global $post;

			if ( false === wpstarter_comment_rating_get_average_ratings( $post->ID ) ) {
				return $content;
			}
			
			$stars   = '';
			$average = wpstarter_comment_rating_get_average_ratings( $post->ID );

			for ( $i = 1; $i <= $average + 1; $i++ ) {
				
				$width = intval( $i - $average > 0 ? 20 - ( ( $i - $average ) * 20 ) : 20 );

				if ( 0 === $width ) {
					continue;
				}

				$stars .= '<span style="overflow:hidden; width:' . $width . 'px" class="dashicons dashicons-star-filled"></span>';

				if ( $i - $average > 0 ) {
					$stars .= '<span style="overflow:hidden; position:relative; left:-' . $width .'px;" class="dashicons dashicons-star-empty"></span>';
				}
			}
			
			$custom_content  = '<p class="average-rating">This post\'s average rating is: ' . $average .' ' . $stars .'</p>';
			$custom_content .= $content;

			return $custom_content;

		endif;

	}

endif;

if ( class_exists( 'WooCommerce' ) && is_product() ) :

	add_filter( 'comment_text', 'wpstarter_woo_comment_rating_display' );
	add_action( 'comment_form_logged_in_after', 'wpstarter_woo_comment_rating_field' );
	add_action( 'comment_form_after_fields', 'wpstarter_woo_comment_rating_field' );
	add_filter( 'the_content', 'wpstarter_woo_comment_rating_display_average' );

endif;

/**
* Remove "p" tags from Contact Form 7.
*/
add_filter( 'wpcf7_autop_or_not', '__return_false' );

if ( ! function_exists( 'wpstarter_remove_jquery_migrate' ) ) :

	/**
	 * Remove JQuery migrate.
	 */
	function wpstarter_remove_jquery_migrate( $scripts ) {

		if ( ! is_admin() && isset( $scripts->registered['jquery'] ) ) :

				$script = $scripts->registered['jquery'];

				// Check whether the script has any dependencies.
				if ( $script->deps ) :
					$script->deps = array_diff( $script->deps, [
						'jquery-migrate'
					] );
				endif;

		endif;

	}

endif;

add_action( 'wp_default_scripts', 'wpstarter_remove_jquery_migrate' );

/**
* HTML Compression.
* 
* @see https://zuziko.com/tutorials/how-to-minify-html-in-wordpress-without-a-plugin/
*/

class WPSTARTER_HTML_Compression {

	protected $flhm_compress_css = true;
	protected $flhm_compress_js = false;
	protected $flhm_info_comment = true;
	protected $flhm_remove_comments = true;
	protected $html;

	public function __construct( $html ) {
		if (!empty($html)) {
			$this->flhm_parseHTML($html);
		}
	}

	public function __toString() {
		return $this->html;
	}

	protected function flhm_bottomComment($raw, $compressed) {
		$raw = strlen($raw);
		$compressed = strlen($compressed);
		$savings = ($raw-$compressed) / $raw * 100;
		$savings = round($savings, 2);
		return '<!--HTML compressed, size saved '.$savings.'%. From '.$raw.' bytes, now '.$compressed.' bytes-->';
	}

	protected function flhm_minifyHTML($html) {
		$pattern = '/<(?<script>script).*?<\/script\s*>|<(?<style>style).*?<\/style\s*>|<!(?<comment>--).*?-->|<(?<tag>[\/\w.:-]*)(?:".*?"|\'.*?\'|[^\'">]+)*>|(?<text>((<[^!\/\w.:-])?[^<]*)+)|/si';
		preg_match_all($pattern, $html, $matches, PREG_SET_ORDER);
		$overriding = false;
		$raw_tag = false;
		$html = '';

		foreach ($matches as $token) {
			$tag = (isset($token['tag'])) ? strtolower($token['tag']) : null;
			$content = $token[0];
			if (is_null($tag))
			{
			if ( !empty($token['script']) )
			{
			$strip = $this->flhm_compress_js;
			}
			else if ( !empty($token['style']) )
			{
			$strip = $this->flhm_compress_css;
			}
			else if ($content == '<!--wp-html-compression no compression-->')
			{
			$overriding = !$overriding; 
			continue;
			}
			else if ($this->flhm_remove_comments)
			{
			if (!$overriding && $raw_tag != 'textarea')
			{
			$content = preg_replace('/<!--(?!\s*(?:\[if [^\]]+]|<!|>))(?:(?!-->).)*-->/s', '', $content);
			}
			}
			}
			else
			{
			if ($tag == 'pre' || $tag == 'textarea')
			{
			$raw_tag = $tag;
			}
			else if ($tag == '/pre' || $tag == '/textarea')
			{
			$raw_tag = false;
			}
			else
			{
			if ($raw_tag || $overriding)
			{
			$strip = false;
			}
			else
			{
			$strip = true; 
			$content = preg_replace('/(\s+)(\w++(?<!\baction|\balt|\bcontent|\bsrc)="")/', '$1', $content); 
			$content = str_replace(' />', '/>', $content);
			}
			}
			} 
			if ($strip)
			{
			$content = $this->flhm_removeWhiteSpace($content);
			}
			$html .= $content;
		}

		return $html;
	}

	public function flhm_parseHTML($html) {
		$this->html = $this->flhm_minifyHTML($html);
		if ($this->flhm_info_comment)
		{
		$this->html .= "\n" . $this->flhm_bottomComment($html, $this->html);
		}
	}

	protected function flhm_removeWhiteSpace($str) {
		$str = str_replace("\t", ' ', $str);
		$str = str_replace("\n",  '', $str);
		$str = str_replace("\r",  '', $str);
		while (stristr($str, '  '))
		{
		$str = str_replace('  ', ' ', $str);
		}   
		return $str;
	}

}

function wpstarter_html_compression_finish($html) {
	return new WPSTARTER_HTML_Compression($html);
}

function wpstarter_html_compression_start() {
	ob_start( 'wpstarter_html_compression_finish' );
}

add_action( 'get_header', 'wpstarter_html_compression_start' );

/**
 * Deregister Contact Form 7 scripts and styles.
 */
add_action( 'wp_enqueue_scripts', function() {

	if ( ! is_admin() ) :
		wp_dequeue_script( 'contact-form-7' );
		wp_dequeue_style( 'contact-form-7' );
	endif;

} );
