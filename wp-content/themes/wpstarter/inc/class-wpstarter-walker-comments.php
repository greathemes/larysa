<?php
/**
 * Comment API: Walker_Comment class
 *
 * @since 1.0.0
 *
 * @package wpstarter
 */

if ( ! class_exists( 'Wpstarter_Walker_Comments' ) ) :

	/**
	 * Wpstarter_Walker_Comments
	 *
	 * The class used to remodel the comments section extended by the core walker class used to create an HTML list of comments.
	 */
	class Wpstarter_Walker_Comments extends Walker_Comment {

		/**
		 * What the class handles.
		 *
		 * @var string
		 * @see Walker::$tree_type
		 */
		public $tree_type = 'comment';

		/**
		 * Database fields to use.
		 *
		 * @var array
		 * @see Walker::$db_fields
		 */
		public $db_fields = array(
			'parent' => 'comment_parent',
			'id'     => 'comment_ID',
		);

		/**
		 * Instance
		 *
		 * @access private
		 * @var object Class object.
		 */
		private static $instance;

		function __construct( $class ) {

			$this->class = $class;

		}

		/**
		 * Starts the list before the elements are added.
		 *
		 * @see Walker::start_lvl()
		 * @global int $comment_depth
		 *
		 * @param string $output Used to append additional content (passed by reference).
		 * @param int    $depth  Optional. Depth of the current comment. Default 0.
		 * @param array  $args   Optional. Uses 'style' argument for type of HTML list. Default empty array.
		 */
		public function start_lvl( &$output, $depth = 0, $args = [] ) {

			$comment_depth = $depth + 2;
			$output       .= '<section class="' . $this->class . '__child-comments">';

		}

		/**
		 * Ends the list of items after the elements are added.
		 *
		 * @see Walker::end_lvl()
		 * @global int $comment_depth
		 *
		 * @param string $output Used to append additional content (passed by reference).
		 * @param int    $depth  Optional. Depth of the current comment. Default 0.
		 * @param array  $args   Optional. Will only append content if style argument value is 'ol' or 'ul'.
		 *                       Default empty array.
		 */
		public function end_lvl( &$output, $depth = 0, $args = [] ) {

			$comment_depth = $depth + 2;
			$output       .= '</section>';

		}

		/**
		 * Starts the element output.
		 *
		 * @see Walker::start_el()
		 * @see wp_list_comments()
		 * @global int        $comment_depth
		 * @global WP_Comment $comment
		 *
		 * @param string     $output  Used to append additional content. Passed by reference.
		 * @param WP_Comment $comment Comment data object.
		 * @param int        $depth   Optional. Depth of the current comment in reference to parents. Default 0.
		 * @param array      $args    Optional. An array of arguments. Default empty array.
		 * @param int        $id      Optional. ID of the current comment. Default 0 (unused).
		 */
		public function start_el( &$output, $comment, $depth = 0, $args = [], $id = 0 ) {

			$output .= '<div class="' . $this->class . '__comments-wrapper">';

			$depth++;
			$comment_depth = $depth;

			$class = ( empty( $args['has_children'] ) ? '' : 'parent' );

			if ( 'article' === $args['style'] ) :

				$tag       = 'article';
				$add_below = 'comment';

			else :

				$tag       = 'article';
				$add_below = 'comment';

			endif;

			$output .= '<article id="comment-' . $comment->comment_ID . '">';

			$output .= '<div class="' . $this->class . '__info">';
			$output .= '<figure class="' . $this->class . '__gravatar">' . wpstarter_avatar( $comment->user_id, 38, 'wpstarter_avatar_small', [ 'wpstarter_avatar_small' ] ) . '</figure>';
			$output .= '<div>';
			$output .= '<div class="' . $this->class . '__author">' . $comment->comment_author . '</div>';
			$output .= '<span class="' . $this->class . '__date">' . '<time datetime="' . get_comment_date( 'Y-m-d', $comment->comment_ID ) . 'T' . get_comment_date( 'H:iP', $comment->comment_ID ) . '">' . sprintf( _x( '%s ago', '%s = human-readable time difference', 'TRANSLATE' ), human_time_diff( strtotime( $comment->comment_date ), current_time( 'timestamp' ) ) ) . '</time>' . '</span>';
			$output .= '</div>';
			$output .= '</div>';

			$output .= '<p class="' . $this->class . '__content">';
			$output .= get_comment_text( $comment->comment_ID );
			$output .= '</p>';

			if ( '0' === $comment->comment_approved ) :
				$output .= '<p class="' . $this->class . '__awaiting">' . esc_html__( 'Your comment is awaiting moderation', 'TRANSLATE' ) . '</p>';
			endif;

			$output .= '<div class="' . $this->class . '__links-container">';
			$output .= '<a href="' . get_edit_comment_link( $comment->comment_ID ) . '">' . esc_html__( 'Edit', 'TRANSLATE' ) . '</a>';
			$output .= get_comment_reply_link(
				array_merge(
					$args,
					[
						'add_below' => $add_below,
						'depth'     => $depth,
						'max_depth' => $args['max_depth'],
					]
				),
				$comment
			);

			$output .= '</div>';

			$output .= '</article>';

		}

		/**
		 * Ends the element output, if needed.
		 *
		 * @see Walker::end_el()
		 * @see wp_list_comments()
		 *
		 * @param string     $output  Used to append additional content. Passed by reference.
		 * @param WP_Comment $comment The current comment object. Default current comment.
		 * @param int        $depth   Optional. Depth of the current comment. Default 0.
		 * @param array      $args    Optional. An array of arguments. Default empty array.
		 */
		public function end_el( &$output, $comment, $depth = 0, $args = [] ) {

			$output .= '</div>';

		}

	}

endif;
