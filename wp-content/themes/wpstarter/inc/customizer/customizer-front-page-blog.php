<?php
/**
 * Front Page blog Customizer settings.
 *
 * @since 1.0.0
 *
 * @package wpstarter
 */

if ( class_exists( 'Kirki' ) ) :

	// Blog visibility.
	Kirki::add_field( 'wpstarter', [
		'type'        => 'toggle',
		'settings'    => 'wpstarter_front_page_blog_visibility',
		'label'       => esc_html__( 'Blog visibility', 'TRANSLATE' ),
		'description' => esc_html__( 'Show or hide the blog.', 'TRANSLATE' ),
		'section'     => 'wpstarter_front_page_blog',
		'default'     => '0',
	] );

	// Heading text.
	Kirki::add_field( 'wpstarter', [
		'type'     => 'text',
		'settings' => 'wpstarter_front_page_blog_heading_text',
		'label'    => esc_html__( 'Heading text', 'TRANSLATE' ),
		'section'  => 'wpstarter_front_page_blog',
		'default'  => esc_html__( 'Latest posts', 'TRANSLATE' ),
	] );

	// Number of posts.
	Kirki::add_field( 'wpstarter', [
		'type'        => 'number',
		'settings'    => 'wpstarter_front_page_blog_posts_num',
		'label'       => esc_html__( 'Number of posts', 'TRANSLATE' ),
		'description' => esc_html__( 'Maximum 20.', 'TRANSLATE' ),
		'section'     => 'wpstarter_front_page_blog',
		'default'     => 7,
		'choices'     => [
			'min'  => 1,
			'max'  => 20,
			'step' => 1,
		],
	] );

	// First post featured.
	Kirki::add_field( 'wpstarter', [
		'type'     => 'toggle',
		'settings' => 'wpstarter_front_page_blog_featured',
		'label'    => esc_html__( 'First post featured', 'TRANSLATE' ),
		'section'  => 'wpstarter_front_page_blog',
		'default'  => '1',
	] );

	// Button visibility.
	Kirki::add_field( 'wpstarter', [
		'type'        => 'toggle',
		'settings'    => 'wpstarter_front_page_blog_btn_visibility',
		'label'       => esc_html__( 'Button visibility', 'TRANSLATE' ),
		'description' => esc_html__( 'Show or hide the button.', 'TRANSLATE' ),
		'section'     => 'wpstarter_front_page_blog',
		'default'     => '0',
	] );

	// Button text.
	Kirki::add_field( 'wpstarter', [
		'type'            => 'text',
		'settings'        => 'wpstarter_front_page_blog_btn_text',
		'label'           => esc_html__( 'Button text', 'TRANSLATE' ),
		'section'         => 'wpstarter_front_page_blog',
		'default'         => '',
		'active_callback' => [
			[
				'setting'  => 'wpstarter_front_page_blog_btn_visibility',
				'operator' => '===',
				'value'    => true,
			]
		],
	] );

	$pages[0]  = esc_html__( 'Please Select', 'TRANSLATE' );
	$get_pages = get_pages();

	foreach ( $get_pages as $get_page ) :
		$pages[ $get_page->ID ] = esc_attr( $get_page->post_title );
	endforeach;

	// Button page url.
	Kirki::add_field( 'wpstarter', [
		'type'            => 'select',
		'settings'        => 'wpstarter_front_page_blog_btn_page_url',
		'label'           => esc_html__( 'Button page url', 'TRANSLATE' ),
		'description'     => esc_html__( 'Select the page to which the button links.', 'TRANSLATE' ),
		'section'         => 'wpstarter_front_page_blog',
		'default'         => 0,
		'choices'         => $pages,
		'active_callback' => [
			[
				'setting'  => 'wpstarter_front_page_blog_btn_visibility',
				'operator' => '===',
				'value'    => true,
			]
		],
	] );

endif;
