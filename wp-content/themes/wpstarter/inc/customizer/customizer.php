<?php
/**
 * Customizer config.
 *
 * @since 1.0.0
 *
 * @package wpstarter
 */

if ( class_exists( 'Kirki' ) ) :

	// Config.
	Kirki::add_config( 'wpstarter', [
		'capability'  => 'edit_theme_options',
		'option_type' => 'theme_mod',
	] );

	/**
	* Panels.
	*/

	// Wpstarter options panel.
	Kirki::add_panel( 'wpstarter_options', [
		'priority'    => 10,
		'title'       => esc_html__( 'Wpstarter Options', 'TRANSLATE' ),
	] );

	/**
	* Sections.
	*/

	// Main header.
	Kirki::add_section( 'wpstarter_header', [
		'title'          => esc_html__( 'Main Header', 'TRANSLATE' ),
		'description'    => esc_html__( 'In this section you can set several things related to navigation. Set the breakpoint, search engine visibility and add your social media.', 'TRANSLATE' ),
		'panel'          => 'wpstarter_options',
	] );

	// Front page slider section.
	Kirki::add_section( 'wpstarter_front_page_slider', [
		'title'          => esc_html__( 'Front Page Slider', 'TRANSLATE' ),
		'description'    => esc_html__( "In this section, you can change the blog posts slider settings.", 'TRANSLATE' ),
		'panel'          => 'wpstarter_options',
	] );

	if ( class_exists( 'Wpstarter_Portfolio' ) ) :

		// Front page portfolio section.
		Kirki::add_section( 'wpstarter_front_page_portfolio', [
			'title'          => esc_html__( 'Front Page Portfolio', 'TRANSLATE' ),
			'description'    => esc_html__( 'In this section you can show your portfolio by selecting the best albums.', 'TRANSLATE' ),
			'panel'          => 'wpstarter_options',
		] );

	endif;

	// Front page blog section.
	Kirki::add_section( 'wpstarter_front_page_blog', [
		'title'          => esc_html__( 'Front Page Blog', 'TRANSLATE' ),
		'description'    => esc_html__( 'In this section you can display your latest blog posts on the front page.', 'TRANSLATE' ),
		'panel'          => 'wpstarter_options',
	] );

	// Front page testimonials section.
	Kirki::add_section( 'wpstarter_front_page_testimonials', [
		'title'          => esc_html__( 'Front Page Testimonials', 'TRANSLATE' ),
		'description'    => esc_html__( 'Here you can add opinions of satisfied customers and set some things in the slider.', 'TRANSLATE' ),
		'panel'          => 'wpstarter_options',
	] );

	// Blog section.
	Kirki::add_section( 'wpstarter_blog', [
		'title'          => esc_html__( 'Blog', 'TRANSLATE' ),
		'description'    => esc_html__( 'In this section you can set some blog related things.', 'TRANSLATE' ),
		'panel'          => 'wpstarter_options',
	] );

	if ( class_exists( 'Wpstarter_Portfolio' ) ) :

		// Portfolio section.
		Kirki::add_section( 'wpstarter_portfolio', [
			'title'          => esc_html__( 'Portfolio', 'TRANSLATE' ),
			'description'    => esc_html__( 'In this section you can set some portfolio related things.', 'TRANSLATE' ),
			'panel'          => 'wpstarter_options',
		] );

	endif;

	// Footer section.
	Kirki::add_section( 'wpstarter_footer', [
		'title'          => esc_html__( 'Footer', 'TRANSLATE' ),
		'description'    => esc_html__( "In this section, you'll change some footer related things, for example add Instagram section, copyright text and social media buttons.", 'TRANSLATE' ),
		'panel'          => 'wpstarter_options',
	] );

endif;
