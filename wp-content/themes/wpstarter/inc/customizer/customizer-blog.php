<?php
/**
 * Blog Customizer settings.
 *
 * @since 1.0.0
 *
 * @package wpstarter
 */

if ( class_exists( 'Kirki' ) ) :

	// Blog layout.
	Kirki::add_field( 'wpstarter', [
		'type'        => 'radio',
		'settings'    => 'wpstarter_blog_layout',
		'label'       => esc_html__( 'Blog layout', 'TRANSLATE' ),
		'section'     => 'wpstarter_blog',
		'default'     => 'sidebar_right',
		'choices'     => [
			'default'       => esc_html__( 'Default', 'TRANSLATE' ),
			'narrow'        => esc_html__( 'Narrow', 'TRANSLATE' ),
			'sidebar_left'  => esc_html__( 'Sidebar Left', 'TRANSLATE' ),
			'sidebar_right' => esc_html__( 'Sidebar Right', 'TRANSLATE' ),
		],
	] );

	// Single blog layout.
	Kirki::add_field( 'wpstarter', [
		'type'        => 'radio',
		'settings'    => 'wpstarter_blog_single_layout',
		'label'       => esc_html__( 'Single blog layout', 'TRANSLATE' ),
		'description' => esc_html__( 'This setting is global for all single posts. However, you can change this setting later on the individual post page. So you can have, for example, all blogs with a sidebar on the right and some without.', 'TRANSLATE' ),
		'section'     => 'wpstarter_blog',
		'default'     => 'sidebar_right',
		'choices'     => [
			''              => esc_html__( 'No Sidebar', 'TRANSLATE' ),
			'sidebar_left'  => esc_html__( 'Sidebar Left', 'TRANSLATE' ),
			'sidebar_right' => esc_html__( 'Sidebar Right', 'TRANSLATE' ),
		],
	] );

	// Blog featured.
	Kirki::add_field( 'wpstarter', [
		'type'        => 'toggle',
		'settings'    => 'wpstarter_blog_featured',
		'label'       => esc_html__( 'First post featured', 'TRANSLATE' ),
		'description' => esc_html__( 'Is the first post featured?', 'TRANSLATE' ),
		'section'     => 'wpstarter_blog',
		'default'     => '1',
	] );

	if ( class_exists( 'Wpstarter_Author_Init' ) ) :

		// Author visibility.
		Kirki::add_field( 'wpstarter', [
			'type'        => 'toggle',
			'settings'    => 'wpstarter_blog_single_author_visibility',
			'label'       => esc_html__( 'Author visibility', 'TRANSLATE' ),
			'description' => esc_html__( 'Show or hide the author on a single blog page.', 'TRANSLATE' ),
			'section'     => 'wpstarter_blog',
			'default'     => '0',
		] );

	endif;

	// Releated posts visibility.
	Kirki::add_field( 'wpstarter', [
		'type'        => 'toggle',
		'settings'    => 'wpstarter_blog_single_releated_posts_visibility',
		'label'       => esc_html__( 'Releated posts visibility', 'TRANSLATE' ),
		'description' => esc_html__( 'Show or hide the releated posts on a single blog page.', 'TRANSLATE' ),
		'section'     => 'wpstarter_blog',
		'default'     => '0',
	] );

	// Releated posts heading text.
	Kirki::add_field( 'wpstarter', [
		'type'     => 'text',
		'settings' => 'wpstarter_blog_single_releated_posts_heading_text',
		'label'    => esc_html__( 'Releated posts heading text', 'TRANSLATE' ),
		'section'  => 'wpstarter_blog',
		'default'  => esc_html__( 'You may also like', 'TRANSLATE' ),
	] );

	// Sticky sidebar.
	Kirki::add_field( 'wpstarter', [
		'type'        => 'toggle',
		'settings'    => 'wpstarter_blog_sticky_sidebar',
		'label'       => esc_html__( 'Sticky sidebar', 'TRANSLATE' ),
		'description' => esc_html__( 'Is the sidebar sticky?', 'TRANSLATE' ),
		'section'     => 'wpstarter_blog',
		'default'     => '0',
	] );

endif;
