<?php
/**
 * Front Page slider Customizer options.
 *
 * @since 1.0.0
 *
 * @package wpstarter
 */

if ( class_exists( 'Kirki' ) ) :

	// Slider visibility.
	Kirki::add_field( 'wpstarter', [
		'type'        => 'toggle',
		'settings'    => 'wpstarter_front_page_slider_visibility',
		'label'       => esc_html__( 'Slider visibility', 'TRANSLATE' ),
		'description' => esc_html__( 'Show or hide the slider.', 'TRANSLATE' ),
		'section'     => 'wpstarter_front_page_slider',
		'default'     => '0',
	] );

	// Number of slides.
	Kirki::add_field( 'wpstarter', [
		'type'        => 'number',
		'settings'    => 'wpstarter_front_page_slider_slides_num',
		'label'       => esc_html__( 'Number of slides', 'TRANSLATE' ),
		'description' => esc_html__( 'Maximum 10.', 'TRANSLATE' ),
		'section'     => 'wpstarter_front_page_slider',
		'default'     => 5,
		'choices'     => [
			'min'  => 1,
			'max'  => 10,
			'step' => 1,
		],
	] );

	// Rewind.
	Kirki::add_field( 'wpstarter', [
		'type'        => 'toggle',
		'settings'    => 'wpstarter_front_page_slider_rewind',
		'label'       => esc_html__( 'Rewind', 'TRANSLATE' ),
		'description' => esc_html__( 'When the last slide is active and you click the next slide, you will go back to the first slide. Similarly, if the first one is active and you click the previous one, you activate the last one.', 'TRANSLATE' ),
		'section'     => 'wpstarter_front_page_slider',
		'default'     => '1',
	] );

	// Custom animation.
	Kirki::add_field( 'wpstarter', [
		'type'        => 'toggle',
		'settings'    => 'wpstarter_front_page_slider_custom_anim',
		'label'       => esc_html__( 'Custom animation', 'TRANSLATE' ),
		'description' => esc_html__( 'Change the default animation after clicking the next or previous arrow.', 'TRANSLATE' ),
		'section'     => 'wpstarter_front_page_slider',
		'default'     => '0',
	] );

	// Animation in type.
	Kirki::add_field( 'wpstarter', [
		'type'        => 'select',
		'settings'    => 'wpstarter_front_page_slider_custom_anim_in',
		'label'       => esc_html__( 'Animation in type', 'TRANSLATE' ),
		'description' => esc_html__( 'The type of animation in of your slider when changing the slide (It works only when you click the arrows).', 'TRANSLATE' ),
		'section'     => 'wpstarter_front_page_slider',
		'default'     => '',
		'multiple'    => 1,
		'choices'     => [
			''                   => esc_html__( 'Please Select', 'TRANSLATE' ),
			'anim-fade-in'       => esc_html__( 'Fade In', 'TRANSLATE' ),
			'anim-fade-in-up'    => esc_html__( 'Fade In Up', 'TRANSLATE' ),
			'anim-fade-in-down'  => esc_html__( 'Fade In Down', 'TRANSLATE' ),
			'anim-fade-in-left'  => esc_html__( 'Fade In Left', 'TRANSLATE' ),
			'anim-fade-in-right' => esc_html__( 'Fade In Right', 'TRANSLATE' ),
			'anim-zoom-in'       => esc_html__( 'Zoom In', 'TRANSLATE' ),
		],
		'active_callback' => [
			[
				'setting'  => 'wpstarter_front_page_slider_custom_anim',
				'operator' => '===',
				'value'    => true,
			]
		],
	] );

	// Animation out type.
	Kirki::add_field( 'wpstarter', [
		'type'        => 'select',
		'settings'    => 'wpstarter_front_page_slider_custom_anim_out',
		'label'       => esc_html__( 'Animation out type', 'TRANSLATE' ),
		'description' => esc_html__( 'The type of animation out of your slider when changing the slide (It works only when you click the arrows).', 'TRANSLATE' ),
		'section'     => 'wpstarter_front_page_slider',
		'default'     => '',
		'multiple'    => 1,
		'choices'     => [
			''                    => esc_html__( 'Please Select', 'TRANSLATE' ),
			'anim-fade-out'       => esc_html__( 'Fade Out', 'TRANSLATE' ),
			'anim-fade-out-up'    => esc_html__( 'Fade Out Up', 'TRANSLATE' ),
			'anim-fade-out-down'  => esc_html__( 'Fade Out Down', 'TRANSLATE' ),
			'anim-fade-out-left'  => esc_html__( 'Fade Out Left', 'TRANSLATE' ),
			'anim-fade-out-right' => esc_html__( 'Fade Out Right', 'TRANSLATE' ),
			'anim-zoom-out'       => esc_html__( 'Zoom Out', 'TRANSLATE' ),
		],
		'active_callback' => [
			[
				'setting'  => 'wpstarter_front_page_slider_custom_anim',
				'operator' => '===',
				'value'    => true,
			]
		],
	] );

	// Autoplay slider.
	Kirki::add_field( 'wpstarter', [
		'type'        => 'toggle',
		'settings'    => 'wpstarter_front_page_slider_autoplay',
		'label'       => esc_html__( 'Autoplay slider', 'TRANSLATE' ),
		'description' => esc_html__( 'Set whether the slides are to change automatically. It does not work for mobile phones.', 'TRANSLATE' ),
		'section'     => 'wpstarter_front_page_slider',
		'default'     => '0',
	] );

	// Autoplay slider time.
	Kirki::add_field( 'wpstarter', [
		'type'        => 'number',
		'settings'    => 'wpstarter_front_page_slider_autoplay_time',
		'label'       => esc_html__( 'Autoplay slider time', 'TRANSLATE' ),
		'description' => esc_html__( 'Time for the next slide. The time is in seconds.', 'TRANSLATE' ),
		'section'     => 'wpstarter_front_page_slider',
		'default'     => 5,
		'choices'     => [
			'min'  => 3,
			'max'  => 10,
			'step' => 1,
		],
		'active_callback' => [
			[
				'setting'  => 'wpstarter_front_page_slider_autoplay',
				'operator' => '===',
				'value'    => true,
			]
		],
	] );

	// Autoplay hover pause.
	Kirki::add_field( 'wpstarter', [
		'type'            => 'toggle',
		'settings'        => 'wpstarter_front_page_slider_autoplay_pause',
		'label'           => esc_html__( 'Autoplay hover pause', 'TRANSLATE' ),
		'description'     => esc_html__( 'Set whether the slider should stop if it is set to autoplay and the user hovers over it with the mouse.', 'TRANSLATE' ),
		'section'         => 'wpstarter_front_page_slider',
		'default'         => '0',
		'active_callback' => [
			[
				'setting'  => 'wpstarter_front_page_slider_autoplay',
				'operator' => '===',
				'value'    => true,
			]
		],
	] );

endif;
