<?php
/**
 * Footer Customizer settings.
 *
 * @since 1.0.0
 *
 * @package wpstarter
 */

if ( class_exists( 'Kirki' ) ) :

	// Subscription.
	if ( class_exists( 'WPCF7' ) ) :

		// Subscription visibility.
		Kirki::add_field( 'wpstarter', [
			'type'        => 'toggle',
			'settings'    => 'wpstarter_footer_subscription_visibility',
			'label'       => esc_html__( 'Subscription visibility', 'TRANSLATE' ),
			'description' => esc_html__( 'Show or hide the subscription section.', 'TRANSLATE' ),
			'section'     => 'wpstarter_footer',
			'default'     => '0',
		] );

		// Subscription title.
		Kirki::add_field( 'wpstarter', [
			'type'            => 'text',
			'settings'        => 'wpstarter_footer_subscription_title_text',
			'label'           => esc_html__( 'Subscription title', 'TRANSLATE' ),
			'section'         => 'wpstarter_footer',
			'default'         => esc_html__( 'Sign up to our newsletter', 'TRANSLATE' ),
			'active_callback' => [
				[
					'setting'  => 'wpstarter_footer_subscription_visibility',
					'operator' => '===',
					'value'    => true,
				]
			],
		] );

		$forms[0]  = esc_html__( 'Please Select', 'TRANSLATE' );
		$get_forms = get_posts( [
			'post_type'      => 'wpcf7_contact_form',
			'posts_per_page' => -1
		] );

		foreach ( $get_forms as $form ) :
			$forms[ $form->ID ] = esc_attr( $form->post_title );
		endforeach;

		// Forms.
		Kirki::add_field( 'wpstarter', [
			'type'        => 'select',
			'settings'    => 'wpstarter_footer_subscription_forms',
			'label'       => esc_html__( 'Subscription form', 'TRANSLATE' ),
			'description' => esc_html__( 'Select the form.', 'TRANSLATE' ),
			'section'     => 'wpstarter_footer',
			'default'     => 0,
			'choices'     => $forms,
		] );

	endif;

	// Instagram visibility.
	Kirki::add_field( 'wpstarter', [
		'type'        => 'toggle',
		'settings'    => 'wpstarter_footer_instagram_visibility',
		'label'       => esc_html__( 'Instagram visibility', 'TRANSLATE' ),
		'description' => esc_html__( 'Show or hide the Instagram section.', 'TRANSLATE' ),
		'section'     => 'wpstarter_footer',
		'default'     => '0',
	] );

	Kirki::add_field( 'wpstarter', [
		'type'            => 'custom',
		'settings'        => 'wpstarter_footer_instagram_info_1',
		'label'           => esc_html__( 'What is the Instagram access token?', 'TRANSLATE' ),
		'section'         => 'wpstarter_footer',
		'default'         => esc_html__( 'Thanks to the access token, we can send a request to Instagram and securely retrieve our photos. This is required to show your photos.', 'TRANSLATE' ),
		'active_callback' => [
			[
				'setting'  => 'wpstarter_footer_instagram_visibility',
				'operator' => '===',
				'value'    => true,
			]
		],
	] );

	Kirki::add_field( 'wpstarter', [
		'type'     => 'custom',
		'settings' => 'wpstarter_footer_instagram_info_2',
		'label'    => esc_html__( 'Generate access token', 'TRANSLATE' ),
		'section'  => 'wpstarter_footer',
		'default'  => sprintf(
			// translators: %1$s: The beginning of the HTML code for the link tag.
			// translators: %2$s: The ending of the HTML code for the link tag.
			esc_html__( '%1$sClick here%2$s to generate an Instagram Access Token and then paste it in the field below.', 'TRANSLATE' ),
			'<a href="https://greathemes.com/get-instagram-token/" target="_blank">',
			'</a>'
		),
		'active_callback' => [
			[
				'setting'  => 'wpstarter_footer_instagram_visibility',
				'operator' => '===',
				'value'    => true,
			]
		],
	] );

	// Instagram access token.
	Kirki::add_field( 'wpstarter', [
		'type'            => 'text',
		'settings'        => 'wpstarter_footer_instagram_access_token',
		'label'           => esc_html__( 'Instagram access token', 'TRANSLATE' ),
		'description'     => esc_html__( 'Paste the generated token here.', 'TRANSLATE' ),
		'section'         => 'wpstarter_footer',
		'default'         => '',
		'active_callback' => [
			[
				'setting'  => 'wpstarter_footer_instagram_visibility',
				'operator' => '===',
				'value'    => true,
			]
		],
	] );

	// Instagram new tab.
	Kirki::add_field( 'wpstarter', [
		'type'            => 'toggle',
		'settings'        => 'wpstarter_footer_instagram_new_tab',
		'label'           => esc_html__( 'Instagram new tab', 'TRANSLATE' ),
		'description'     => esc_html__( "After clicking on Instagram's image, open the website in a new window.", 'TRANSLATE' ),
		'section'         => 'wpstarter_footer',
		'default'         => '1',
		'active_callback' => [
			[
				'setting'  => 'wpstarter_footer_instagram_visibility',
				'operator' => '===',
				'value'    => true,
			]
		],
	] );

	// Back to top button visibility.
	Kirki::add_field( 'wpstarter', [
		'type'        => 'toggle',
		'settings'    => 'wpstarter_footer_back_to_top_btn_visibility',
		'label'       => esc_html__( 'Back to top button visibility', 'TRANSLATE' ),
		'description' => esc_html__( 'Show or hide the back to top button.', 'TRANSLATE' ),
		'section'     => 'wpstarter_footer',
		'default'     => '1',
	] );

	// Social media.
	Kirki::add_field( 'wpstarter', [
		'type'        => 'repeater',
		'settings'    => 'wpstarter_footer_social_media',
		'label'       => esc_html__( 'Social media', 'TRANSLATE' ),
		'description' => esc_html__( 'Add a new item, select a website from the list and add a link.', 'TRANSLATE' ),
		'section'     => 'wpstarter_footer',
		'row_label'   => [
			'type'  => 'field',
			'field' => 'website',
		],
		'button_label' => esc_html__( 'Add new', 'TRANSLATE' ),
		'default'      => [
			[
				'website' => 0,
				'url'     => '',
			],
		],
		'fields' => [
			'website'  => [
				'type'    => 'select',
				'label'   => esc_html__( 'Website', 'TRANSLATE' ),
				'default' => 0,
				'choices' => wpstarter_get_social_media_arr(),
			],
			'url' => [
				'type'    => 'link',
				'label'   => esc_html__( 'Link', 'TRANSLATE' ),
				'default' => '',
			]
		]
	] );

	// Social media new tab.
	Kirki::add_field( 'wpstarter', [
		'type'        => 'toggle',
		'settings'    => 'wpstarter_footer_social_media_new_tab',
		'label'       => esc_html__( 'Open a new tab', 'TRANSLATE' ),
		'description' => esc_html__( 'After clicking on social media link, open a new tab.', 'TRANSLATE' ),
		'section'     => 'wpstarter_footer',
		'default'     => '1',
	] );

	// Copyright text.
	Kirki::add_field( 'wpstarter', [
		'type'     => 'editor',
		'settings' => 'wpstarter_footer_copyright_text',
		'label'    => esc_html__( 'Copyright text', 'TRANSLATE' ),
		'section'  => 'wpstarter_footer',
		'default'  => '',
	] );

endif;
