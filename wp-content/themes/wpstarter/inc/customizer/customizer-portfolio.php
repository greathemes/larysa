<?php
/**
 * Portfolio Customizer settings.
 *
 * @since 1.0.0
 *
 * @package wpstarter
 */

if ( class_exists( 'Kirki' ) && class_exists( 'Wpstarter_Portfolio' ) ) :

	// Portfolio layout.
	Kirki::add_field( 'wpstarter', [
		'type'        => 'radio',
		'settings'    => 'wpstarter_portfolio_layout',
		'label'       => esc_html__( 'Portfolio layout', 'TRANSLATE' ),
		'section'     => 'wpstarter_portfolio',
		'default'     => 'grid-2-col',
		'choices'     => [
			'grid-2-col'    => esc_html__( 'Grid 2 columns', 'TRANSLATE' ),
			'grid-3-col'    => esc_html__( 'Grid 3 columns', 'TRANSLATE' ),
			'masonry-2-col' => esc_html__( 'Masonry 2 columns', 'TRANSLATE' ),
			'masonry-3-col' => esc_html__( 'Masonry 3 columns', 'TRANSLATE' ),
		],
	] );

	// Filter visibility.
	Kirki::add_field( 'wpstarter', [
		'type'        => 'toggle',
		'settings'    => 'wpstarter_portfolio_filter_visibility',
		'label'       => esc_html__( 'Filter visibility', 'TRANSLATE' ),
		'description' => esc_html__( 'Show or hide the category filter buttons.', 'TRANSLATE' ),
		'section'     => 'wpstarter_portfolio',
		'default'     => '1',
	] );

	// Filter button text.
	Kirki::add_field( 'wpstarter', [
		'type'            => 'text',
		'settings'        => 'wpstarter_portfolio_filter_all_btn_text',
		'label'           => esc_html__( 'Filter button text', 'TRANSLATE' ),
		'description'     => esc_html__( 'Text for the button showing all albums.', 'TRANSLATE' ),
		'section'         => 'wpstarter_portfolio',
		'default'         => esc_html__( 'All', 'TRANSLATE' ),
		'active_callback' => [
			[
				'setting'  => 'wpstarter_portfolio_filter_visibility',
				'operator' => '===',
				'value'    => true,
			]
		],
	] );

	// Load more button text.
	Kirki::add_field( 'wpstarter', [
		'type'        => 'text',
		'settings'    => 'wpstarter_portfolio_load_more_btn_text',
		'label'       => esc_html__( 'Load more button text', 'TRANSLATE' ),
		'description' => esc_html__( 'Text for the load more albums button.', 'TRANSLATE' ),
		'section'     => 'wpstarter_portfolio',
		'default'     => esc_html__( 'More albums', 'TRANSLATE' ),
	] );

	// Custom number of images.
	Kirki::add_field( 'wpstarter', [
		'type'        => 'toggle',
		'settings'    => 'wpstarter_portfolio_custom_images_num',
		'label'       => esc_html__( 'Custom number of images', 'TRANSLATE' ),
		'description' => esc_html__( 'This setting determines the number of photos displayed at once and how many consecutive photos appear after clicking the load more button. By default, this is the number set in the Settings -> Reading -> Blog pages show at most tab. Here you can set your own number.', 'TRANSLATE' ),
		'section'     => 'wpstarter_portfolio',
		'default'     => '0',
	] );

	// Number of images.
	Kirki::add_field( 'wpstarter', [
		'type'        => 'number',
		'settings'    => 'wpstarter_portfolio_images_num',
		'label'       => esc_html__( 'Number of images', 'TRANSLATE' ),
		'description' => esc_html__( 'It is recommended that this number is not too high. For example, 15 would be good.', 'TRANSLATE' ),
		'section'     => 'wpstarter_portfolio',
		'default'     => 5,
		'choices'     => [
			'min'  => 1,
			'max'  => 50,
			'step' => 1,
		],
		'active_callback' => [
			[
				'setting'  => 'wpstarter_portfolio_custom_images_num',
				'operator' => '===',
				'value'    => true,
			]
		],
	] );

	// Releated albums visibility.
	Kirki::add_field( 'wpstarter', [
		'type'        => 'toggle',
		'settings'    => 'wpstarter_portfolio_single_releated_albums_visibility',
		'label'       => esc_html__( 'Releated albums visibility', 'TRANSLATE' ),
		'description' => esc_html__( 'Show or hide the releated albums on a single portfolio page.', 'TRANSLATE' ),
		'section'     => 'wpstarter_portfolio',
		'default'     => '0',
	] );

	// Releated albums heading text.
	Kirki::add_field( 'wpstarter', [
		'type'            => 'text',
		'settings'        => 'wpstarter_portfolio_single_releated_albums_heading_text',
		'label'           => esc_html__( 'Releated albums heading text', 'TRANSLATE' ),
		'section'         => 'wpstarter_portfolio',
		'default'         => esc_html__( 'Releated albums', 'TRANSLATE' ),
		'active_callback' => [
			[
				'setting'  => 'single_portfolio_releated_albums_visibility',
				'operator' => '===',
				'value'    => true,
			]
		],
	] );

endif;
