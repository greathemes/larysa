<?php
/**
 * Main header Customizer options.
 *
 * @since 1.0.0
 *
 * @package wpstarter
 */

if ( class_exists( 'Kirki' ) ) :

	// Nav breakpoint.
	Kirki::add_field( 'wpstarter', [
		'type'        => 'slider',
		'settings'    => 'wpstarter_header_nav_breakpoint',
		'label'       => esc_html__( 'Navigation breakpoint', 'TRANSLATE' ),
		'description' => esc_html__( 'Choose the breakpoint when your menu turns into a hamburger button. The breakpoint is in pixels.', 'TRANSLATE' ),
		'section'     => 'wpstarter_header',
		'default'     => 900,
		'choices'     => [
			'min'  => 0,
			'max'  => 3000,
			'step' => 10,
		],
	] );

	add_filter( 'kirki_wpstarter_dynamic_css', function( $styles ) {

		$nav_breakpoint = get_theme_mod( 'wpstarter_header_nav_breakpoint', 900 );

		$styles .= "@media only screen and (min-width:{$nav_breakpoint}px){.primary-header-desktop-icons__social-media-list,.primary-header-desktop-icons__search-show-btn::before,.primary-header-desktop-nav{display: block;} .primary-header-desktop__show-btn{display:none;}}";

		return $styles;

	} );

	// Search visibility.
	Kirki::add_field( 'wpstarter', [
		'type'        => 'toggle',
		'settings'    => 'wpstarter_header_search_visibility',
		'label'       => esc_html__( 'Search visibility', 'TRANSLATE' ),
		'description' => esc_html__( 'Show or hide the search icon in navigation.', 'TRANSLATE' ),
		'section'     => 'wpstarter_header',
		'default'     => '1',
	] );

	// Social media.
	Kirki::add_field( 'wpstarter', [
		'type'        => 'repeater',
		'settings'    => 'wpstarter_header_social_media',
		'label'       => esc_html__( 'Social media', 'TRANSLATE' ),
		'description' => esc_html__( 'Add a new item, select a website from the list and add a link.', 'TRANSLATE' ),
		'section'     => 'wpstarter_header',
		'row_label'   => [
			'type'  => 'field',
			'field' => 'website',
		],
		'button_label' => esc_html__( 'Add new', 'TRANSLATE' ),
		'default'      => [
			[
				'website' => 0,
				'url'     => '',
			],
		],
		'fields' => [
			'website'  => [
				'type'    => 'select',
				'label'   => esc_html__( 'Website', 'TRANSLATE' ),
				'default' => 0,
				'choices' => wpstarter_get_social_media_arr(),
			],
			'url' => [
				'type'    => 'link',
				'label'   => esc_html__( 'Link', 'TRANSLATE' ),
				'default' => '',
			]
		]
	] );

	// Social media new tab.
	Kirki::add_field( 'wpstarter', [
		'type'        => 'toggle',
		'settings'    => 'wpstarter_header_social_media_new_tab',
		'label'       => esc_html__( 'Open a new tab (social media)', 'TRANSLATE' ),
		'description' => esc_html__( 'After clicking on social media link, open a new tab.', 'TRANSLATE' ),
		'section'     => 'wpstarter_header',
		'default'     => '1',
	] );

endif;
