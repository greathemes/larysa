<?php
/**
 * Portfolio Customizer options.
 *
 * @since 1.0.0
 *
 * @package wpstarter
 */

if ( class_exists( 'Kirki' ) && class_exists( 'Wpstarter_Portfolio' ) ) :

	// Portfolio visibility.
	Kirki::add_field( 'wpstarter', [
		'type'        => 'toggle',
		'settings'    => 'wpstarter_front_page_portfolio_visibility',
		'label'       => esc_html__( 'Portfolio visibility', 'TRANSLATE' ),
		'description' => esc_html__( 'Show or hide the portfolio.', 'TRANSLATE' ),
		'section'     => 'wpstarter_front_page_portfolio',
		'default'     => '0',
	] );

	// Heading text.
	Kirki::add_field( 'wpstarter', [
		'type'     => 'text',
		'settings' => 'wpstarter_front_page_portfolio_heading_text',
		'label'    => esc_html__( 'Heading text', 'TRANSLATE' ),
		'section'  => 'wpstarter_front_page_portfolio',
		'default'  => esc_html__( 'Portfolio', 'TRANSLATE' ),
	] );

	// Albums.
	$albums     = [ 0 => esc_html__( 'Please Select', 'TRANSLATE' ) ];
	$get_albums = get_posts( 'post_type=portfolio&posts_per_page=-1' );

	foreach ( $get_albums as $album ) :
		$albums[ $album->ID ] = $album->post_title;
	endforeach;

	Kirki::add_field( 'wpstarter', [
		'type'      => 'repeater',
		'settings'  => 'wpstarter_front_page_portfolio_albums',
		'label'     => esc_html__( 'Choose your albums', 'TRANSLATE' ),
		'section'   => 'wpstarter_front_page_portfolio',
		'row_label' => [
			'type'  => 'field',
			'field' => 'album',
		],
		'button_label' => esc_html__( 'Add new album', 'TRANSLATE' ),
		'default'      => [
			[
				'album'   => 0,
				'columns' => 'col-1',
			],
		],
		'fields' => [
			'album' => [
				'type'    => 'select',
				'label'   => esc_html__( 'The title of the album.', 'TRANSLATE' ),
				'default' => 0,
				'choices' => $albums,
			],
			'columns'  => [
				'type'        => 'select',
				'label'       => esc_html__( 'The width of the element.', 'TRANSLATE' ),
				'description' => esc_html__( 'The best effect you get if the majority will have the width of one column and a few two columns.', 'TRANSLATE' ),
				'default'     => 'col-1',
				'choices'     => [
					'col-1' => esc_html__( '1 Column', 'TRANSLATE' ),
					'col-2' => esc_html__( '2 Columns', 'TRANSLATE' ),
				],
			],
		]
	] );

	// Button visibility.
	Kirki::add_field( 'wpstarter', [
		'type'        => 'toggle',
		'settings'    => 'wpstarter_front_page_portfolio_btn_visibility',
		'label'       => esc_html__( 'Button visibility', 'TRANSLATE' ),
		'description' => esc_html__( 'Show or hide the button as the last element in the portfolio (1 column).', 'TRANSLATE' ),
		'section'     => 'wpstarter_front_page_portfolio',
		'default'     => '0',
	] );

	// Button text.
	Kirki::add_field( 'wpstarter', [
		'type'            => 'text',
		'settings'        => 'wpstarter_front_page_portfolio_btn_text',
		'label'           => esc_html__( 'Button text', 'TRANSLATE' ),
		'section'         => 'wpstarter_front_page_portfolio',
		'default'         => '',
		'active_callback' => [
			[
				'setting'  => 'wpstarter_front_page_portfolio_btn_visibility',
				'operator' => '===',
				'value'    => true,
			]
		],
	] );

	$pages[0]  = esc_html__( 'Please Select', 'TRANSLATE' );
	$get_pages = get_pages();

	foreach ( $get_pages as $get_page ) :
		$pages[ $get_page->ID ] = esc_attr( $get_page->post_title );
	endforeach;

	// Button page url.
	Kirki::add_field( 'wpstarter', [
		'type'            => 'select',
		'settings'        => 'wpstarter_front_page_portfolio_btn_page_url',
		'label'           => esc_html__( 'Button page url', 'TRANSLATE' ),
		'description'     => esc_html__( 'Select the page to which the button links.', 'TRANSLATE' ),
		'section'         => 'wpstarter_front_page_portfolio',
		'default'         => 0,
		'choices'         => $pages,
		'active_callback' => [
			[
				'setting'  => 'wpstarter_front_page_portfolio_btn_visibility',
				'operator' => '===',
				'value'    => true,
			]
		],
	] );

endif;
