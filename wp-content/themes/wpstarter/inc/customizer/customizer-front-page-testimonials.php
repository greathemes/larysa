<?php
/**
 * Front Page testimonials Customizer settings.
 *
 * @since 1.0.0
 *
 * @package wpstarter
 */

if ( class_exists( 'Kirki' ) ) :

	// Testimonials visibility.
	Kirki::add_field( 'wpstarter', [
		'type'        => 'toggle',
		'settings'    => 'wpstarter_front_page_testimonials_visibility',
		'label'       => esc_html__( 'Testimonials visibility', 'TRANSLATE' ),
		'description' => esc_html__( 'Show or hide the testimonials.', 'TRANSLATE' ),
		'section'     => 'wpstarter_front_page_testimonials',
		'default'     => '0',
	] );

	// Heading text.
	Kirki::add_field( 'wpstarter', [
		'type'     => 'text',
		'settings' => 'wpstarter_front_page_testimonials_heading_text',
		'label'    => esc_html__( 'Heading text', 'TRANSLATE' ),
		'section'  => 'wpstarter_front_page_testimonials',
		'default'  => esc_html__( 'Testimonials', 'TRANSLATE' ),
	] );

	// Testimonials.
	Kirki::add_field( 'wpstarter', [
		'type'        => 'repeater',
		'settings'    => 'wpstarter_front_page_testimonials_items',
		'label'       => esc_html__( 'Add testimonials', 'TRANSLATE' ),
		'description' => esc_html__( 'Author and quote are required to view the review, but for the best visual effects it is better to complete all fields.', 'TRANSLATE' ),
		'section'     => 'wpstarter_front_page_testimonials',
		'row_label'   => [
			'type'  => 'field',
			'field' => 'author',
		],
		'button_label' => esc_html__( 'Add new testimonial', 'TRANSLATE' ),
		'default'      => [
			[
				'author' => '',
				'job'    => '',
				'quote'  => '',
				'img'    => '',
				'stars'  => 0,
			],
		],
		'fields' => [
			'author' => [
				'type'    => 'text',
				'label'   => esc_html__( 'Author', 'TRANSLATE' ),
				'default' => '',
			],
			'job' => [
				'type'    => 'text',
				'label'   => esc_html__( 'Job', 'TRANSLATE' ),
				'default' => '',
			],
			'quote' => [
				'type'    => 'textarea',
				'label'   => esc_html__( 'Quote', 'TRANSLATE' ),
				'default' => '',
			],
			'img' => [
				'type'    => 'image',
				'label'   => esc_html__( 'Picture', 'TRANSLATE' ),
				'default' => '',
				'choices' => [
					'save_as' => 'id',
				],
			],
			'stars'  => [
				'type'    => 'select',
				'label'   => esc_html__( 'Stars', 'TRANSLATE' ),
				'default' => 0,
				'choices' => [
					0    => esc_html__( 'Please Select', 'TRANSLATE' ),
					'1'  => esc_html__( '0 and half star', 'TRANSLATE' ),
					'2'  => esc_html__( '1 star', 'TRANSLATE' ),
					'3'  => esc_html__( '1 and a half stars', 'TRANSLATE' ),
					'4'  => esc_html__( '2 stars', 'TRANSLATE' ),
					'5'  => esc_html__( '2 and a half stars', 'TRANSLATE' ),
					'6'  => esc_html__( '3 stars', 'TRANSLATE' ),
					'7'  => esc_html__( '3 and a half stars', 'TRANSLATE' ),
					'8'  => esc_html__( '4 stars', 'TRANSLATE' ),
					'9'  => esc_html__( '4 and a half stars', 'TRANSLATE' ),
					'10' => esc_html__( '5 stars', 'TRANSLATE' ),
				],
			],
		]
	] );

	// Rewind.
	Kirki::add_field( 'wpstarter', [
		'type'        => 'toggle',
		'settings'    => 'wpstarter_front_page_testimonials_slider_rewind',
		'label'       => esc_html__( 'Rewind', 'TRANSLATE' ),
		'description' => esc_html__( 'When the last slide is active and you click the next slide, you will go back to the first slide. Similarly, if the first one is active and you click the previous one, you activate the last one.', 'TRANSLATE' ),
		'section'     => 'wpstarter_front_page_testimonials',
		'default'     => '1',
	] );

	// Custom animation.
	Kirki::add_field( 'wpstarter', [
		'type'        => 'toggle',
		'settings'    => 'wpstarter_front_page_testimonials_slider_custom_anim',
		'label'       => esc_html__( 'Custom animation', 'TRANSLATE' ),
		'description' => esc_html__( 'Change the default animation after clicking the next or previous arrow.', 'TRANSLATE' ),
		'section'     => 'wpstarter_front_page_testimonials',
		'default'     => '0',
	] );

	// Animation in type.
	Kirki::add_field( 'wpstarter', [
		'type'        => 'select',
		'settings'    => 'wpstarter_front_page_testimonials_slider_custom_anim_in',
		'label'       => esc_html__( 'Animation in type', 'TRANSLATE' ),
		'description' => esc_html__( 'The type of animation in of your slider when changing the slide (It works only when you click the arrows).', 'TRANSLATE' ),
		'section'     => 'wpstarter_front_page_testimonials',
		'default'     => '',
		'multiple'    => 1,
		'choices'     => [
			''                   => esc_html__( 'Please Select', 'TRANSLATE' ),
			'anim-fade-in'       => esc_html__( 'Fade In', 'TRANSLATE' ),
			'anim-fade-in-up'    => esc_html__( 'Fade In Up', 'TRANSLATE' ),
			'anim-fade-in-down'  => esc_html__( 'Fade In Down', 'TRANSLATE' ),
			'anim-fade-in-left'  => esc_html__( 'Fade In Left', 'TRANSLATE' ),
			'anim-fade-in-right' => esc_html__( 'Fade In Right', 'TRANSLATE' ),
			'anim-zoom-in'       => esc_html__( 'Zoom In', 'TRANSLATE' ),
		],
		'active_callback' => [
			[
				'setting'  => 'wpstarter_front_page_testimonials_slider_custom_anim',
				'operator' => '===',
				'value'    => true,
			]
		],
	] );

	// Animation out type.
	Kirki::add_field( 'wpstarter', [
		'type'        => 'select',
		'settings'    => 'wpstarter_front_page_testimonials_slider_custom_anim_out',
		'label'       => esc_html__( 'Animation out type', 'TRANSLATE' ),
		'description' => esc_html__( 'The type of animation out of your slider when changing the slide (It works only when you click the arrows).', 'TRANSLATE' ),
		'section'     => 'wpstarter_front_page_testimonials',
		'default'     => '',
		'multiple'    => 1,
		'choices'     => [
			''                    => esc_html__( 'Please Select', 'TRANSLATE' ),
			'anim-fade-out'       => esc_html__( 'Fade Out', 'TRANSLATE' ),
			'anim-fade-out-up'    => esc_html__( 'Fade Out Up', 'TRANSLATE' ),
			'anim-fade-out-down'  => esc_html__( 'Fade Out Down', 'TRANSLATE' ),
			'anim-fade-out-left'  => esc_html__( 'Fade Out Left', 'TRANSLATE' ),
			'anim-fade-out-right' => esc_html__( 'Fade Out Right', 'TRANSLATE' ),
			'anim-zoom-out'       => esc_html__( 'Zoom Out', 'TRANSLATE' ),
		],
		'active_callback' => [
			[
				'setting'  => 'wpstarter_front_page_testimonials_slider_custom_anim',
				'operator' => '===',
				'value'    => true,
			]
		],
	] );

	// Autoplay slider.
	Kirki::add_field( 'wpstarter', [
		'type'        => 'toggle',
		'settings'    => 'wpstarter_front_page_testimonials_slider_autoplay',
		'label'       => esc_html__( 'Autoplay slider', 'TRANSLATE' ),
		'description' => esc_html__( 'Set whether the slides are to change automatically.', 'TRANSLATE' ),
		'section'     => 'wpstarter_front_page_testimonials',
		'default'     => '0',
	] );

	// Autoplay slider time.
	Kirki::add_field( 'wpstarter', [
		'type'        => 'number',
		'settings'    => 'wpstarter_front_page_testimonials_slider_autoplay_time',
		'label'       => esc_html__( 'Autoplay slider time', 'TRANSLATE' ),
		'description' => esc_html__( 'Time for the next slide. The time is in seconds.', 'TRANSLATE' ),
		'section'     => 'wpstarter_front_page_testimonials',
		'default'     => 5,
		'choices'     => [
			'min'  => 3,
			'max'  => 10,
			'step' => 1,
		],
		'active_callback' => [
			[
				'setting'  => 'wpstarter_front_page_testimonials_slider_autoplay',
				'operator' => '===',
				'value'    => true,
			]
		],
	] );

	// Autoplay hover pause.
	Kirki::add_field( 'wpstarter', [
		'type'            => 'toggle',
		'settings'        => 'wpstarter_front_page_testimonials_slider_autoplay_pause',
		'label'           => esc_html__( 'Autoplay hover pause', 'TRANSLATE' ),
		'description'     => esc_html__( 'Set whether the slider should stop if it is set to autoplay and the user hovers over it with the mouse.', 'TRANSLATE' ),
		'section'         => 'wpstarter_front_page_testimonials',
		'default'         => '0',
		'active_callback' => [
			[
				'setting'  => 'wpstarter_front_page_testimonials_slider_autoplay',
				'operator' => '===',
				'value'    => true,
			]
		],
	] );

endif;
