<?php
/**
 * Custom template & tag functions.
 */

if ( ! function_exists( 'wpstarter_header_primary_navigation' ) ) :

	/**
	 * Displays the primary navigation.
	 */
	function wpstarter_header_primary_navigation( array $args = [] ) {

		if ( has_nav_menu( 'primary' ) ) :

			$class = isset( $args['class'] ) ? $args['class'] : '';
			$is_kirki     = class_exists( 'Kirki' ) ? '' : 'no-kirki'; ?>

			<nav class='<?php echo esc_attr( "$class $is_kirki" ) ?>'>

				<div class='<?php echo esc_attr( "{$class}__container" ) ?>'>

					<?php wp_nav_menu( [
						'theme_location' => 'primary',
						'container'      => false,
						'items_wrap'     => "<ul class='{$class}__list'>" . '%3$s' . "</ul>",
						'walker'         => new Wpstarter_Walker_Primary( $class, true ),
					] ); ?>

				</div>

			</nav>

		<?php endif; ?>

	<?php }

endif;

if ( ! function_exists( 'wpstarter_primary_header_social_media' ) ) :

	/**
	 * Displays social media.
	 */
	function wpstarter_primary_header_social_media( array $args = [] ) {

		$header       = wpstarter_get_options_header();
		$items        = $header['social_media'];
		$new_tab      = $header['social_media_new_tab'] ? 'target="_blank"' : '';
		$class = isset( $args['class'] ) ? $args['class'] : '';
		$is_kirki     = class_exists( 'Kirki' ) ? '' : 'no-kirki';

		if ( wpstarter_kirki_repeater_check_required_fields( $items, [ 'website', 'url' ] ) ) : ?>

			<ul class='<?php echo esc_attr( "{$class}__social-media-list $is_kirki" ); ?>'>

				<?php foreach ( $items as $item ) :

					$slug = $item['website'];
					$url  = $item['url'];

					if ( $slug && $url ) : ?>

						<li class='<?php echo esc_attr( "{$class}__social-media-item" ); ?>'>
							<a class='<?php echo esc_attr( "{$class}__social-media-link" ); ?>' href='<?php echo esc_url( $url ); ?>' <?php echo esc_attr( $new_tab ); ?>>
								<span class='<?php echo esc_attr( "{$class}__social-media-icon fab fa-{$slug}" ); ?>' aria-hidden='true'></span>
							</a>
						</li>

					<?php endif;

				endforeach; ?>

			</ul>

		<?php endif;

	}

endif;

if ( ! function_exists( 'wpstarter_primary_header_search' ) ) :

	/**
	 * Displays search.
	 */
	function wpstarter_primary_header_search( array $args = [] ) {
		
		$header              = wpstarter_get_options_header();
		$search_visibility   = $header['search_visibility'];
		$header_social_media = wpstarter_kirki_repeater_check_required_fields( $header['social_media'], [ 'website', 'url' ] );
		$class        = isset( $args['class'] ) ? $args['class'] : '';
		$icon_or_description = isset( $args['icon_or_description'] ) ? $args['icon_or_description'] : true;

		if ( $search_visibility ) : ?>

			<button type='button' class='<?php echo esc_attr( "{$class}__search-show-btn" ), ' ', esc_attr( $header_social_media ? 'line' : '' ); ?>'>
				<?php if ( $icon_or_description ) : ?>
					<span class='<?php echo esc_attr( "{$class}__search-show-btn-icon fas fa-search" ) ?>' aria-hidden='true'></span>
					<span class='screen-reader-text'><?php esc_html_e( 'Show search engine', 'TRANSLATE' ); ?></span>
				<?php else : ?>
					<?php esc_html_e( 'Search', 'TRANSLATE' ); ?>
				<?php endif; ?>
			</button>

			<div class='<?php echo esc_attr( "{$class}__search-container {$class}__search-container--is-hidden" ) ?>'>
				<div class='wrapper'>
					<div class='<?php echo esc_attr( "{$class}__search-inner-container" ); ?>'>

						<button type='button' class='<?php echo esc_attr( "{$class}__search-hide-btn {$class}__search-hide-btn--is-hidden" ) ?>'>
							<span class='<?php echo esc_attr( "{$class}__search-hide-btn-icon fas fa-times" ) ?>' aria-hidden='true'></span>
							<span class='screen-reader-text'><?php esc_html_e( 'Hide search engine', 'TRANSLATE' ); ?></span>
						</button>

						<h3 class='<?php echo esc_attr( "{$class}__search-heading {$class}__search-heading--is-hidden" ) ?>'><?php esc_html_e( 'What are you looking for?', 'TRANSLATE' ); ?></h3>

						<?php get_search_form(); ?>

					</div>
				</div>
			</div>

		<?php endif; 

	}

endif;

if ( ! function_exists( 'wpstarter_site_content_start' ) ) :

	/**
	 * Displays the beggining of the content.
	 */
	function wpstarter_site_content_start() { ?>

		<div id='site-area' class='site-area'>

	<?php }

endif;

if ( ! function_exists( 'wpstarter_featured_image_area' ) ) :

	function wpstarter_featured_image_area() {

		$class           = 'featured-img-area';
		$id              = is_home() ? get_option( 'page_for_posts' ) : get_the_id();
		$is_featured_img = has_post_thumbnail( $id );
		$img_id          = $is_featured_img ? get_post_thumbnail_id( $id ) : '';

		if ( ! wpstarter_is_static_front_page() && ! wpstarter_is_non_static_front_page() && ! is_archive() && ! is_search() && $is_featured_img && $img_id ) : ?>
		
			<div class='<?php echo esc_attr( "$class" ); ?>'>
				<div class='wrapper'>
					<div class='<?php echo esc_attr( "{$class}__container" ); ?>'>

						<?php if ( $is_featured_img ) :
							wpstarter_post_thumbnail( $img_id, [
								'class' => "{$class}__img {$class}__img--covered" . ( has_post_thumbnail() ? " {$class}__img--" . wpstarter_get_img_orientation( $img_id ) : '' ),
								'size'  => 'wpstarter_post',
								'sizes' => [ 'wpstarter_post_medium', 'wpstarter_post', 'wpstarter_post_medium_retina', 'wpstarter_post_retina' ],
							] );
						endif; ?>

					</div>
				</div>
			</div>

		<?php endif;

	}

endif;

if ( ! function_exists( 'wpstarter_site_content_header_area' ) ) :

	function wpstarter_site_content_header_area() {

		if ( ! wpstarter_is_static_front_page() ) :

			$is_single_portfolio = is_singular( 'portfolio' );
			$is_single_blog      = is_singular( 'post' );
			$class        = is_single() ? 'post-header-area' : 'page-header-area'; ?>

			<div class='<?php echo esc_attr( $class ); ?>'>

					<header class='<?php echo esc_attr( "{$class}__header" ); ?>'>

						<?php if ( $is_single_portfolio ) : ?>

							<div class='<?php echo esc_attr( "{$class}__data-container" ); ?>'>

								<?php	if ( wp_get_post_terms( get_the_id(), 'portfolio_categories' ) ) : ?>

									<div class='post-data-categories'>
										<?php wpstarter_post_categories( get_the_id(), [
											'categories_are_links' => false,
											'class'         => $class,
											'category_slug'        => 'portfolio_categories',
											'is_unstyled'          => true
										] ); ?>
									</div>

								<?php endif; ?>

							</div>

						<?php elseif ( $is_single_blog ) : ?>

							<div class='<?php echo esc_attr( "{$class}__data-container" ); ?>'>

								<span class='<?php echo esc_attr( "{$class}__date-container" ); ?>'>
									<span class='<?php echo esc_attr( "{$class}__date fas fa-calendar-alt" ); ?>'><?php echo esc_html( get_the_date() ); ?></span>
								</span>

								<span class='<?php echo esc_attr( "{$class}__author-container" ); ?>'>
									<?php $author = wpstarter_post_author(); ?>
									<a class='<?php echo esc_attr( "{$class}__author fas fa-user" ); ?>' href='<?php echo esc_url( $author['url'] ) ?>' title='<?php echo esc_attr( $author['title_attr'] ) ?>'>
										<span><?php printf(
											// translators: %s: The name of the author.
											esc_html__( 'By %s', 'TRANSLATE' ), esc_html( $author['name'] )
										); ?></span>
									</a>
								</span>

								<?php if ( comments_open() || get_comments_number() ) : ?>

									<span class='<?php echo esc_attr( "{$class}__comments-container" ); ?>'>
										<a class='<?php echo esc_attr( "{$class}__comment fas fa-comment" ); ?>' href='<?php echo esc_url( get_comments_link() ); ?>' title='<?php esc_attr_e( 'Go to the comments section', 'TRANSLATE' ); ?>'>
											<span><?php printf(
												// translators: %s: Comments number ( First is text to be used if the number is singular, second is text to be used if the number is plural ).
												esc_html( _n( '%s Comment', '%s Comments', get_comments_number(), 'TRANSLATE' ) ), get_comments_number()
											); ?></span>
										</a>
									</span>

								<?php endif; ?>

							</div>

						<?php endif; ?>

						<h1 class='<?php echo esc_attr( "{$class}__heading" ); ?>'>

							<?php if ( is_search() ) :
								printf(
									// translators: %s: Search query.
									esc_html__( 'Search phrase: %s', 'TRANSLATE' ), "<span class='{$class}__search-phrase'>" . get_search_query() . '</span>'
								);
							elseif ( class_exists( 'WooCommerce' ) && is_shop() ) :
								echo wp_kses_post( get_the_title( wc_get_page_id( 'shop' ) ) );
							elseif ( is_archive() ) :
								the_archive_title();
							elseif ( wpstarter_is_blog_posts_page() ) :
								echo esc_html( wpstarter_get_options_front_page_blog()['heading_text'] );
							elseif ( is_404() ) :
								esc_html_e( 'The requested page can not be found', 'TRANSLATE' );
							else :
								echo wp_kses_post( get_the_title( get_queried_object_id() ) );
							endif; ?>

						</h1>

						<?php if ( class_exists( 'ACF' ) && $is_single_portfolio ) :
							$meta_box_content = wpstarter_get_options_portfolio( get_the_id() )[ 'single_content' ]; ?>
							<?php if ( $meta_box_content ) : ?>
								<p class='<?php echo esc_attr( "{$class}__content" ); ?>'><?php wpstarter_textarea_output( $meta_box_content, true ); ?></p>
							<?php endif; ?>
						<?php endif; ?>

					</header>

			</div>

		<?php endif;

	}

endif;

if ( ! function_exists( 'wpstarter_site_content_end' ) ) :

	function wpstarter_site_content_end() { ?>

		</div>

	<?php }

endif;

if ( ! function_exists( 'wpstarter_site_content_area_start' ) ) :

	function wpstarter_site_content_area_start() { ?>

		<div class='content-area wrapper'>

	<?php }

endif;

if ( ! function_exists( 'wpstarter_site_content_primary_area_start' ) ) :

	function wpstarter_site_content_primary_area_start( $page_layout = false ) {

		if ( $page_layout === 'sidebar_left' ) :
			is_singular( 'portfolio' ) ? get_sidebar( 'portfolio' ) : get_sidebar();
		endif; ?>

		<div class='primary-area'>

			<main id='main' class='site-main'>

	<?php }

endif;

if ( ! function_exists( 'wpstarter_site_content_primary_area_before_loop' ) ) :

	/**
	 * Displays the beggining of the content area before loop.
	 */
	function wpstarter_site_content_primary_area_before_loop() {

		if ( ( is_home() || is_archive() || wpstarter_is_non_static_front_page() ) && ! wpstarter_is_static_front_page() ) : ?>

			<section class='blog-page-posts'>
				<?php if ( ! is_paged() && wpstarter_get_options_blog()['featured'] ) : ?>
					<div class='blog-page-posts__featured-inner-container'></div>
				<?php endif; ?>

				<div class='blog-page-posts__inner-container'>

		<?php endif;

	}

endif;

if ( ! function_exists( 'wpstarter_site_content_primary_area_after_loop' ) ) :

	/**
	 * Displays the end of the content area after loop.
	 */
	function wpstarter_site_content_primary_area_after_loop() {

		$blog = wpstarter_is_blog_posts_page() && ! wpstarter_is_static_front_page();

		if ( $blog ) : ?>

				</div>
			</section>

		<?php endif;

		if ( $blog || is_search() ) :

			global $wp_query;

			$is_first_page = ! is_paged();
			$is_last_page  = isset( $wp_query->query['paged'] ) && intval( $wp_query->max_num_pages ) === intval( $wp_query->query['paged'] );
			$inactive_prev = '<span class="pagination-area__page-number pagination-area__page-number--button pagination-area__page-number--is-not-active">' . esc_html__( 'Previous', 'TRANSLATE' ) . '</span>';
			$inactive_next = '<span class="pagination-area__page-number pagination-area__page-number--button pagination-area__page-number--is-not-active">' . esc_html__( 'Next', 'TRANSLATE' ) . '</span>';

			$pagination = get_the_posts_pagination();
			$pagination = str_replace( 'class="navigation pagination"', 'class="pagination-area"', $pagination );
			$pagination = str_replace( 'class="nav-links"', 'class="pagination-area__container"', $pagination );
			$pagination = str_replace( 'page-numbers', 'pagination-area__page-number pagination-area__page-number--button pagination-area__page-number--button-transparent', $pagination );
			$pagination = str_replace( 'pagination-area__page-number pagination-area__page-number--button pagination-area__page-number--button-transparent current', 'pagination-area__page-number pagination-area__page-number--button pagination-area__page-number--button-transparent pagination-area__page-number--current', $pagination );
			$pagination = str_replace( 'pagination-area__page-number pagination-area__page-number--button pagination-area__page-number--button-transparent dots', 'pagination-area__page-number pagination-area__page-number--button pagination-area__page-number--button-transparent pagination-area__page-number--dots', $pagination );
			$pagination = str_replace( 'next pagination-area__page-number pagination-area__page-number--button pagination-area__page-number--button-transparent', 'pagination-area__page-number pagination-area__page-number--button pagination-area__page-number--next', $pagination );
			$pagination = str_replace( 'prev pagination-area__page-number pagination-area__page-number--button pagination-area__page-number--button-transparent', 'pagination-area__page-number pagination-area__page-number--button pagination-area__page-number--prev', $pagination );



			if ( $is_first_page ) :
				$pagination = str_replace( "<span aria-current='page'", $inactive_prev . "<span aria-current='page'", $pagination );
			elseif ( $is_last_page ) :
				$pagination = str_replace( '</div>', $inactive_next . '</div>', $pagination );
			endif;

			echo wp_kses_post( $pagination );

		endif;

	}

endif;

if ( ! function_exists( 'wpstarter_site_content_primary_area_end' ) ) :

	function wpstarter_site_content_primary_area_end( $page_layout = false ) { ?>

			</main>

		</div>

		<?php if ( $page_layout === 'sidebar_right' ) :
			get_sidebar();
		endif;

	}

endif;

if ( ! function_exists( 'wpstarter_site_content_area_end' ) ) :

	function wpstarter_site_content_area_end() { ?>

		</div>

	<?php }

endif;

if ( ! function_exists( 'wpstarter_releated_posts' ) ) :

	/**
	 * Displays releated posts.
	 */
	function wpstarter_releated_posts() {

		$blog = wpstarter_get_options_blog();

		if ( $blog['single_releated_posts_visibility'] && is_singular( 'post' ) ) :

			$heading        = $blog['single_releated_posts_heading_text'];
			$taxonomy       = 'category';
			$id             = get_the_id();
			$cats           = wp_get_object_terms( $id, $taxonomy );
			$cat_ids        = [];
			$tax_query_args = '';
			$class   = 'releated-posts-area';

			foreach ( $cats as $cat ) :
				$cat_ids[] = $cat->term_id;
			endforeach;

			if ( sizeof( $cat_ids ) ) :
				$tax_query_args = [
					'taxonomy' => $taxonomy,
					'field'    => 'term_id',
					'terms'    => $cat_ids
				];
			endif;

			$query = new WP_query(
				[
					'post_type'      => 'post',
					'post__not_in'   => [ $id ],
					'orderby'        => 'rand',
					'posts_per_page' => 4,
					'tax_query'      => [ $tax_query_args ],
				]
			);

			if ( $query->have_posts() ) : ?>

				<section class='<?php echo esc_attr( $class ); ?>'>

					<div class='<?php echo esc_attr( "{$class}__container" ); ?>'>
						
						<?php if ( $heading ) : ?>
							<header class='<?php echo esc_attr( "{$class}__header" ); ?>'>
								<h3 class='<?php echo esc_attr( "{$class}__heading" ); ?>'><?php echo esc_html( $heading ); ?></h3>
							</header>
						<?php endif; ?>

						<div class='<?php echo esc_attr( "{$class}__list" ); ?>'>

							<?php while ( $query->have_posts() ) :

								$query->the_post();

								do_action( 'wpstarter_blog_posts', [
									'index'          => $query->current_post,
									'class'   => $class,
									'excerpt_length' => 20,
									'image_class'    => "{$class}__img {$class}__img--covered"
								] );

							endwhile; ?>

						</div>

					</div>

				</section>

				<?php wp_reset_postdata();

			endif;

		endif;

	}

endif;

if ( ! function_exists( 'wpstarter_site_content_end' ) ) :

	/**
	 * Displays the end of the content.
	 */
	function wpstarter_site_content_end() { ?>

			</div>

		</div>

	<?php }

endif;

if ( ! function_exists( 'wpstarter_blog_post_content' ) ) :

	function wpstarter_blog_post_content( array $args = [] ) {

		$index                              = isset( $args['index'] ) ? $args['index'] : '';
		$class                       = isset( $args['class'] ) ? $args['class'] : '';
		$image_class                        = isset( $args['image_class'] ) ? $args['image_class'] : '';
		$image_size                         = isset( $args['image_size'] ) ? $args['image_size'] : 'wpstarter_post';
		$image_sizes                        = isset( $args['image_sizes'] ) ? $args['image_sizes'] : [ 'wpstarter_post_medium', 'wpstarter_post', 'wpstarter_post_retina' ];
		$lazy                               = isset( $args['lazy'] ) ? $args['lazy'] : false;
		$all_featured                       = isset( $args['all_featured'] ) ? $args['all_featured'] : '';
		$all_featured                       = $all_featured ? 'featured' : '';
		$first_featured                     = isset( $args['first_featured'] ) ? $args['first_featured'] : '';
		$first_featured                     = $first_featured && $index === 0 ? 'featured' : '';
		$featured_class                     = $all_featured ? "{$class}__item--featured" : ( $first_featured && ! is_paged() ? "{$class}__item--featured" : '' );
		$sticky_class                       = is_sticky() ? "{$class}__item--sticky" : '';
		$excerpt_length                     = isset( $args['excerpt_length'] ) ? $args['excerpt_length'] : 20;
		$featured_excerpt_length            = isset( $args['featured_excerpt_length'] ) ? $args['featured_excerpt_length'] : '';
		$is_slider                          = isset( $args['is_slider'] ) ? $args['is_slider'] : '';
		$slides_num                         = isset( $args['slides_num'] ) ? $args['slides_num'] : '';
		$link                               = get_the_permalink();
		$img_orientation                    = has_post_thumbnail() ? wpstarter_get_img_orientation( get_post_thumbnail_id( get_the_id() ) ) : '';
		$title                              = get_the_title();
		$link_title                         = sprintf(
			// translators: %s: Title of the post.
			esc_html__( 'Read more about %s', 'TRANSLATE' ),
			$title
		); ?>

		<article class='<?php echo esc_attr( "{$class}__item $featured_class $sticky_class" . ( has_post_thumbnail() ? '' : " {$class}__item--img-no-exist" ) ); ?>'>

			<div class='<?php echo esc_attr( "{$class}__img-container" . ( has_post_thumbnail() ? '' : " {$class}__img-container--img-no-exist" ) . ( $img_orientation ? " {$class}__img-container--img-{$img_orientation}" : '' ) ); ?>'>
				<?php if ( has_post_thumbnail() ) :

					$image_args = [
						'max_width' => 1050,
						'size'      => $image_size,
						'sizes'     => $image_sizes,
						'lazy'      => $lazy,
					];

					$image_class ? $image_args['class'] = $image_class : '';

					wpstarter_post_thumbnail( '', $image_args );

				endif; ?>
			</div>

			<div class='<?php echo esc_attr( "{$class}__content" . ( has_post_thumbnail() ? '' : " {$class}__content--img-no-exist" ) ); ?>'>

				<?php if ( is_sticky() ) : ?>
					<span class='<?php echo esc_attr( "{$class}__sticky-icon fa fa-thumbtack" ); ?>'></span>
				<?php endif; ?>

				<div class='<?php echo esc_attr( "{$class}__categories-container" ); ?>'>

					<?php wpstarter_post_categories( get_the_id(), [
						'categories_are_links' => true,
						'class'         => $class,
						'category_slug'        => 'category',
						'is_unstyled'          => true,
						'max_number'           => 5
					] ); ?>

				</div>

				<?php if ( $title ) : ?>
					<h2 class='<?php echo esc_attr( "{$class}__title" ); ?>'>
						<a href='<?php echo esc_url( $link ); ?>' title='<?php echo esc_attr( $link_title ); ?>'><?php echo wp_kses_post( $title ); ?></a>
					</h2>
				<?php endif; ?>

				<?php if ( wpstarter_excerpt() ): ?>		
					<?php if ( $first_featured ): ?>
						<p class='<?php echo esc_attr( "{$class}__excerpt" ); ?>'><?php echo esc_html( $first_featured && $featured_excerpt_length ? wpstarter_excerpt( $featured_excerpt_length ) : wpstarter_excerpt( $excerpt_length ) ); ?></p>
					<?php elseif ( $all_featured ) : ?>
						<p class='<?php echo esc_attr( "{$class}__excerpt" ); ?>'><?php echo esc_html( wpstarter_excerpt( $featured_excerpt_length ) ); ?></p>
					<?php else : ?>
						<p class='<?php echo esc_attr( "{$class}__excerpt" ); ?>'><?php echo esc_html( wpstarter_excerpt( $excerpt_length ) ); ?></p>
					<?php endif; ?>
				<?php endif; ?>

				<div class='<?php echo esc_attr( "{$class}__details-container" ); ?>'>
					<a class='<?php echo esc_attr( "{$class}__more" ); ?>' title='<?php echo esc_attr( $link_title ); ?>' href='<?php echo esc_url( $link ); ?>'><?php esc_html_e( 'Read more', 'TRANSLATE' ); ?></a>
					<span class='<?php echo esc_attr( "{$class}__date fas fa-calendar-alt" ); ?>'><?php echo get_the_date( 'M j, Y' ); ?></span>
				</div>

				<?php if ( $is_slider ) : ?>
					<span class='<?php echo esc_attr( "{$class}__num" ); ?>'><?php echo esc_html( $index >= 9 ? $index + 1 : 0 . ( $index + 1 ) ); ?></span>
					<?php if ( $slides_num ) : ?>
						<div class='<?php echo esc_attr( "{$class}__nav" ); ?>'>
							<button type='button' class='<?php echo esc_attr( "{$class}__nav-btn {$class}__nav-btn--is-hidden prev fa fa-long-arrow-alt-left" ); ?>' disabled></button>
							<button type='button' class='<?php echo esc_attr( "{$class}__nav-btn {$class}__nav-btn--is-hidden next fa fa-long-arrow-alt-right" ); ?>' disabled></button>
						</div>
					<?php endif; ?>
				<?php endif; ?>

			</div>

		</article>

	<?php }

endif;

if ( ! function_exists( 'wpstarter_single_blog_post_content' ) ) :

	/**
	 * Displays the single blog post content.
	 */
	function wpstarter_single_blog_post_content() {

		$class = 'single-post-area'; ?>

		<article id='post-<?php echo esc_attr( the_ID() ); ?>' <?php echo esc_attr( post_class( $class ) ); ?>>

			<div class='<?php echo esc_attr( "{$class}__container" ); ?>'>

				<div class='<?php echo esc_attr( "{$class}__content" ); ?>'><?php the_content(); ?></div>

				<?php $pagination = wp_link_pages( [
					'before' => '<div class="page-pagination-area">',
					'after'  => '</div>',
					'echo'   => false
				] );

				$pagination = str_replace( 'class="post-page-numbers"', 'class="page-pagination-area__number page-pagination-area__number--button page-pagination-area__number--button-transparent"', $pagination );
				$pagination = str_replace( 'class="post-page-numbers current"', 'class="page-pagination-area__number page-pagination-area__number--current page-pagination-area__number--button page-pagination-area__number--button-transparent"', $pagination );
				
				echo wp_kses_post( $pagination ); ?>

				<div class='<?php echo esc_attr( "{$class}__meta" ); ?>'>

					<?php if ( has_category() ) : ?>
						<div class='<?php echo esc_attr( "{$class}__categories" ); ?>'>
							<?php wpstarter_post_categories( get_the_id(), [
								'categories_are_links' => true,
								'is_unstyled'          => true,
								'class'         => $class,
							] ); ?>
						</div>
					<?php endif; ?>

					<?php if ( has_tag() ): ?>
						<div class='<?php echo esc_attr( "{$class}__tags" ); ?>'>
							<?php wpstarter_post_tags( get_the_id(), [
								'tags_are_links' => true,
								'is_unstyled'    => true,
								'class'   => $class,
							] ); ?>
						</div>
					<?php endif; ?>

				</div>

			</div>

		</article>

	<?php }

endif;

if ( ! function_exists( 'wpstarter_single_nav' ) ) :

	function wpstarter_single_nav() {

		$nav = get_the_post_navigation(
			wp_parse_args(
				[
					'prev_text' => sprintf(
							// translators: %s: Title of the previous post.
							esc_html__( 'Previous %s', 'TRANSLATE' ),
							'<span class="screen-reader-text">%title</span>'
						),
					'next_text' => sprintf(
						// translators: %s: Title of the next post.
						esc_html__( 'Next %s', 'TRANSLATE' ),
						'<span class="screen-reader-text">%title</span>'
					),
				]
			)
		);

		$inactive_prev = '<div class="navigation-area__previous-container"><span class="navigation-area__button navigation-area__button--button navigation-area__button--is-not-active">' . esc_html__( 'Previous', 'TRANSLATE' ) . '</span></div>';
		$inactive_next = '<div class="navigation-area__next-container"><span class="navigation-area__button navigation-area__button--button navigation-area__button--is-not-active">' . esc_html__( 'Next', 'TRANSLATE' ) . '</span></div>';

		$nav = str_replace( 'class="navigation post-navigation"', 'class="navigation-area"', $nav );
		$nav = str_replace( 'class="nav-links"', 'class="navigation-area__container"', $nav );
		$nav = str_replace( 'class="nav-previous"', 'class="navigation-area__previous-container"', $nav );
		$nav = str_replace( 'class="nav-next"', 'class="navigation-area__next-container"', $nav );
		$nav = str_replace( '<a', '<a class="navigation-area__link navigation-area__link--button"', $nav );

		if ( ! get_next_post() ) :
			$nav = str_replace( '</a></div>', '</a></div>' . $inactive_next, $nav );
		elseif ( ! get_previous_post() ) :
			$nav = str_replace( '<div class="navigation-area__next-container">', $inactive_prev . '<div class="navigation-area__next-container">', $nav );
		endif;

		echo wp_kses_post( $nav );

	}

endif;

if ( ! function_exists( 'wpstarter_page_content' ) ) :

	/**
	 * Displays the page content.
	 */
	function wpstarter_page_content() { ?>

		<article id='post-<?php the_ID(); ?>' <?php post_class( 'page-area' ); ?>>

			<div class='page-area__container'>

				<div class='page-area__content'><?php the_content(); ?></div>

				<?php $pagination = wp_link_pages( [
					'before' => '<div class="page-pagination-area">',
					'after'  => '</div>',
					'echo'   => false
				] );

				$pagination = str_replace( 'class="post-page-numbers"', 'class="page-pagination-area__number page-pagination-area__number--button page-pagination-area__number--button-transparent"', $pagination );
				$pagination = str_replace( 'class="post-page-numbers current"', 'class="page-pagination-area__number page-pagination-area__number--current page-pagination-area__number--button page-pagination-area__number--button-transparent"', $pagination );
				
				echo wp_kses_post( $pagination ); ?>

			</div>

		</article>

	<?php }

endif;

if ( ! function_exists( 'wpstarter_search_results_content' ) ) :

	/**
	 * Displays the search result content.
	 */
	function wpstarter_search_results_content( $i ) {

		$link           = get_permalink();
		$link_title     = sprintf(
			// translators: %s: Title of the post.
			esc_html__( 'Read more about %s', 'TRANSLATE' ),
			the_title_attribute( [ 'echo' => false ] )
		);
		$title        = get_the_title();
		$class = 'search-page';
		$post_type    = get_post_type();

		if ( class_exists( 'Wpstarter_Portfolio' ) && class_exists( 'ACF' ) ) :
			$portfolio_content        = wpstarter_get_options_portfolio( get_the_id() )['single_content'];
			$sliced_portfolio_content = implode( ' ', array_slice( str_word_count( $portfolio_content, 1 ), 0, 20 ) );
			$portfolio_content        = str_word_count( $portfolio_content ) > 20 ? $sliced_portfolio_content . '...' : $sliced_portfolio_content;
			$content                  = 'portfolio' === $post_type ? $portfolio_content : wpstarter_excerpt( 20 );
		else :
			$content   = wpstarter_excerpt( 20 );
		endif;

		if ( 'portfolio' === $post_type ) :
			$post_name = esc_html__( 'Portfolio', 'TRANSLATE' );
		elseif( 'post' === $post_type ) :
			$post_name = esc_html__( 'Blog', 'TRANSLATE' );
		elseif( 'page' === $post_type ) :
			$post_name = esc_html__( 'Page', 'TRANSLATE' );
		else :
			$post_name = $post_type;
		endif; ?>

		<article id='post-<?php the_ID(); ?>' <?php post_class( "{$class}__item" ); ?>>

			<a class='<?php echo esc_attr( "{$class}__link" ); ?>' href='<?php echo esc_url( $link ); ?>' title='<?php echo esc_attr( $link_title ); ?>'>

				<span class='<?php echo esc_attr( "{$class}__post-type" ); ?>'><?php echo esc_html( $post_name ); ?></span>

				<?php if ( $title ) : ?>
					<header class='<?php echo esc_attr( "{$class}__post-header" ); ?>'>
						<h2 class='<?php echo esc_attr( "{$class}__post-title" ); ?>'><?php echo wp_kses_post( $title ); ?></h2>
					</header>
				<?php endif; ?>

				<?php if ( $content ) : ?>
					<p class='<?php echo esc_attr( "{$class}__post-content" ); ?>'><?php echo esc_html( $content ); ?></p>
				<?php endif; ?>

			</a>

		</article>

	<?php }

endif;

if ( ! function_exists( 'wpstarter_attachment_content' ) ) :

	/**
	 * Displays the attachment content;
	 */
	function wpstarter_attachment_content() { ?>

		<article id='post-<?php the_ID(); ?>' <?php post_class( 'single-attachment-area' ); ?>>

			<div class='single-attachment-area__container'>
				<?php wpstarter_post_thumbnail( get_the_ID(), [
					'size'  => 'wpstarter_post',
					'sizes' => [ 'wpstarter_post_medium', 'wpstarter_post', 'wpstarter_post_medium_retina', 'wpstarter_post_retina' ]
				] ); ?>
			</div>

		</article>

	<?php }

endif;

if ( ! function_exists( 'wpstarter_not_found' ) ) :

	/**
	 * Displays the not found page content.
	 */
	function wpstarter_not_found() {

		$class = 'search-no-results-page'; ?>

		<section class='<?php echo esc_attr( $class ); ?>'>
			<article class='<?php echo esc_attr( "{$class}__item" ); ?>'>
				<p class='<?php echo esc_attr( "{$class}__content" ); ?>'><?php esc_html_e( 'Sorry, but nothing matched your search terms.', 'TRANSLATE' ); ?></p>
				<a href='<?php echo esc_url( home_url( '/' ) ); ?>' rel='home' class='<?php echo esc_attr( "{$class}__link {$class}__link--button" ); ?>'><?php esc_html_e( 'Go to homepage', 'TRANSLATE' ); ?></a>
			</article>
		</section>

	<?php	}

endif;
