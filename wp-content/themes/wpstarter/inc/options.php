<?php
/**
 * Theme options from Customizer.
 *
 * @since 1.0.0
 *
 * @package wpstarter
 */

if ( ! function_exists( 'wpstarter_get_options_loop' ) ) :

	/**
	 * Itterates through all the Customizer settings for the given array.
	 *
	 * @param string $id Initial part of ID of specify get_theme_mod option.
	 * @param array $settings Array of get_theme_mod ids string.
	 * @return array Arrays/Specified array with Customizer options.
	 */
	function wpstarter_get_options_loop( $id, array $settings ) {

		if ( $id && ! empty( $settings ) ) :

			foreach ( $settings as $v => $k ) :

				$arr[ $v ] = class_exists( 'Kirki' ) ? get_theme_mod( $id . $v, $k ) : $k;
				
			endforeach;

			return $arr;

		endif;

	}

endif;

if ( ! function_exists( 'wpstarter_get_options_footer' ) ) :

	/**
	 * Get theme Footer Customizer options.
	 *
	 * @return array Arrays/Specified array with Customizer options.
	 */
	function wpstarter_get_options_footer() {

		$options = wpstarter_get_options_loop(
			'wpstarter_footer_',
			[
				'subscription_visibility'    => false,
				'subscription_title_text'    => esc_html__( 'Sign up to our newsletter', 'TRANSLATE' ),
				'subscription_forms'         => '',
				'instagram_visibility'       => false,
				'instagram_access_token'     => '',
				'instagram_new_tab'          => false,
				'back_to_top_btn_visibility' => true,
				'social_media'               => '',
				'social_media_new_tab'       => true,
				'copyright_text'             => '',
			]
		);

		return apply_filters( 'wpstarter_get_options_footer', $options );

	}

endif;

if ( ! function_exists( 'wpstarter_get_options_header' ) ) :

	/**
	 * Get theme Header Customizer options.
	 *
	 * @return array Arrays/Specified array with Customizer options.
	 */
	function wpstarter_get_options_header() {

		$options = wpstarter_get_options_loop(
			'wpstarter_header_',
			[
				'nav_breakpoint'       => 900,
				'search_visibility'    => true,
				'social_media'         => '',
				'social_media_new_tab' => true,
			]
		);

		return apply_filters( 'wpstarter_get_options_header', $options );

	}

endif;

if ( ! function_exists( 'wpstarter_get_options_blog' ) ) :

	/**
	 * Get theme Blog Customizer options.
	 *
	 * @return array Arrays/Specified array with Customizer options.
	 */
	function wpstarter_get_options_blog( $id = '' ) {

		$options = wpstarter_get_options_loop(
			'wpstarter_blog_',
			[
				'layout'                             => 'sidebar_right',
				'single_layout'                      => 'sidebar_right',
				'featured'                           => true,
				'single_author_visibility'           => false,
				'single_releated_posts_visibility'   => false,
				'single_releated_posts_heading_text' => esc_html__( 'You may also like', 'TRANSLATE' ),
				'sticky_sidebar'                     => false,
			]
		);

		return apply_filters( 'wpstarter_get_options_blog', $options );

	}

endif;

if ( ! function_exists( 'wpstarter_get_options_front_page_slider' ) ) :

	/**
	 * Get theme Front Page Slider Customizer options.
	 *
	 * @return array Arrays/Specified array with Customizer options.
	 */
	function wpstarter_get_options_front_page_slider() {

		$options = wpstarter_get_options_loop(
			'wpstarter_front_page_slider_',
			[
				'visibility'      => false,
				'slides_num'      => 5,
				'rewind'          => true,
				'custom_anim'     => true,
				'custom_anim_in'  => '',
				'custom_anim_out' => '',
				'autoplay'        => false,
				'autoplay_time'   => 5,
				'autoplay_pause'  => false,
			]
		);

		return apply_filters( 'wpstarter_get_options_front_page_slider', $options );

	}

endif;

if ( ! function_exists( 'wpstarter_get_options_front_page_testimonials' ) ) :

	/**
	 * Get theme Front Page Testimonials Customizer options.
	 *
	 * @return array Arrays/Specified array with Customizer options.
	 */
	function wpstarter_get_options_front_page_testimonials() {

		$options = wpstarter_get_options_loop(
			'wpstarter_front_page_testimonials_',
			[
				'visibility'             => false,
				'heading_text'           => esc_html__( 'Testimonials', 'TRANSLATE' ),
				'items'                  => '',
				'slider_rewind'          => true,
				'slider_custom_anim'     => true,
				'slider_custom_anim_in'  => '',
				'slider_custom_anim_out' => '',
				'slider_autoplay'        => false,
				'slider_autoplay_time'   => 5,
				'slider_autoplay_pause'  => false,
			]
		);

		return apply_filters( 'wpstarter_get_options_front_page_testimonials', $options );

	}

endif;

if ( ! function_exists( 'wpstarter_get_options_front_page_blog' ) ) :

	/**
	 * Get theme Front Page Blog Customizer options.
	 *
	 * @return array Arrays/Specified array with Customizer options.
	 */
	function wpstarter_get_options_front_page_blog() {

		$options = wpstarter_get_options_loop(
			'wpstarter_front_page_blog_',
			[
				'visibility'     => false,
				'heading_text'   => esc_html__( 'Latest posts', 'TRANSLATE' ),
				'posts_num'      => 7,
				'featured'       => true,
				'btn_visibility' => false,
				'btn_text'       => '',
				'btn_page_url'   => '',
			]
		);

		return apply_filters( 'wpstarter_get_options_front_page_blog', $options );

	}

endif;


if ( ! function_exists( 'wpstarter_get_options_front_page_portfolio' ) ) :

	/**
	 * Get theme Front Page portfolio Customizer options.
	 *
	 * @return array Arrays/Specified array with Customizer options.
	 */
	function wpstarter_get_options_front_page_portfolio() {

		$options = wpstarter_get_options_loop(
			'wpstarter_front_page_portfolio_',
			[
				'visibility'     => false,
				'heading_text'   => esc_html__( 'Portfolio', 'TRANSLATE' ),
				'albums'         => [],
				'btn_visibility' => false,
				'btn_text'       => '',
				'btn_page_url'   => '',
			]
		);

		return apply_filters( 'wpstarter_get_options_front_page_portfolio', $options );

	}

endif;

if ( ! function_exists( 'wpstarter_get_options_portfolio' ) ) :

	/**
	 * Get theme portfolio Customizer options.
	 *
	 * @return array Arrays/Specified array with Customizer options.
	 */
	function wpstarter_get_options_portfolio( $id = '' ) {

		$options = wpstarter_get_options_loop(
			'wpstarter_portfolio_',
			[
				'layout'                              => 'grid-2-col',
				'filter_all_btn_text'                 => esc_html__( 'All', 'TRANSLATE' ),
				'filter_visibility'                   => true,
				'load_more_btn_text'                  => esc_html__( 'More albums', 'TRANSLATE' ),
				'custom_images_num'                   => false,
				'images_num'                          => 5,
				'single_releated_albums_visibility'   => false,
				'single_releated_albums_heading_text' => esc_html__( 'Releated albums', 'TRANSLATE' ),
			]
		);

		if ( class_exists( 'ACF' ) ) :
			$options['single_content'] = get_field( 'portfolio_content', $id );
			$options['single_gallery'] = get_field( 'portfolio_images_gallery', $id );
			$options['single_layout']  = get_field( 'portfolio_layout', $id );
		endif;

		return apply_filters( 'wpstarter_get_options_portfolio', $options );

	}

endif;

if ( ! function_exists( 'wpstarter_get_all_options' ) ) :

	/**
	 * Get all custom theme Customizer options.
	 *
	 * @return array Arrays/Specified array with Customizer options.
	 */
	function wpstarter_get_all_options() {

		$options = [
			'footer'                  => wpstarter_get_options_footer(),
			'header'                  => wpstarter_get_options_header(),
			'blog'                    => wpstarter_get_options_blog(),
			'front_page_header'       => wpstarter_get_options_front_page_slider(),
			'front_page_testimonials' => wpstarter_get_options_front_page_testimonials(),
			'front_page_blog'         => wpstarter_get_options_front_page_blog(),
			'front_page_portfolio'    => class_exists( 'Wpstarter_Portfolio' ) ? wpstarter_get_options_front_page_portfolio() : '',
			'portfolio'               => class_exists( 'Wpstarter_Portfolio' ) ? wpstarter_get_options_portfolio() : '',
		];

		return apply_filters( 'wpstarter_get_all_options', $options );

	}

endif;
