<?php
/**
 * Custom hooks for wpstarter theme.
 */

// Site content start.
add_action( 'wpstarter_site_content_start', 'wpstarter_site_content_start', 10 );
add_action( 'wpstarter_site_content_start', 'wpstarter_featured_image_area', 20 );

// Site content area start.
add_action( 'wpstarter_site_content_area_start', 'wpstarter_site_content_area_start', 10 );
add_action( 'wpstarter_site_content_area_start', 'wpstarter_site_content_header_area', 20 );
add_action( 'wpstarter_site_content_area_start', 'wpstarter_site_content_primary_area_start', 40, 1 );
add_action( 'wpstarter_site_content_area_start', 'wpstarter_site_content_primary_area_before_loop', 50 );

// Single site content area start.
add_action( 'wpstarter_single_site_content_area_start', 'wpstarter_site_content_area_start', 10 );
add_action( 'wpstarter_single_site_content_area_start', 'wpstarter_site_content_primary_area_start', 30, 1 );
add_action( 'wpstarter_single_site_content_area_start', 'wpstarter_site_content_header_area', 40 );
add_action( 'wpstarter_single_site_content_area_start', 'wpstarter_site_content_primary_area_before_loop', 50 );

// Site content area end.
add_action( 'wpstarter_site_content_area_end', 'wpstarter_site_content_primary_area_after_loop', 10, 0 );
add_action( 'wpstarter_site_content_area_end', 'wpstarter_site_content_primary_area_end', 20, 1 );
add_action( 'wpstarter_site_content_area_end', 'wpstarter_site_content_area_end', 40, 0 );
add_action( 'wpstarter_site_content_area_end', 'comments_template', 50, 0 );

// Site content end.
add_action( 'wpstarter_site_content_end', 'wpstarter_site_content_end', 10 );

// Blog posts.
add_action( 'wpstarter_blog_posts', 'wpstarter_blog_post_content', 10, 1 );

// Blog single post.
add_action( 'wpstarter_single_blog_post', 'wpstarter_single_blog_post_content', 10 );
add_action( 'wpstarter_single_blog_post', 'wpstarter_single_nav', 20 );
add_action( 'wpstarter_single_blog_post', 'wpstarter_releated_posts', 30 );

// Page.
add_action( 'wpstarter_page', 'wpstarter_page_content', 10 );

// Attachments.
add_action( 'wpstarter_attachment', 'wpstarter_attachment_content', 10 );

// Search.
add_action( 'wpstarter_search', 'wpstarter_search_results_content', 10, 1 );

// Woocommerce.
add_action( 'woocommerce_before_shop_loop', 'wpstarter_site_content_area_start', 40 );
add_action( 'woocommerce_before_shop_loop', 'wpstarter_site_content_header_area', 40 );
add_action( 'woocommerce_before_shop_loop', 'wpstarter_site_content_primary_area_start', 40 );
add_action( 'woocommerce_before_shop_loop', 'wpstarter_site_content_primary_area_before_loop', 40 );

add_action( 'woocommerce_after_shop_loop', 'wpstarter_site_content_primary_area_after_loop', 40 );
add_action( 'woocommerce_after_shop_loop', 'wpstarter_site_content_primary_area_end', 40 );
add_action( 'woocommerce_after_shop_loop', 'wpstarter_site_content_area_end', 40 );

// WooCommerce single product.
remove_action( 'woocommerce_before_single_product', 'woocommerce_output_all_notices', 10 );
add_action( 'woocommerce_before_single_product', 'wpstarter_site_content_area_start', 20 );
add_action( 'woocommerce_before_single_product', 'woocommerce_output_all_notices', 30 );
add_action( 'woocommerce_before_single_product', 'wpstarter_site_content_primary_area_start', 40 );
add_action( 'woocommerce_before_single_product', 'wpstarter_site_content_primary_area_before_loop', 50 );

add_action( 'woocommerce_after_single_product', 'wpstarter_site_content_primary_area_after_loop', 60 );
add_action( 'woocommerce_after_single_product', 'wpstarter_site_content_primary_area_end', 70 );
add_action( 'woocommerce_after_single_product', 'wpstarter_site_content_area_end', 80 );

remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_title', 5 );
add_action( 'woocommerce_single_product_summary', 'wpstarter_site_content_header_area', 5 );

remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_rating', 10 );
// add_action( 'woocommerce_single_product_summary', 'wpstarter_site_content_header_area', 5 );

// WooCommerce shop page.
remove_action( 'woocommerce_before_shop_loop' , 'woocommerce_catalog_ordering', 30 ); // Remove the sorting dropdown from Woocommerce.
remove_action( 'woocommerce_before_shop_loop' , 'woocommerce_result_count', 20 ); // Remove the result count from WooCommerce.
