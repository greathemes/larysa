<?php
/**
 * The template for displaying comments.
 */

// If comments are open or we have at least one comment, load up the comment template.
if ( comments_open() || get_comments_number() ) :

	if ( ! post_password_required() && ! is_front_page() ) :

		$class = 'comments-area'; ?>

		<section id='comments' class='<?php echo esc_attr( $class ); ?>'>

			<div class='wrapper'>

				<div class='<?php echo esc_attr( "{$class}__comments" ); ?>'>
					<?php wp_list_comments( [
						'short_ping' => true,
						'walker'     => new Wpstarter_Walker_Comments( $class )
					] ); ?>
				</div>

				<?php echo wp_kses_post( str_replace( '<a', '<a class="nav-link nav-link--button"', get_the_comments_navigation() ) ); ?>

				<?php if ( ! comments_open() ) : ?>
					<p class='<?php echo esc_attr( "{$class}__no-comments" ); ?>'><?php esc_html_e( 'Comments are closed.', 'TRANSLATE' ); ?></p>
				<?php endif; ?>

				<div class='<?php echo esc_attr( "{$class}__form-container" . ( ! get_comments_number() ? " {$class}__form-container--no-comments" : '' ) ); ?>'>
					<?php comment_form( [
						'class_submit' => 'submit submit--button'
					] ); ?>
				</div>

			</div>

		</section>

	<?php endif;

endif;
