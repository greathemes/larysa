<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that other 'pages' on your WordPress site may use a different template.
 */

get_header();

do_action( 'wpstarter_site_content_area_start' );

if ( have_posts() ) :
	get_template_part( 'loop' );
else :
	wpstarter_not_found();
endif;

do_action( 'wpstarter_site_content_area_end' );

get_footer();
