A minimalistic, feminine, beautiful theme for photographers and bloggers.

There is still a lot of to do, but this is my biggest project. As I'm looking for a job as a junior WordPress developer while creating this theme I learned a lot:

- Working with WordPress Customizer API.
- Working with WordPress Transient API.
- How to work with actions and filters.
- I got to know plugins such as Kirki and Jetpack better.
- I worked with various JS libraries such as Photoswipe, Spotlight, Masonry DeSandro, Macy, Owl Carousel.
- I downloaded photos from Instagram API and made a cache using Transient API.
- I started learning the BEM concept in CSS.
- I created a portfolio and combined them with AJAX, adding lazyloading and category filtering there.
- I learned about various things in WordPress core, such as posts format, emoji, plugin territory etc.
- I created my own simple plugins (portfolio, author informations, custom widgets).
- I increased my knowledge of WordPress optimization https://gtmetrix.com/reports/larysa.maciejruminski.com/rCAx7acn.

The demo is here http://larysa.maciejruminski.com/
