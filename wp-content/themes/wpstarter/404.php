<?php
/**
 * The template for displaying 404 pages (not found).
 */

get_header();

$class = 'error-404-page'; ?>

<section class='<?php echo esc_attr( $class ); ?>'>

	<span class='<?php echo esc_attr( "{$class}__num-background" ); ?>'>404</span>

	<div class='<?php echo esc_attr( "{$class}__container container" ); ?>'>
		<span class='<?php echo esc_attr( "{$class}__num" ); ?>'>404</span>
		<h1 class='<?php echo esc_attr( "{$class}__heading" ); ?>'><?php esc_html_e( "Oops! That page can't be found!", 'TRANSLATE' ); ?></h1>
		<p class='<?php echo esc_attr( "{$class}__content" ); ?>'>
			<?php printf(
				// translators: %1$s: Start of the HTML link tag.
				// translators: %2$s: End of the HTML link tag.
				esc_html__( 'Sorry, it looks like nothing was found at this location. Maybe return to the %1$shomepage%2$s or try a search?', 'TRANSLATE' ),
				'<a href="' . esc_url( home_url( '/' ) ) . '" class="' . "{$class}__content-link" . '">', '</a>'
			); ?>
		</p>

		<?php wpstarter_primary_header_search( [
			'class'               => "{$class}",
			'icon_or_description' => false
		] ); ?>

	</div>

</section>

<?php get_footer();
