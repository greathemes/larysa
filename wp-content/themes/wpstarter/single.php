<?php
/**
 * The template for displaying all single posts.
 */

get_header();

$layout = wpstarter_get_options_blog()['single_layout'];

do_action( 'wpstarter_single_site_content_area_start', $layout );

if ( have_posts() ) :
	get_template_part( 'loop' );
	if ( class_exists( 'Wpstarter_Widgets_Init' ) ) :
		wpstarter_set_post_views( get_the_ID() );
	endif;
else :
	wpstarter_not_found();
endif;

do_action( 'wpstarter_site_content_area_end', $layout );

get_footer();
