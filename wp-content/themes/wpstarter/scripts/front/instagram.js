/**
 * Footer Instagram gallery.
 *
 * @since 1.0.0
 *
 * @package wpstarter
 */

jQuery( function( $ ) {

	'use strict';

	const $instagram = $( '.instagram-gallery__slider' );

	if ( $instagram.length ) {

		$instagram.owlCarousel( {
			center    : false,
			loop      : true,
			nav       : false,
			dots      : false,
			smartSpeed: 1000,
			margin    : 0,
			items     : 2,
			responsive: {
				600 : { items: 3 },
				750 : { items: 4 },
				900 : { items: 5 },
				1050: { items: 6 },
				1200: { items: 7 },
				1350: { items: 8 },
				1500: { items: 9 },
				1750: { items: 11 }
			}
		} );

	}

} );
