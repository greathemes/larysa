/**
 * Front Page portfolio.
 *
 * @package wpstarter
 *
 * @since 1.0.0
 */

( function () {

	'use strict';

	const portfolio = document.querySelector( '.front-page-portfolio' );

	if ( ! portfolio ) {
		return;
	}

	const portfolioItemsArr = [].slice.call( portfolio.querySelectorAll( '.front-page-portfolio__item' ) ),
	firstItem               = portfolioItemsArr[0],
	isFirstItem2Col         = firstItem ? firstItem.classList.contains( 'front-page-portfolio__item--col-2' ) : '',
	time                    = parseFloat( getComputedStyle( document.documentElement ).getPropertyValue( '--transition-duration' ) ) * 1000;

	if ( ! firstItem ) {
		return;
	}

	let itemHeight = 0;

	function setItemsHeight( timeout ) {

		setTimeout( function() {

			if ( isFirstItem2Col && window.innerWidth >= 1000 ) {
				itemHeight = ( firstItem.clientWidth / 2 ) + 'px';
			} else {
				itemHeight = firstItem.clientWidth + 'px';
			}

			portfolioItemsArr.forEach( function( el ) {
				el.style.height = itemHeight;
				setTimeout( function() {
					el.classList.remove( 'front-page-portfolio__item--is-hidden' );
				}, time );
			} );

		}, timeout );

	}

	setItemsHeight( 0 );

	window.addEventListener( 'resize', function() {
		setItemsHeight( time * 2 );
	}, false );

	window.addEventListener( 'orientationchange', function() {
		setItemsHeight( time * 2 );
	}, false );

} )();
