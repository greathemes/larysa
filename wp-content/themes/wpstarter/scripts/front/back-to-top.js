/**
 * Back to top button.
 *
 * @since 1.0.0
 *
 * @package wpstarter
 */

( function() {

	'use strict';

	const toTop = document.querySelector( '.back-to-top-button' );

	if ( toTop ) {

		function animateScroll() {

			if ( document.body.scrollTop > 0 || window.pageYOffset > 0 ) {
				window.scrollBy( 0, -60 );
				setTimeout( animateScroll, 5 );
			}

		}

		toTop.addEventListener( 'click', function() {
			animateScroll();
		} );

		window.addEventListener( 'scroll', function() {

			if ( document.body.scrollTop >= 1000 || window.pageYOffset >= 1000 ) {
				toTop.classList.remove( 'back-to-top-button--is-hidden' );
				toTop.classList.add( 'back-to-top-button--is-visible' );
			} else {
				toTop.classList.add( 'back-to-top-button--is-hidden' );
				toTop.classList.remove( 'back-to-top-button--is-visible' );
			}

		}, false);

	}

}() );
