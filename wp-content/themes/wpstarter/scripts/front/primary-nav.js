/**
 * Primary navigation.
 *
 * Handles toggling the navigation menu for small screens.
 *
 * @package wpstarter
 *
 * @since 1.0.0
 */

( function () {

	'use strict';

	const header = document.querySelector( '.primary-header' );

	if ( ! header ) {
		return;
	}

	const headerDesktop = header.querySelector( '.primary-header-desktop' ),
	headerMobile        = header.querySelector( '.primary-header-mobile' ),
	navMobile           = headerMobile.querySelector( '.primary-header-mobile-nav__list' );

	if ( ! navMobile ) {
		return;
	}

	const navItems         = Array.prototype.slice.call( headerDesktop.querySelectorAll( 'li' ) ),
	dropdownBtns           = Array.prototype.slice.call( navMobile.querySelectorAll( 'button' ) ),
	dropdownsDesktop       = Array.prototype.slice.call( headerDesktop.querySelectorAll( '.primary-header-desktop-nav__sub-menu' ) ),
	dropdownsMobile        = Array.prototype.slice.call( navMobile.querySelectorAll( '.primary-header-mobile-nav__sub-menu' ) ),
	showNavBtn             = header.querySelector( '.primary-header-desktop__show-btn' ),
	hideNavBtn             = header.querySelector( '.primary-header-mobile__hide-btn' ),
	showSearchBtn          = header.querySelector( '.primary-header-desktop-icons__search-show-btn' ),
	mediaQuery             = window.matchMedia( 'screen and (min-width: ' + header.getAttribute( 'data-nav' ) + 'px)' ),
	activeClass            = 'active',
	dropdownActiveClass    = 'dropdown-active',
	btnDropdownActiveClass = 'clicked';

	function showNav() {

		headerMobile.classList.add( activeClass );

	}

	function hideNav() {

		headerMobile.classList.remove( activeClass );

		setTimeout( function() {

			dropdownsMobile.forEach( function( dropdown ) {
				dropdown.classList.remove( dropdownActiveClass );
			} );

			resetPreventSubmenuOutOffScreen();

		}, 500 );

	}

	if ( showNavBtn && hideNavBtn ) {
		showNavBtn.addEventListener( 'click', showNav );
		hideNavBtn.addEventListener( 'click', hideNav );
	}

	function responsiveNav() {

		if ( mediaQuery.matches ) {
			hideNav();
		}

	}

	mediaQuery.addListener( responsiveNav );

	/**
	 * Toggles dropdownsMobile.
	 *
	 * Updates which dropdown is now active after clicking the arrow button.
	 */
	function toggleDropdown() {

		const that     = this,
		dropdown       = that.nextElementSibling,
		depthZeroClass = 'primary-nav-item-depth-0';

		if ( dropdown.classList.contains( dropdownActiveClass ) ) {

			dropdown.previousElementSibling.classList.remove( btnDropdownActiveClass );
			dropdown.classList.remove( dropdownActiveClass );

		} else {

			const currentActive = navMobile.querySelectorAll( depthZeroClass + ' .' + dropdownActiveClass );

			if ( that.parentNode.classList.contains( depthZeroClass ) && currentActive ) {

				const currentActiveLength = currentActive.length;

				for ( let i = 0; i < currentActiveLength; i++ ) {
					currentActive[ i ].previousElementSibling.classList.remove( btnDropdownActiveClass );
					currentActive[ i ].classList.remove( dropdownActiveClass );
				}
			}

			dropdown.previousElementSibling.classList.add( btnDropdownActiveClass );
			dropdown.classList.add( dropdownActiveClass );

		}

	}

	dropdownBtns.forEach( function( btn ) {
		btn.addEventListener( 'click', toggleDropdown );
	} );

	document.addEventListener( 'keydown', function( e ) {

		if ( e.keyCode == 27 && headerMobile.classList.contains( activeClass ) ) {
				hideNav();
		}

	} );

	/**
	 * Reset and calling the function preventSubmenuOutOffScreen() again if the window width has changed.
	 */
	function resetPreventSubmenuOutOffScreen() {

		const dropdownsDesktopOutOfScreen = Array.prototype.slice.call( headerDesktop.querySelectorAll( '.primary-header-desktop-nav__sub-menu--out-of-screen' ) ),
		dropdownsDesktopOutOfScreenParent = Array.prototype.slice.call( headerDesktop.querySelectorAll( '.primary-header-desktop-nav__sub-menu--out-of-screen-parent' ) );

		dropdownsDesktopOutOfScreen.forEach( function( el, i ) {
			el.classList.remove( 'primary-header-desktop-nav__sub-menu--out-of-screen' );
		} );

		dropdownsDesktopOutOfScreenParent.forEach( function( el, i ) {
			el.classList.remove( 'primary-header-desktop-nav__sub-menu--out-of-screen-parent' );
		} );

		if ( mediaQuery.matches ) {
			preventSubmenuOutOffScreen();
		}

	}

	window.addEventListener( 'resize', resetPreventSubmenuOutOffScreen, false );
	window.addEventListener( 'orientationchange', resetPreventSubmenuOutOffScreen, false );

	/**
	 * Changes the position of the submenu if there is no space.
	 */
	function preventSubmenuOutOffScreen() {

		const headerWidth = header.clientWidth;

		dropdownsDesktop.forEach( function( el, i ) {

			const rect = el.getBoundingClientRect();

			if ( rect.width + rect.left > headerWidth ) {
				el.parentElement.parentElement.classList.add( 'primary-header-desktop-nav__sub-menu--out-of-screen-parent' );
				el.classList.add( 'primary-header-desktop-nav__sub-menu--out-of-screen' );
			}

		} );

	}

	if ( mediaQuery.matches ) {
		preventSubmenuOutOffScreen();
	}

} )();
