/**
 * Latest Albums Widget Resize.
 *
 * @package wpstarter
 *
 * @since 1.0.0
 */

( function () {

	'use strict';

	const recentAlbumsWidget = document.querySelectorAll( '.widget-latest-albums__item ' );

	if ( recentAlbumsWidget.length ) {

		const albumItems = [].slice.call( recentAlbumsWidget ),
		firstItem        = albumItems[0];

		let itemHeight = 0;

		function setItemsHeight( timeout ) {

			setTimeout( function() {

				itemHeight = firstItem.clientWidth + 'px';

				albumItems.forEach( function( el ) {
					el.style.height = itemHeight;
				} );

			}, timeout );

		}

		setItemsHeight( 0 );

		window.addEventListener( 'resize', function() {
			setItemsHeight( 1000 );
		}, false );

		window.addEventListener( 'orientationchange', function() {
			setItemsHeight( 1000 );
		}, false );

	}

} )();
