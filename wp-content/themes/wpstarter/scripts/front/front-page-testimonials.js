/**
 * Front Page testimonials slider.
 *
 * @since 1.0.0
 *
 * @package wpstarter
 */

jQuery( function( $ ) {

	'use strict';

	const $owl = $( '#front_page_testimonials__slider' );

	if ( $owl.length ) {

		const $navBtns    = $( '.front-page-testimonials__nav-btn' ),
		dataCustomAnim    = $owl.data( 'custom-anim' ),
		dataAnimIn        = $owl.data( 'anim-in-type' ),
		dataAnimOut       = $owl.data( 'anim-out-type' ),
		dataRewind        = $owl.data( 'rewind' ),
		dataAutoplay      = $owl.data( 'autoplay' ),
		dataAutoplayTime  = $owl.data( 'autoplay-time' ) * 1000,
		dataAutoplayPause = $owl.data( 'autoplay-pause' );

		function btnsDisabledToggling( status ) {

			$navBtns.each( function( i, btn ) {
				$( btn ).attr( 'disabled', status );
			} );

		}

		$navBtns.on( 'click', function() {
			btnsDisabledToggling( true );
			$( this ).hasClass( 'prev' ) ? $owl.trigger( 'prev.owl.carousel' ) : $owl.trigger( 'next.owl.carousel' );
		} );

		$owl.on('initialized.owl.carousel', function( e ) {

			setTimeout( btnsDisabledToggling, 500, false );

			const $owlStage = $( '#front_page_testimonials__slider .owl-stage' );

			$owlStage.on( 'transitionend webkitTransitionEnd oTransitionEnd animationend webkitAnimationEnd oAnimationEnd', function() {
				btnsDisabledToggling( false );
			} );

		} );

		$owl.owlCarousel( {
			items             : 1,
			center            : false,
			nav               : false,
			dots              : false,
			loop              : false,
			checkVisible      : false,
			autoHeight        : true,
			animateIn         : dataCustomAnim ? dataAnimIn : false,
			animateOut        : dataCustomAnim ? dataAnimOut : false,
			rewind            : dataRewind,
			autoplay          : false,
			autoplayTimeout   : false,
			autoplayHoverPause: false,
			responsive        : {
				600: {
					autoplay          : dataAutoplay,
					autoplayTimeout   : dataAutoplay ? dataAutoplayTime : false,
					autoplayHoverPause: dataAutoplay ? dataAutoplayPause : false,
				}
			}
		} );

	}

} );
