/**
 * Sidebar left position fix script.
 *
 * When the template is active with the sidebar on the left, the sidebar is above the content on mobile phones. This script fixes this.
 *
 * @since 1.0.0
 *
 * @package wpstarter
 */

( function () {

	'use strict';

	if ( document.body.classList.contains( 'sidebar_left' ) ) {

		const sidebar  = document.querySelector( '.sidebar-area' ),
		content        = document.querySelector( '.primary-area' ),
		mobileViewport = window.matchMedia( 'screen and (min-width: 750px)' );

		if ( sidebar && content ) {

			mobileViewport.matches ? sidebar.after( content ) : content.after( sidebar );

			mobileViewport.addListener( function() {
				mobileViewport.matches ? sidebar.after( content ) : content.after( sidebar );
			} );

		}

	}

} )();
