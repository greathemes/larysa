<?php
/**
 * The template for displaying front page.
 */

get_header();

do_action( 'wpstarter_site_content_area_start' );

get_template_part( 'template-parts/front-page/customizer' );
get_template_part( 'template-parts/front-page/slider' );
get_template_part( 'template-parts/front-page/portfolio' );
get_template_part( 'template-parts/front-page/blog' );
get_template_part( 'template-parts/front-page/testimonials' );
get_template_part( 'template-parts/front-page/woo-products' );

do_action( 'wpstarter_site_content_area_end' );

get_footer();
