<?php
/**
 * The header for wpstarter theme.
 */

get_template_part( 'template-parts/header/head' );

if ( ! is_404() ) :

	$options     = wpstarter_get_options_header();
	$nav_bp      = $options['nav_breakpoint'];
	$is_search   = $options['search_visibility'] ? '' : 'no-search';
	$is_nav      = has_nav_menu( 'primary' ) ? '' : 'no-nav';
	$class_names = "primary-header $is_nav $is_search"; ?>

	<header class='<?php echo esc_attr( $class_names ); ?>' data-nav='<?php echo esc_attr( $nav_bp ); ?>'>

		<?php
		get_template_part( 'template-parts/header/mobile' );
		get_template_part( 'template-parts/header/desktop' );
		get_template_part( 'template-parts/header/identity' );
		?>

	</header>

	<?php
	do_action( 'wpstarter_site_content_start' );

endif;
