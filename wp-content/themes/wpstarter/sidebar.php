<?php
/**
 * Displays the primary sidebar.
 */

$pages_with_sidebar = is_home() || is_archive() || is_singular( 'post' ) || is_attachment() || is_page();

if ( is_active_sidebar( 'primary' ) && $pages_with_sidebar ) :

	$class = wpstarter_get_options_blog()['sticky_sidebar'] ? 'sidebar-area sidebar-area--sticky' : 'sidebar-area'; ?>

	<aside class='<?php echo esc_attr( $class ); ?>'><?php dynamic_sidebar( 'primary' ); ?></aside>

<?php endif;
