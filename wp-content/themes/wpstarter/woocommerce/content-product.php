<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

// Ensure visibility.
if ( empty( $product ) || ! $product->is_visible() ) {
	return;
}

$class = 'woo-products';
$index        = get_query_var( 'index' ); ?>

<li <?php wc_product_class( "{$class}__item", $product ); ?>>

	<div class='<?php echo esc_attr( "{$class}__img-container" ); ?>'>

		<a href='<?php echo esc_url( $product->add_to_cart_url() ); ?>' data-product_id='<?php echo esc_attr( $product->get_id() ); ?>' data-product_sku='<?php echo esc_attr( $product->get_sku() ); ?>' data-quantity='<?php echo esc_attr( isset( $quantity ) ? $quantity : 1 ); ?>' class='<?php echo esc_attr( "{$class}__cart-link {$class}__cart-link--button fas fa-spinner " . ( 'product_type_' . $product->get_type() ) . ' ' . ( $product->is_purchasable() && $product->is_in_stock() ? 'add_to_cart_button custom_add_to_cart_button' : '' ) . ' ' . ( $product->supports( 'ajax_add_to_cart' ) ? 'ajax_add_to_cart' : '' ) ); ?>'>
			<span class='<?php echo esc_attr( "{$class}__cart-link-icon fas fa-shopping-cart" ); ?>'></span>
		</a>

		<a href='<?php echo esc_url( get_the_permalink() ); ?>' class='<?php echo esc_attr( "{$class}__img-link" ); ?>'>
			<?php wpstarter_post_thumbnail( $id, [
				'size'  => 'medium',
				'sizes' => [ 'medium' ],
				'class' => "{$class}__img {$class}__img--covered {$class}__img--zoom-animation lazy"
			] ); ?>
		</a>
	</div>

	<div class='<?php echo esc_attr( "{$class}__content-container" ); ?>'>
		<span class='<?php echo esc_attr( "{$class}__price" ); ?>'>
			<?php echo wp_kses_post( wc_price( $product->get_price() ) ); ?>
		</span>

		<h3 class='<?php echo esc_attr( "{$class}__title" ); ?>'>
			<a href='<?php echo esc_url( get_the_permalink() ); ?>'><?php echo esc_html( get_the_title() ); ?></a>
		</h3>

		<div class='<?php echo esc_attr( "{$class}__stars-container" ); ?>'>
			<?php for ( $i = 0; $i < 5; $i++ ) : ?>
				<span class='<?php echo esc_attr( "{$class}__stars-icon fa fa-star" ); ?>'></span>
			<?php endfor; ?>
			<div class='<?php echo esc_attr( "{$class}__stars-inner-container {$class}__stars-inner-container--$index-item" ); ?>'>
				<?php for ( $i = 0; $i < 5; $i++ ) : ?>
					<span class='<?php echo esc_attr( "{$class}__stars-icon fa fa-star" ); ?>'></span>
				<?php endfor; ?>
			</div>
		</div>
	</div>

</li>

<?php $inline_css = ".{$class}__stars-inner-container--$index-item { width:" . ( wpstarter_comment_rating_get_average_ratings( $id ) * 20 ) . "% }"; ?>
<?php set_query_var( 'inline_css', $inline_css ); ?>

