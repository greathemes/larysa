<?php
/**
 * Template Name: Sidebar Left
 *
 * @since 1.0.0
 *
 * @package wpstarter
 */

get_header();

do_action( 'wpstarter_site_content_area_start', 'sidebar_left' );

if ( have_posts() ) :
	get_template_part( 'loop' );
else :
	wpstarter_not_found();
endif;

do_action( 'wpstarter_site_content_area_end' );

get_footer();
