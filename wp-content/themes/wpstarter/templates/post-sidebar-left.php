<?php
/**
 * Template Name: Sidebar Left
 * Template Post Type: post
 *
 * @since 1.0.0
 *
 * @package wpstarter
 */

get_header();

do_action( 'wpstarter_single_site_content_area_start', 'sidebar_left' );

if ( have_posts() ) :
	get_template_part( 'loop' );
	if ( class_exists( 'Wpstarter_Widgets_Init' ) ) :
		wpstarter_set_post_views( get_the_ID() );
	endif;
else :
	wpstarter_not_found();
endif;

do_action( 'wpstarter_site_content_area_end', 'sidebar_left' );

get_footer();
