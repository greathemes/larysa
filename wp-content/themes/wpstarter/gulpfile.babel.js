/**
 * Gulp file.
 */

import gulp from 'gulp';
import yargs from 'yargs';
import sass from 'gulp-sass';
import cleancss from 'gulp-clean-css';
import autoprefixer from 'gulp-autoprefixer';
import gulpif from 'gulp-if';
import sourcemaps from 'gulp-sourcemaps';
import webpack from 'webpack-stream';
import del from 'del';
import uglify from 'gulp-uglify-es';
import concat from 'gulp-concat';
import rename from 'gulp-rename';
import named from 'vinyl-named';
import browsersync from 'browser-sync';
import zip from 'gulp-zip';
import replace from 'gulp-replace';
import wpPot from 'gulp-wp-pot';
import info from './package.json';
import fontawesomesubset from 'fontawesome-subset';
import fontawesome from './fontawesome.json';
import file from 'gulp-file';

const dev_name        = 'wpstarter',
capitalize_dev_name   = 'Wpstarter',
uppercase_dev_name    = 'WPSTARTER',
translate_string      = 'TRANSLATE',
theme_name            = info.name,
theme_name_dir        = '../' + theme_name,
capitalize_theme_name = theme_name.charAt( 0 ).toUpperCase() + theme_name.slice( 1 ),
uppercase_theme_name  = theme_name.toUpperCase(),
assets                = 'assets/',
styles_dir            = 'styles',
assets_js             = 'assets/scripts',
server                = browsersync.create(),
PRODUCTION            = yargs.argv.prod;

export const fonts = ( done ) => {

	const solid_keys = Object.keys( fontawesome.solid ),
	solid_vals       = Object.values( fontawesome.solid ),
	solid_items_num  = solid_keys.length,
	brands_keys      = Object.keys( fontawesome.brands ),
	brands_vals      = Object.values( fontawesome.brands ),
	brands_items_num = brands_keys.length;

	let solid_str = '',
	brands_str    = '',
	final_str     = '';

	for ( let i = 0; i < solid_items_num; i++ ) {

		solid_str += '.fa-' + solid_keys[ i ] + ':before{content:"\\' + solid_vals[ i ] + '";} ';

	}

	for ( let i = 0; i < brands_items_num; i++ ) {

		brands_str += '.fa-' + brands_keys[ i ] + ':before{content:"\\' + brands_vals[ i ] + '";} ';

	}

	final_str = solid_str + brands_str;

	// Creates a file with Font Awesome styles.
	file( '_font-awesome-icons.scss', final_str, { src: true } ).pipe( gulp.dest( styles_dir ) );

	// Creates web fonts files ( woff, woff2, etc ).
	fontawesomesubset(
		{
			solid: solid_keys,
			brands: brands_keys
		},
		theme_name_dir + '/webfonts'
	);

	// Deletes unnecessary files ( eot, svg & ttf ).
	const brands_path = theme_name_dir + '/webfonts/fa-brands-400.',
	solid_path        = theme_name_dir + '/webfonts/fa-solid-900.';

	del(
		[ brands_path + 'eot', brands_path + 'svg', solid_path + 'eot', solid_path + 'svg' ],
		{ force:true }
	);

	done();

}

// Server.
export const serve = ( done ) => {

	server.init( { proxy: 'http://localhost:8888/' + theme_name } );
	done();

}

// Reload.
export const reload = ( done ) => {

	server.reload();
	done();

}

// Create .pot file.
export const pot = () => {

	return gulp.src( '**/*.php' )
	.pipe(
		wpPot(
			{
				domain: translate_string,
				package: theme_name
			}
		)
	)
	.pipe( gulp.dest( 'languages/' + dev_name + '.pot' ) )

}

// Remove production directory & .zip file.
export const clean = () => del( [ theme_name_dir, theme_name_dir + '.zip' ], { force: true } );

// Translation.
export const translate = ( to_translate ) => {

	return to_translate
	.pipe( replace( dev_name, theme_name ) )
	.pipe( replace( capitalize_dev_name, capitalize_theme_name ) )
	.pipe( replace( uppercase_dev_name, uppercase_theme_name ) )
	.pipe( replace( translate_string, theme_name ) );

}

// Styles.
export const styles = ( done ) => {

	return translate( gulp.src( styles_dir + '/front/styles.scss' ) )
		.pipe( gulpif( ! PRODUCTION, sourcemaps.init() ) )
		.pipe( sass().on( 'error', sass.logError ) )
		.pipe( autoprefixer(
			{
				browsers: 'last 2 versions, > 1%',
				cascade: false
			}
		) )
		.pipe( gulpif( ! PRODUCTION, sourcemaps.write() ) )
		.pipe( gulpif( PRODUCTION, cleancss( { compatibility: 'ie11' } ) ) )
		.pipe( rename( { suffix: '.min' } ) )
		.pipe( gulp.dest( theme_name_dir ) )
		done();

}

// Admin styles.
export const admin_styles = ( done ) => {

	return translate( gulp.src( styles_dir + '/admin/admin-styles.scss' ) )
		.pipe( gulpif( ! PRODUCTION, sourcemaps.init() ) )
		.pipe( sass().on( 'error', sass.logError ) )
		.pipe( autoprefixer( {
			browsers: ['last 2 versions'],
			cascade: false
			}
		))
		.pipe( gulpif( ! PRODUCTION, sourcemaps.write() ) )
		.pipe( gulpif( PRODUCTION, cleancss( { compatibility: 'ie11' } ) ) )
		.pipe( rename( { suffix: '.min' } ) )
		.pipe( gulp.dest( theme_name_dir ) )
		done();

}

// Theme style.css.
export const theme_styles = ( done ) => {

	return translate( gulp.src( 'style.css' ) )
	.pipe( gulp.dest( theme_name_dir ) )

}

// Scripts.
export const scripts = ( done ) => {

	return translate( gulp.src( 'scripts/front/**/*.js' ) )
		.pipe( named() )
		.pipe( concat( 'scripts.js' ) )
		.pipe( gulpif( PRODUCTION, uglify() ) )
		.pipe( rename( { suffix: '.min' } ) )
		.pipe( gulp.dest( theme_name_dir ) )
		.pipe( server.stream() )

	done();

}

// Views (php).
export const php = ( done ) => {

	return translate( gulp.src( '**/*.php' ) )
	.pipe(
		rename( ( opt ) => {
			opt.basename = opt.basename.replace( dev_name, theme_name );
			opt.basename = opt.basename.replace( capitalize_dev_name, capitalize_theme_name );
			return opt;
		} )
	)
	.pipe( gulp.dest( theme_name_dir ) );

}

export const files = ( done ) => {

	// Fonts directory.
	gulp.src( 'fonts/**.*' )
	.pipe( gulp.dest( theme_name_dir + '/fonts' ) );

	gulp.src( 'screenshot.png' )
	.pipe( gulp.dest( theme_name_dir ) );

	translate( gulp.src( [ 'languages/' + dev_name + '.pot' ] ) )
	.pipe(
		rename( ( opt ) => {
			opt.basename = opt.basename.replace( dev_name, theme_name );
			return opt;
		} )
	)
	.pipe( gulp.dest( theme_name_dir + '/languages' ) );

	done();

}

// Watch.
export const watch = () => {

	gulp.watch( 'styles/**/*.scss', gulp.series( styles, theme_styles, admin_styles, reload ) );
	gulp.watch( 'scripts/**/*.scss', gulp.series( scripts ) );
	gulp.watch( '**/*.php', gulp.series( php, reload ) );

}

// Compress to .zip file.
export const compress = () => {

	return gulp.src( theme_name_dir + '/**/*' )
		.pipe( zip( theme_name + '.zip' ) )
		.pipe( gulp.dest( '../' ) )

}

// Remove production directory & .zip file.
export const clean_child = () => del( [ '../' + theme_name + '-child' ], { force: true } );

// Child Theme.
export const child = () => {

	return translate( gulp.src( '../' + dev_name + '-child/**.*' ) )
	.pipe( gulp.dest( '../' + theme_name + '-child' ) )

}

// Tasks.
export const dev = gulp.series( clean, fonts, gulp.parallel( styles, admin_styles, theme_styles, scripts, php, files ), serve, watch );
export const build = gulp.series( clean, fonts, pot, gulp.parallel( styles, admin_styles, theme_styles, scripts, php, files ) );
export const production = gulp.series( build, compress );

export default dev;
