<?php
/**
 * Functions and definitions.
 */

// Constants.
define( 'WPSTARTER_THEME_DIR', trailingslashit( get_template_directory() ) );
define( 'WPSTARTER_THEME_INCLUDES', trailingslashit( get_template_directory() . '/inc' ) );
define( 'WPSTARTER_THEME_CUSTOMIZER', trailingslashit( get_template_directory() . '/inc/customizer' ) );
define( 'WPSTARTER_THEME_URI', trailingslashit( esc_url( get_template_directory_uri() ) ) );

// Setup.
require_once WPSTARTER_THEME_INCLUDES . 'tgmpa/class-tgm-plugin-activation.php';
require_once WPSTARTER_THEME_INCLUDES . 'options.php';
require_once WPSTARTER_THEME_INCLUDES . 'setup.php';

// Helper Functions.
require_once WPSTARTER_THEME_INCLUDES . 'custom-functions.php';

// Walker Classes.
require_once WPSTARTER_THEME_INCLUDES . 'class-wpstarter-walker-primary.php';
require_once WPSTARTER_THEME_INCLUDES . 'class-wpstarter-walker-comments.php';

// Template hooks & functions.
require_once WPSTARTER_THEME_INCLUDES . 'template-hooks.php';
require_once WPSTARTER_THEME_INCLUDES . 'template-functions.php';
require_once WPSTARTER_THEME_INCLUDES . 'woo-template-functions.php';

// Customizer.
require_once WPSTARTER_THEME_CUSTOMIZER . 'customizer.php';
require_once WPSTARTER_THEME_CUSTOMIZER . 'customizer-main-header.php';
require_once WPSTARTER_THEME_CUSTOMIZER . 'customizer-front-page-slider.php';
require_once WPSTARTER_THEME_CUSTOMIZER . 'customizer-front-page-portfolio.php';
require_once WPSTARTER_THEME_CUSTOMIZER . 'customizer-front-page-blog.php';
require_once WPSTARTER_THEME_CUSTOMIZER . 'customizer-front-page-testimonials.php';
require_once WPSTARTER_THEME_CUSTOMIZER . 'customizer-blog.php';
require_once WPSTARTER_THEME_CUSTOMIZER . 'customizer-portfolio.php';
require_once WPSTARTER_THEME_CUSTOMIZER . 'customizer-footer.php';
