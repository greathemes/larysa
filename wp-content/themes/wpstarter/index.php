<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 */

get_header();

$layout = wpstarter_get_options_blog()['layout'];

do_action( 'wpstarter_site_content_area_start', $layout );

if ( have_posts() ) :
	get_template_part( 'loop' );
else :
	wpstarter_not_found();
endif;

do_action( 'wpstarter_site_content_area_end', $layout );

get_footer();
