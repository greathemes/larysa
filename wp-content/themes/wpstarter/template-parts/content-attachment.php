<?php
/**
 * Displays attachments content.
 *
 * @since 1.0.0
 *
 * @package wpstarter
 */

do_action( 'wpstarter_attachment' );
