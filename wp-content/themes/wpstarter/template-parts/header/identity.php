<?php
/**
* Header logo and tagline.
*/

$tagline        = get_bloginfo( 'description', 'display' );
$class          = 'primary-header-identity';
$is_tagline     = $tagline ? '' : 'no-tagline';
$is_custom_logo = function_exists( 'the_custom_logo' ) && has_custom_logo() ? true : false;
$logo           = $is_custom_logo ? get_custom_logo() : get_bloginfo( 'name' );

if ( $logo || $tagline ) : ?>

	<div class='<?php echo esc_attr( "$class wrapper $is_tagline" ); ?>'>

		<a class='<?php echo esc_attr( "{$class}__link" ); ?>' href='<?php echo esc_url( home_url( '/' ) ); ?>' rel='home'>

			<?php if ( is_front_page() ) : ?>
				<?php if ( $is_custom_logo ): ?>
					<h1 class='<?php echo esc_attr( "{$class}__logo-img" ); ?>'><?php echo wp_kses_post( get_custom_logo() ); ?></h1>
				<?php else : ?>
					<h1 class='<?php echo esc_attr( "{$class}__logo" ); ?>'><?php echo wp_kses_post( get_bloginfo( 'name' ) ); ?></h1>
				<?php endif; ?>
			<?php else : ?>
				<?php if ( $is_custom_logo ): ?>
					<p class='<?php echo esc_attr( "{$class}__logo-img" ); ?>'><?php echo wp_kses_post( get_custom_logo() ); ?></p>
				<?php else : ?>
					<p class='<?php echo esc_attr( "{$class}__logo" ); ?>'><?php echo wp_kses_post( get_bloginfo( 'name' ) ); ?></p>
				<?php endif; ?>
			<?php endif; ?>

			<?php if ( ! $is_custom_logo && ( $tagline || is_customize_preview() ) ) : ?>
				<p class='<?php echo esc_attr( "{$class}__tagline" ); ?>'><?php echo esc_html( $tagline ); ?></p>
			<?php endif; ?>

		</a>

	</div>

<?php endif;
