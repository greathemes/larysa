<?php
/**
* Header mobile navigation.
*/

$class = 'primary-header-mobile'; ?>

<div class='<?php echo esc_attr( "$class" ); ?>'>
	<div class='<?php echo esc_attr( "{$class}__container" ); ?>'>

		<button type='button' class='<?php echo esc_attr( "{$class}__hide-btn" ); ?>'>
			<span class='<?php echo esc_attr( "{$class}__hide-btn-icon fas fa-times" ); ?>' aria-hidden='true'></span>
			<span class='screen-reader-text'><?php esc_html_e( 'Hide navigation', 'TRANSLATE' ); ?></span>
		</button>

		<?php wpstarter_header_primary_navigation( [
			'class' => "{$class}-nav"
		] ); ?>

		<?php wpstarter_primary_header_social_media( [
			'class' => "{$class}-icons"
		] ); ?>

	</div>
</div>
