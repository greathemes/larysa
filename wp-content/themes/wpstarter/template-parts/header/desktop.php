<?php
/**
* Header desktop navigation.
*/

$class    = 'primary-header-desktop';
$is_kirki = class_exists( 'Kirki' ) ? '' : 'no-kirki'; ?>

<div class='<?php echo esc_attr( "$class" ); ?>'>

	<div class='<?php echo esc_attr( "{$class}__container wrapper" ) ?>'>

		<button type='button' class='<?php echo esc_attr( "{$class}__show-btn $is_kirki" ) ?>'>
			<span class='<?php echo esc_attr( "{$class}__show-btn-icon fas fa-bars" ) ?>' aria-hidden='true'></span>
			<span class='screen-reader-text'><?php esc_html_e( 'Show navigation', 'TRANSLATE' ); ?></span>
		</button>

		<?php wpstarter_header_primary_navigation( [
			'class' => "{$class}-nav"
		] ); ?>

		<div class='<?php echo esc_attr( "{$class}-icons" ); ?>'>
			<?php wpstarter_primary_header_social_media( [
				'class' => "{$class}-icons"
			] );

			wpstarter_primary_header_search( [
				'class' => "{$class}-icons"
			] ); ?>
		</div>

	</div>

</div>
