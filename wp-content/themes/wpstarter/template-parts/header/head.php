<?php
/**
 * Head section.
 */
?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset='<?php bloginfo( 'charset' ); ?>'>
	<meta name='viewport' content='width=device-width, initial-scale=1'>
	<link rel='profile' href='//gmpg.org/xfn/11'>
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

	<div class='site-container'>

		<a class='skip-link screen-reader-text' href='#site-area'><?php esc_html_e( 'Skip to content', 'TRANSLATE' ); ?></a>
