<?php
/**
 * Displays search content area.
 *
 * @since 1.0.0
 *
 * @package wpstarter
 */

do_action( 'wpstarter_search', get_query_var( 'post_index' ) );
