<?php
/**
* Front page WooCommerce products.
*/

if ( class_exists( 'WooCommerce' ) ) :

	$wp_query = new WP_Query( [
		'post_type'      => 'product',
		'posts_per_page' => 4,
	] );

	if ( $wp_query->have_posts() ) :

		$class      = 'front-page-woo-products';
		$index      = 0;
		$inline_css = ''; ?>

		<section class='<?php echo esc_attr( $class ); ?>'>

			<header class='<?php echo esc_attr( "{$class}__header {$class}__header--wrapper" ); ?>'>
				<h2 class='<?php echo esc_attr( "{$class}__heading" ); ?>'><?php esc_html_e( 'Products', 'TRANSLATE' ); ?></h2>
			</header>

			<div class='<?php echo esc_attr( "{$class}__container {$class}__container--wrapper" ); ?>'>

				<ul class='<?php echo esc_attr( "{$class}__list {$class}__list--unstyled" ); ?>'>

					<?php while ( $wp_query->have_posts() ) : $wp_query->the_post();

						set_query_var( 'index', $index );
						wc_get_template_part( 'content', 'product' );

						$inline_css .= get_query_var( 'inline_css' );
						$index++;

					endwhile; ?>

				</ul>

			</div>

		</section>

		<?php
		wp_reset_postdata();

		wp_enqueue_style( 'wpstarter-woocommerce-star-rating-style', WPSTARTER_THEME_URI . 'style.css', [], '1.0.0', 'all' );
		wp_add_inline_style( 'wpstarter-woocommerce-star-rating-style', wp_strip_all_tags( $inline_css ) );

	endif;

endif;
