<?php
/**
* Front page sider.
*/

$front_page = wpstarter_get_options_front_page_slider();

if ( class_exists( 'Kirki' ) && $front_page['visibility'] ) :

	add_action( 'pre_get_posts', 'wpstarter_custom_query_sticky_posts_fix' );

	$query = new WP_Query( [
		'post_type'        => 'post',
		'posts_per_page'   => $front_page['slides_num'],
		'sticky_posts_fix' => true
	] );

	remove_action( 'pre_get_posts', 'wpstarter_custom_query_sticky_posts_fix' );

	if ( $query->have_posts() ) :

		$class = 'front-page-slider';
		$index = 0; ?>

		<section class='<?php echo esc_attr( "$class" ); ?>'>

			<div class='<?php echo esc_attr( "{$class}__container {$class}__container--wrapper" ); ?>'>

				<div class='<?php echo esc_attr( "{$class}__inner-container owl-carousel" ); ?>' data-custom-anim='<?php echo esc_attr( $front_page['custom_anim'] ); ?>' data-anim-in-type='<?php echo esc_attr( $front_page['custom_anim_in'] ); ?>' data-anim-out-type='<?php echo esc_attr( $front_page['custom_anim_out'] ); ?>' data-rewind='<?php echo esc_attr( $front_page['rewind'] ); ?>' data-autoplay='<?php echo esc_attr( $front_page['autoplay'] ); ?>' data-autoplay-time='<?php echo esc_attr( $front_page['autoplay_time'] ); ?>' data-autoplay-pause='<?php echo esc_attr( $front_page['autoplay_pause'] ); ?>'>

					<?php while ( $query->have_posts() ) :

						$query->the_post();

						do_action( 'wpstarter_blog_posts', [
							'index'                   => $query->current_post,
							'class'                   => $class,
							'is_slider'               => true,
							'slides_num'              => $front_page['slides_num'],
							'featured_excerpt_length' => 40,
							'all_featured'            => true,
							'image_class'             => "{$class}__img {$class}__img--absolute-centered {$class}__img--covered",
							'image_size'              => 'wpstarter_front_page_slider',
							'image_sizes'             => [ 'wpstarter_portfolio', 'wpstarter_front_page_slider', 'wpstarter_portfolio_pswp', 'wpstarter_post_retina' ],
							'lazy'                    => $index >= 2 ? true : false,
						] );

						$index++;

					endwhile;

					wp_reset_postdata(); ?>

				</div>

			</div>

		</section>

	<?php endif;

endif;
