<?php
/**
* Front page blog.
*/

$blog           = wpstarter_get_options_front_page_blog();
$visibility     = $blog['visibility'];
$heading        = $blog['heading_text'];
$posts_per_page = $blog['posts_num'];
$featured       = $blog['featured'];
$btn_visibility = $blog['btn_visibility'];
$btn_text       = $blog['btn_text'];
$btn_page_url   = $blog['btn_page_url'];
$class   = 'front-page-blog';

if ( class_exists( 'Kirki' ) && $visibility ) :

	add_action( 'pre_get_posts', 'wpstarter_custom_query_sticky_posts_fix' );

	$query = new WP_Query( [
		'post_type'        => 'post',
		'posts_per_page'   => $posts_per_page,
		'sticky_posts_fix' => true
	] );

	remove_action( 'pre_get_posts', 'wpstarter_custom_query_sticky_posts_fix' );

	if ( $query->have_posts() ) : ?>

		<section class='<?php echo esc_attr( $class ); ?>'>
			
			<?php if ( $heading ) : ?>		
				<header class='<?php echo esc_attr( "{$class}__header wrapper" ); ?>'>
					<h2 class='<?php echo esc_attr( "{$class}__heading" ); ?>'><?php echo esc_html( $heading ); ?></h2>
				</header>
			<?php endif; ?>

			<div class='<?php echo esc_attr( "{$class}__container wrapper" ); ?>'>

				<?php if ( wpstarter_get_options_front_page_blog()['featured'] ) : ?>
					<div class='<?php echo esc_attr( "{$class}__featured-inner-container" ); ?>'></div>
				<?php endif; ?>

				<div class='<?php echo esc_attr( "{$class}__inner-container" ); ?>'>

					<?php while ( $query->have_posts() ) :

						$query->the_post();

						do_action( 'wpstarter_blog_posts', [
							'index'                   => $query->current_post,
							'class'            => $class,
							'first_featured'          => $featured,
							'excerpt_length'          => 20,
							'featured_excerpt_length' => 40,
							'image_class'             => "{$class}__img {$class}__img--covered",
							'lazy'                    => true,
						] );

					endwhile;

					wp_reset_postdata(); ?>

			</div>

		</div>

		<?php if ( $btn_visibility && $btn_text && $btn_page_url ): ?>
			<div class='<?php echo esc_attr( "{$class}__btn-container wrapper" ); ?>'>
				<a class='<?php echo esc_attr( "{$class}__btn-link {$class}__btn-link--button" ); ?>' href='<?php echo esc_url( get_permalink( $btn_page_url ) ); ?>'><?php echo esc_html( $btn_text ); ?></a>
			</div>
		<?php endif; ?>

	</section>

	<?php endif;

endif;
