<?php
/**
* Front page testimonials.
*/

if ( class_exists( 'Kirki' ) ) :

	$testimonials = wpstarter_get_options_front_page_testimonials();
	$items        = $testimonials['items'];

	if ( $testimonials['visibility'] && wpstarter_kirki_repeater_check_required_fields( $items, [ 'author', 'quote' ] ) ) :

		$heading      = $testimonials['heading_text'];
		$class = 'front-page-testimonials';
		$i            = 0; ?>

		<section class='<?php echo esc_attr( $class ); ?>'>

			<?php if ( $heading ) : ?>
				<header class='<?php echo esc_attr( "{$class}__header wrapper" ); ?>'>
					<h2 class='<?php echo esc_attr( "{$class}__heading" ); ?>'><?php echo esc_html( $heading ); ?></h2>
				</header>
			<?php endif; ?>

			<div class='<?php echo esc_attr( "{$class}__container wrapper" ); ?>'>

				<?php if ( sizeof( $items ) > 1 ) : ?>
					<div class='<?php echo esc_attr( "{$class}__nav" ); ?>'>
						<button type='button' class='<?php echo esc_attr( "{$class}__nav-btn prev fa fa-long-arrow-alt-left" ); ?>'></button>
						<button type='button' class='<?php echo esc_attr( "{$class}__nav-btn next fa fa-long-arrow-alt-right" ); ?>'></button>
					</div>
				<?php endif; ?>

				<div id='front_page_testimonials__slider' class='<?php echo esc_attr( "{$class}__slider owl-carousel" ); ?>' data-custom-anim='<?php echo esc_attr( $testimonials['slider_custom_anim'] ); ?>' data-anim-in-type='<?php echo esc_attr( $testimonials['slider_custom_anim_in'] ); ?>' data-anim-out-type='<?php echo esc_attr( $testimonials['slider_custom_anim_out'] ); ?>' data-rewind='<?php echo esc_attr( $testimonials['slider_rewind'] ); ?>' data-autoplay='<?php echo esc_attr( $testimonials['slider_autoplay'] ); ?>' data-autoplay-time='<?php echo esc_attr( $testimonials['slider_autoplay_time'] ); ?>' data-autoplay-pause='<?php echo esc_attr( $testimonials['slider_autoplay_pause'] ); ?>'>

					<?php foreach ( $items as $item ) :

						$author = $item['author'];
						$job    = $item['job'];
						$quote  = $item['quote'];
						$img_id = $item['img'];
						$stars  = $item['stars'];

						if ( $author && $quote ) :

							$img_class   = $img_id ? '' : 'no-img';
							$job_class   = $job ? '' : 'no-work';
							$stars_class = $stars ? '' : 'no-stars'; ?>

							<div class='<?php echo esc_attr( "{$class}__item $img_class $stars_class $job_class" ); ?>'>

								<span class='<?php echo esc_attr( "{$class}__quote-icon fa fa-quote-right" ); ?>'></span>

								<?php if ( $img_id ) : ?>
									<div class='<?php echo esc_attr( "{$class}__img-container" ); ?>'>
										<div class='<?php echo esc_attr( "{$class}__img-inner-container" ); ?>'>
											<?php wpstarter_post_thumbnail( $img_id, [
												'size'  => 'wpstarter_front_page_testimonials',
												'sizes' => [ 'wpstarter_front_page_testimonials', 'wpstarter_portfolio_medium' ],
												'class' => "{$class}__img {$class}__img--covered",
												'lazy'  => true,
											] ); ?>
										</div>
									</div>
								<?php endif; ?>
								
								<div class='<?php echo esc_attr( "{$class}__content-container" ); ?>'>

									<p class='<?php echo esc_attr( "{$class}__quote" ); ?>'><?php echo esc_html( $quote ); ?></p>
									<h4 class='<?php echo esc_attr( "{$class}__author" ); ?>'><?php echo esc_html( $author ); ?></h4>

									<?php if ( $job ) : ?>
										<div class='<?php echo esc_attr( "{$class}__work" ); ?>'><?php echo esc_html( $job ); ?></div>
									<?php endif; ?>
									
									<?php if ( $stars ) : ?>

										<div class='<?php echo esc_attr( "{$class}__stars" ); ?>'>

											<?php $stars_num = $stars / 2;
											$is_integer      = is_int( $stars_num );
											$stars_num       = $is_integer ? $stars_num : $stars_num - 1;

											for ( $j = 0; $j < $stars_num; $j++ ) : ?>
												<span class='<?php echo esc_attr( "{$class}__stars-icon fa fa-star" ); ?>'></span>
											<?php endfor;

											if ( ! $is_integer ) : ?>
												<span class='<?php echo esc_attr( "{$class}__stars-icon fa fa-star-half" ); ?>'></span>
											<?php endif; ?>

										</div>

									<?php endif; ?>

									<div class='<?php echo esc_attr( "{$class}__num" ); ?>'><?php echo esc_html( $i < 10 ? ( '0' . ( $i + 1 ) ) : ( $i + 1 ) ); ?></div>

								</div>

							</div>	

						<?php endif;

						$i++;

					endforeach; ?>

				</div>

			</div>

		</section>

	<?php endif;

endif;
