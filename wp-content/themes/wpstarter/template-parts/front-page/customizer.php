<?php
/**
* Front page customizer.
*/

$slider_visibility         = wpstarter_get_options_front_page_slider()['visibility'];
$blog_visibility           = wpstarter_get_options_front_page_blog()['visibility'];
$testimonials_visibility   = wpstarter_get_options_front_page_testimonials()['visibility'];
$portfolio_visibility      = wpstarter_get_options_front_page_portfolio()['visibility'];
$class                     = 'front-page-customizer';
$query['autofocus[panel]'] = 'wpstarter_options';
$panel_link                = class_exists( 'Kirki' ) ? add_query_arg( $query, admin_url( 'customize.php' ) ) : '#';

if ( ( is_user_logged_in() && current_user_can( 'administrator' ) ) && ( ! $slider_visibility && ! $blog_visibility && ! $testimonials_visibility && ! $portfolio_visibility ) ) : ?>

		<section class='<?php echo esc_attr( "$class wrapper" ); ?>'>

			<?php if ( ! class_exists( 'Kirki' ) ) : ?>
				<p class='<?php echo esc_attr( "{$class}__content" ) ?>'>
					<?php printf(
						// translators: %1$s: Start of the HTML link tag.
						// translators: %2$s: End of the HTML link tag.
						esc_html__( 'To activate the button, you must have the %1$sKirki%2$s plugin activated, because this theme is based on Customizer framework.', 'TRANSLATE' ),
						'<a href="https://wordpress.org/plugins/kirki/" target="_blank">', '</a>'
					); ?>
				</p>
			<?php endif; ?>

			<a class='<?php echo esc_attr( "{$class}__link {$class}__link--button" . ( ! class_exists( 'Kirki' ) ? " {$class}__link--is-not-active" : '' ) ) ?>' href='<?php echo esc_url( $panel_link ); ?>'>
				<?php esc_html_e( 'No front page content? Go to the WordPress Customizer.', 'TRANSLATE' ); ?>
			</a>

		</section>

<?php endif;
