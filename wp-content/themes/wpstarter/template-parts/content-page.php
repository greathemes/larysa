<?php
/**
 * Displays content area.
 *
 * @since 1.0.0
 *
 * @package wpstarter
 */

do_action( 'wpstarter_page' );
