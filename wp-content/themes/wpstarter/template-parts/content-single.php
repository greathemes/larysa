<?php
/**
 * Displays single post content.
 *
 * @since 1.0.0
 *
 * @package wpstarter
 */

do_action( 'wpstarter_single_blog_post' );
