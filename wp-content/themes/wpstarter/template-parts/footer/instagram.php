<?php
/**
* Footer Instagram.
*/

$footer  = get_query_var( 'footer_options' );
$token   = $footer['instagram_access_token'];
$new_tab = $footer['instagram_new_tab'] ? 'target="_blank"' : '';

if ( $footer['instagram_visibility'] && $token ) :

	if ( false === ( $feed = get_transient( 'wpstarter_transient_instagram_feed' ) ) ) :

		$remote = wp_remote_get( 'https://api.instagram.com/v1/users/self/?access_token=' . $token );

		if ( $remote && $remote['response']['code'] === 200 ) :

			$body = json_decode( $remote['body'] );
			$data = $body->data;

			if ( $data->counts->media ) :

				$remote = wp_remote_get( 'https://api.instagram.com/v1/users/self/media/recent/?access_token=' . $token );

				if ( $remote['response']['code'] === 200 ) :

					$body = json_decode( $remote['body'] );
					$feed = $body->data;

					set_transient( 'wpstarter_transient_instagram_feed', $feed, 60 * 60 );

				endif;

			endif;

		endif;

	endif;

	if ( isset( $feed ) && $feed ) :

		$class = 'instagram-gallery'; ?>

		<div class='<?php echo esc_attr( $class ); ?>'>

			<div class='<?php echo esc_attr( "{$class}__slider owl-carousel" ); ?>'>

				<?php foreach ( $feed as $feed_item ) : ?>
					<a href='<?php echo esc_url( $feed_item->link ); ?>' class='<?php echo esc_attr( "{$class}__link" ); ?>' id='<?php echo esc_attr( 'media-' . $feed_item->id ); ?>' <?php echo esc_attr( $new_tab ); ?>>
						<img class='<?php echo esc_attr( "{$class}__img {$class}__img--zoom-animation {$class}__img--covered lazy" ); ?>' src='data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7' data-src='<?php echo esc_url( $feed_item->images->low_resolution->url ); ?>' title='<?php echo esc_attr( $feed_item->caption->text ); ?>'>
					</a>
				<?php endforeach; ?>

			</div>

		</div>

	<?php endif;

endif;
