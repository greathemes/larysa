<?php
/**
* Footer social media.
*/

$footer = get_query_var( 'footer_options' );
$items  = $footer['social_media'];
$class  = 'footer-social-media';

if ( wpstarter_kirki_repeater_check_required_fields( $items, [ 'website', 'url' ] ) ) : ?>

	<div class='<?php echo esc_attr( $class ); ?>'>

		<ul class='<?php echo esc_attr( "{$class}__list {$class}__list--unstyled" ); ?>'>

			<?php foreach ( $items as $item ) :

				$slug = $item['website'];
				$url  = $item['url'];

				if ( $slug && $url ) : ?>

					<li class='<?php echo esc_attr( "{$class}__item" ); ?>'>
						<a class='<?php echo esc_attr( "{$class}__link" ); ?>' href='<?php echo esc_url( $url ); ?>' <?php echo esc_attr( $footer['social_media_new_tab'] ? 'target=_blank' : '' ); ?>>
							<span class='<?php echo esc_attr( "{$class}__icon fab fa-{$slug}" ); ?>' aria-hidden='true'></span>
							<span class='<?php echo esc_attr( "{$class}__title" ); ?>'><?php echo esc_html( wpstarter_get_social_media_arr( $slug ) ); ?></span>
						</a>
					</li>

				<?php endif;

			endforeach; ?>

		</ul>

	</div>

<?php endif;
