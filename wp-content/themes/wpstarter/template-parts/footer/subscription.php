<?php
/**
* Footer subscription.
*/

$footer = get_query_var( 'footer_options' );

if ( $footer['subscription_visibility'] ) : ?>

	<div class='footer-subscription'>
		<div class='footer-subscription__wrapper container'>

			<h3 class='footer-subscription__heading'><?php echo esc_html( $footer['subscription_title_text'] ); ?></h3>

			<?php echo do_shortcode( '[contact-form-7 id="' . esc_html( $footer['subscription_forms'] ) . '"]', false ); ?>

		</div>
	</div>

<?php endif;
