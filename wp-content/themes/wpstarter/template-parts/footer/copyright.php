<?php
/**
* Footer copyright.
*/

$footer    = get_query_var( 'footer_options' );
$copyright = $footer['copyright_text'];

if ( $copyright ) : ?>

	<div class='footer-copyright'><?php echo wp_kses_post( $copyright ); ?></div>

<?php endif;
