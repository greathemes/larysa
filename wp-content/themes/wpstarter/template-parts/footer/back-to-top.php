<?php
/**
* Footer back to top button.
*/

$footer = get_query_var( 'footer_options' );

if ( $footer['back_to_top_btn_visibility'] ) : ?>

	<button type='button' class='back-to-top-button back-to-top-button--button back-to-top-button--is-hidden fa fa-long-arrow-alt-up'></button>

<?php endif;
