<?php
/**
 * Displays post content.
 *
 * @since 1.0.0
 *
 * @package wpstarter
 */

do_action( 'wpstarter_blog_posts', [
	'index'                   => get_query_var( 'post_index' ),
	'class'            => 'blog-page-posts',
	'first_featured'          => wpstarter_get_options_blog()['featured'],
	'excerpt_length'          => 20,
	'featured_excerpt_length' => 40,
	'image_class'             => 'blog-page-posts__img blog-page-posts__img--covered'
] );
