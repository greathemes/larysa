<?php
/**
 * The template for displaying the footer.
 */

do_action( 'wpstarter_site_content_end' );

$footer             = wpstarter_get_options_footer();
$social_media       = $footer['social_media'];
$social_media_exist = wpstarter_kirki_repeater_check_required_fields( $social_media, [ 'website', 'url' ] );
$copyright          = $footer['copyright_text'];

set_query_var( 'footer_options', $footer ); ?>

<footer class='site-footer'>

	<?php if ( ! is_404() ) :
		
		get_template_part( 'template-parts/footer/subscription' );
		get_template_part( 'template-parts/footer/instagram' );

		if ( ( $social_media_exist || $copyright ) ) : ?>

			<div class='footer-area'>
				<div class='wrapper'>
					<?php get_template_part( 'template-parts/footer/social-media' ); ?>
					<?php get_template_part( 'template-parts/footer/copyright' ); ?>
				</div>
			</div>

		<?php endif;

	get_template_part( 'template-parts/footer/back-to-top' );

	endif; ?>

</footer>

</div>

<?php wp_footer(); ?>

</body>
</html>
