<?php
/**
 * The loop template file.
 *
 * @since 1.0.0
 *
 * @package wpstarter
 */

$template = wpstarter_loop_content_template();
$index    = 0;

while ( have_posts() ) :

	the_post();

	/**
	 * Include the Post-Format-specific template for the content.
	 * If you want to override this in a child theme, then include a file
	 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
	 */

	set_query_var( 'post_index', $index );
	get_template_part( 'template-parts/content', $template );

	$index++;

endwhile;
