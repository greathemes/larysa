<?php
/**
 * Plugin Name: Wpstarter Author
 * Plugin URI: https://wpstarter.greathemes.com/
 * Description: Author area for single blog posts.
 * Version: 1.0.0
 * Author: Greathemes
 * Author URI: https://greathemes.com/
 * License: GNU General Public License v2.
 * License URI: http://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain: wpstarter-author
 * Domain Path: /languages
 *
 * @package wpstarter
 * @author Greathemes
 * @license GPL-2.0+
 */

if ( ! defined( 'ABSPATH' ) ) : exit; endif;

// Constants.
define( 'WPSTARTER_AUTHOR_DIR', trailingslashit( plugin_dir_path( __FILE__ ) ) );
define( 'WPSTARTER_AUTHOR_INCLUDES', trailingslashit( plugin_dir_path( __FILE__ ) . '/inc' ) );
define( 'WPSTARTER_AUTHOR_URL', trailingslashit( plugins_url() . '/wpstarter-author' ) );
define( 'WPSTARTER_AUTHOR_JS', trailingslashit( WPSTARTER_AUTHOR_URL . '/assets/js' ) );

// Init.
require_once WPSTARTER_AUTHOR_DIR . 'class-wpstarter-author-init.php';
require_once WPSTARTER_AUTHOR_INCLUDES . 'template-hooks.php';
require_once WPSTARTER_AUTHOR_INCLUDES . 'template-functions.php';
