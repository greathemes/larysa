<?php
/**
 * Plugin uninstaller.
 *
 * @since 1.0.0
 *
 * @package wpstarter
 */

if ( ! defined( 'WP_UNINSTALL_PLUGIN' ) ) : die; endif;

$users = get_users();

foreach ( $users as $user ) :
	
	$user_id = $user->ID;

	// Remove social media.
	delete_user_meta( $user_id, 'facebook', false );
	delete_user_meta( $user_id, 'twitter', false );
	delete_user_meta( $user_id, 'linkedin', false );
	delete_user_meta( $user_id, 'instagram', false );
	delete_user_meta( $user_id, 'pinterest', false );
	delete_user_meta( $user_id, 'flickr', false );
	delete_user_meta( $user_id, 'youtube', false );
	delete_user_meta( $user_id, 'vimeo', false );

	// Remove custom avatar.
	delete_user_meta( $user_id, 'wpstarter_custom_avatar', false );

endforeach;
