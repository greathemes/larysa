<?php
/**
 * Custom template & tag functions.
 *
 * @since 1.0.0
 *
 * @package wpstarter
 */

if ( ! function_exists( 'wpstarter_author_info' ) ) :

	/**
	 * Informations about post author.
	 */
	function wpstarter_author_info( $user_id ) {

		$visibility = is_singular( 'post' ) ? wpstarter_get_options_blog()['single_author_visibility'] : wpstarter_get_options_portfolio()['single_author_visibility'];
		$id         = get_the_author_meta( 'ID' ) ? get_the_author_meta( 'ID' ) : get_post_field( 'post_author', get_the_ID() );
		$name       = get_the_author_meta( 'display_name', $id );
		$nickname   = get_the_author_meta( 'nickname', $id );
		$desc       = get_the_author_meta( 'user_description', $id );
		$web_link   = get_the_author_meta( 'url', $id );
		$web_desc   = $web_link ? sprintf(
			// translators: %s: Name of the website.
			esc_html__( 'Visit me on %s', 'TRANSLATE' ),
			'<span>' . parse_url( $web_link )['host'] . '</span>'
		) : '';
		$avatar       = wpstarter_avatar( $id, 400, 'wpstarter_avatar_small', [ 'wpstarter_avatar_small', 'wpstarter_avatar' ] );
		$social_media = [
			'facebook-f'  => get_the_author_meta( 'facebook', $id ),
			'twitter'     => get_the_author_meta( 'twitter', $id ),
			'linkedin-in' => get_the_author_meta( 'linkedin', $id ),
			'instagram'   => get_the_author_meta( 'instagram', $id ),
			'pinterest-p' => get_the_author_meta( 'pinterest', $id ),
			'flickr'      => get_the_author_meta( 'flickr', $id ),
			'youtube'     => get_the_author_meta( 'youtube', $id ),
			'vimeo-v'     => get_the_author_meta( 'vimeo', $id )
		];
		$parent_class = 'author-area';

		if ( $visibility && $desc ) : ?>

			<div class='<?php echo esc_attr( $parent_class ); ?>'>
				<?php if ( $avatar ) : ?>
					<div class='<?php echo esc_attr( "{$parent_class}__img-container" ); ?>'><?php echo wp_kses_post( $avatar ); ?></div>
				<?php endif; ?>
				<div class='<?php echo esc_attr( "{$parent_class}__content-container" ); ?>'>
					<?php if ( $name || $nickname ) : ?>
						<p class='<?php echo esc_attr( "{$parent_class}__name" ); ?>'><?php echo esc_html( $name ? $name : $nickname ); ?></p>
					<?php endif; ?>
					<?php if ( $desc ) : ?>
						<p class='<?php echo esc_attr( "{$parent_class}__desc" ); ?>'><?php echo esc_html( $desc ); ?></p>
					<?php endif; ?>
					<?php if ( sizeof( array_filter( $social_media ) ) ) : ?>
						<div class='<?php echo esc_attr( "{$parent_class}__social-media-container" ); ?>'>
							<ul class='<?php echo esc_attr( "{$parent_class}__social-media-list {$parent_class}__social-media-list--unstyled" ); ?>'>
								<?php foreach ( $social_media as $k => $v ) :
									if ( $v ) : ?>
										<li class='<?php echo esc_attr( "{$parent_class}__social-media-item" ); ?>'>
											<a class='<?php echo esc_attr( "{$parent_class}__social-media-link" ); ?>' href='<?php echo esc_url( $v ); ?>' target='_blank'>
												<span class='<?php echo esc_attr( "{$parent_class}__social-media-link-icon fab fa-{$k}" ); ?>'></span>
											</a>
										</li>
									<?php endif;
								endforeach; ?>
							</ul>
						</div>
					<?php endif; ?>
					<?php if ( $web_link ) : ?>
						<div class='<?php echo esc_attr( "{$parent_class}__website" ); ?>'>
							<a class='<?php echo esc_attr( "{$parent_class}__website-link" ); ?>' href='<?php echo esc_url( $web_link ); ?>'><?php echo wp_kses_post( $web_desc );  ?></a>
						</div>
					<?php endif; ?>
				</div>
			</div>

		<?php endif;

	}

endif;
