<?php
/**
 * Custom hooks for wpstarter theme.
 *
 * @since 1.0.0
 *
 * @package wpstarter
 */

// Blog single post.
add_action( 'wpstarter_single_blog_post', 'wpstarter_author_info', 35 );

// Single Portfolio.
if ( class_exists( 'Wpstarter_Portfolio_Init' ) ) :
	add_action( 'wpstarter_single_portfolio', 'wpstarter_author_info', 65 );
endif;
