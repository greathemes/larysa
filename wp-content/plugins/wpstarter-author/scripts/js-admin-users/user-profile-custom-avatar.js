/**
 * User profile avatar.
 *
 * @since 1.0.0
 *
 * @package wpstarter
 */

( function( $ ) {

	'use strict';

	// WP Media Uploader.
	var _wpstarter_media = true;
	var _orig_send_attachment = wp.media.editor.send.attachment;

	jQuery( '.wpstarter-image' ).on( 'click', function() {

		var button       = jQuery( this ),
		textbox_id       = jQuery( this ).attr( 'data-id' ),
		image_id         = jQuery( this ).attr( 'data-src' ),
		_wpstarter_media = true;

		wp.media.editor.send.attachment = function( props, attachment ) {

			if ( _wpstarter_media && ( attachment.type === 'image' ) ) {
				if ( image_id.indexOf( "," ) !== -1 ) {
					image_id = image_id.split( "," );
					$image_ids = '';
					jQuery.each( image_id, function( key, value ) {
						if ( $image_ids )
							$image_ids = $image_ids + ',#' + value;
						else
							$image_ids = '#' + value;
					} );

					var current_element = jQuery( $image_ids );
				} else {
					var current_element = jQuery( '#' + image_id );
				}

				jQuery( '#' + textbox_id ).val( attachment.id );
				current_element.attr( 'src', attachment.url ).show();

			} else {
				return false;
			}

		}

		wp.media.editor.open( button );

		return false;

	} );

	jQuery( '.wpstarter-image-remove' ).on( 'click', function() {

		var image_id = jQuery( this ).attr( 'data-src' );

		$( '#' + image_id ).attr( 'src', '' );
		$( '#wpstarter_image_id' ).val( '' );

	} );

} )( jQuery );
