<?php
/**
 * Widgets initializer.
 *
 * @since 1.0.0
 *
 * @package wpstarter
 */

if ( ! class_exists( 'Wpstarter_Author_Init' ) ) :

	/**
	 * Wpstarter_Author_Init.
	 */
	class Wpstarter_Author_Init {

		/**
		 * Instance.
		 *
		 * @access private
		 * @var object Class object.
		 */
		private static $instance;

		/**
		 * Initiator.
		 *
		 * @return object initialized object of class.
		 */
		public static function get_instance() {

			if ( ! isset( self::$instance ) ) :

				self::$instance = new self();

			endif;

			return self::$instance;

		}

		/**
		 * Constructor.
		 */
		public function __construct() {

			add_action( 'admin_enqueue_scripts', [ $this, 'enqueue_admin_scripts' ] );

			add_action( 'plugins_loaded', [ $this, 'load_plugin_lang' ] );
			add_action( 'activated_plugin', 'flush_rewrite_rules'  );
			add_action( 'deactivated_plugin', 'flush_rewrite_rules' );

			add_filter( 'user_contactmethods', [ $this, 'user_contact_methods' ], 10, 1 );

			add_action( 'show_user_profile', [ $this, 'add_user_profile_image_field' ] );
			add_action( 'edit_user_profile', [ $this, 'add_user_profile_image_field' ] );
			add_action( 'user_new_form', [ $this, 'add_user_profile_image_field' ] );

			add_action( 'profile_update', [ $this, 'user_profile_update' ] );
			add_action( 'user_register', [ $this, 'user_profile_update' ] );

		}

		/**
		 * Enqueue scripts.
		 */
		public function enqueue_admin_scripts( $hook ) {

			if ( $hook === 'profile.php' ) :
				wp_enqueue_script( 'wpstarter-scripts-admin-users-min', WPSTARTER_AUTHOR_JS . 'scripts-admin-users.min.js', [ 'jquery' ], '1.0.0', true );
			endif;

		}

		/**
		 * Load text domain.
		 */
		public function load_plugin_lang() {

			load_plugin_textdomain( 'TRANSLATE', FALSE, basename( dirname( __FILE__ ) ) . '/languages/' );

		}

		public function user_contact_methods( $contact_methods ) {

			$contact_methods['facebook']  = esc_html__( 'Facebook URL', 'TRANSLATE' );
			$contact_methods['twitter']   = esc_html__( 'Twitter URL', 'TRANSLATE' );
			$contact_methods['linkedin']  = esc_html__( 'LinkedIn URL', 'TRANSLATE'    );
			$contact_methods['instagram'] = esc_html__( 'Instagram URL', 'TRANSLATE'    );
			$contact_methods['pinterest'] = esc_html__( 'Pinterest URL', 'TRANSLATE'    );
			$contact_methods['flickr']    = esc_html__( 'Flickr URL', 'TRANSLATE'    );
			$contact_methods['youtube']   = esc_html__( 'YouTube URL', 'TRANSLATE' );
			$contact_methods['vimeo']     = esc_html__( 'Vimeo URL', 'TRANSLATE' );

			return $contact_methods;

		}

		public function add_user_profile_image_field( $user ) {

			$profile_img = ( $user !== 'add-new-user' ) ? get_user_meta( $user->ID, 'wpstarter_custom_avatar', true ) : false;
			$img         = ! empty( $profile_img ) ? wp_get_attachment_image_src( $profile_img, 'thumbnail' ) : ''; ?>

			<table class='form-table fh-profile-upload-options'>
				<tr>
					<th><label for='image'><?php esc_html_e( 'Profile picture without gravatar', 'TRANSLATE' ); ?></label></th>
					<td>
						<img id='wpstarter-img' src='<?php echo esc_url( ! empty( $profile_img ) ? $img[0] : '' ); ?>' height='100' />
						<div>
							<input type='button' data-id='wpstarter_image_id' data-src='wpstarter-img' class='button wpstarter-image' name='wpstarter_image' id='wpstarter-image' value='Upload' />
							<input type='button' data-src='wpstarter-img' class='button wpstarter-image-remove' value='Remove' />
						</div>
						<input type='hidden' class='button' name='wpstarter_image_id' id='wpstarter_image_id' value='<?php echo esc_attr( ! empty( $profile_img ) ? $profile_img : '' ); ?>' />
						<p class='description'><?php esc_html_e( 'By default, the gravatar image is used. However, you can load your own here without using gravatar. This photo will be used only on your single blog posts and albums.', 'TRANSLATE' ); ?></p>
					</td>
				</tr>
			</table>

		<?php }

		public function user_profile_update( $user_id ) {

			if ( current_user_can( 'edit_users' ) ) :
				$profile_pic = empty( $_POST['wpstarter_image_id'] ) ? '' : $_POST['wpstarter_image_id'];
				update_user_meta( $user_id, 'wpstarter_custom_avatar', $profile_pic );
			endif;

		}

	}

	// Get instance.
	Wpstarter_Author_Init::get_instance();

endif;
