<?php
/**
 * Plugin Name: Wpstarter Portfolio
 * Plugin URI: https://wpstarter.greathemes.com/
 * Description: Portfolio for Wpstarter theme.
 * Version: 1.0.0
 * Author: Greathemes
 * Author URI: https://greathemes.com/
 * License: GNU General Public License v2.
 * License URI: http://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain: wpstarter-portfolio
 * Domain Path: /languages
 *
 * @package wpstarter
 * @author Greathemes
 * @license GPL-2.0+
 */

if ( ! defined( 'ABSPATH' ) ) : exit; endif;

// Constants.
define( 'WPSTARTER_PORTFOLIO_DIR', trailingslashit( plugin_dir_path( __FILE__ ) ) );
define( 'WPSTARTER_PORTFOLIO_INCLUDES', trailingslashit( plugin_dir_path( __FILE__ ) . '/inc' ) );
define( 'WPSTARTER_PORTFOLIO_CUSTOMIZER', trailingslashit( plugin_dir_path( __FILE__ ) . '/inc/customizer' ) );
define( 'WPSTARTER_PORTFOLIO_TEMPLATES', trailingslashit( plugin_dir_path( __FILE__ ) . '/templates' ) );
define( 'WPSTARTER_PORTFOLIO_TEMPLATE_PARTS', trailingslashit( plugin_dir_path( __FILE__ ) . '/template-parts' ) );
define( 'WPSTARTER_PORTFOLIO_URL', trailingslashit( plugins_url() . '/wpstarter-portfolio' ) );

// Init.
require_once WPSTARTER_PORTFOLIO_DIR . 'class-wpstarter-portfolio.php';

// Includes.
require_once WPSTARTER_PORTFOLIO_INCLUDES . 'class-wpstarter-portfolio-cpt.php';
require_once WPSTARTER_PORTFOLIO_INCLUDES . 'class-wpstarter-portfolio-taxonomy.php';
require_once WPSTARTER_PORTFOLIO_INCLUDES . 'class-wpstarter-portfolio-load-more.php';
require_once WPSTARTER_PORTFOLIO_INCLUDES . 'template-hooks.php';
require_once WPSTARTER_PORTFOLIO_INCLUDES . 'template-functions.php';
