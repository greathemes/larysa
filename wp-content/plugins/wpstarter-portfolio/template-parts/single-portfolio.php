<?php
/**
 * The template for displaying all single posts.
 *
 * @since 1.0.0
 *
 * @package wpstarter
 *
 * Displays single portfolio content.
 *
 * @see wpstarter_site_content_area_start  - 10
 * @see wpstarter_single_portfolio_content - 20
 * @see wpstarter_site_content_area_end    - 30
 */

get_header();

do_action( 'wpstarter_single_portfolio' );

get_footer();
