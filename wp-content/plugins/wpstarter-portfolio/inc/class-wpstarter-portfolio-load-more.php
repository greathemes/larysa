<?php
/**
 * Portfolio load more.
 *
 * @since 1.0.0
 *
 * @package wpstarter
 */

if ( ! class_exists( 'Wpstarter_Portfolio_Load_more' ) ) :

	/**
	 * Wpstarter_Portfolio_Load_more.
	 */
	class Wpstarter_Portfolio_Load_more {

		/**
		 * Instance.
		 *
		 * @access private
		 * @var object Class object.
		 */
		private static $instance;

		/**
		 * Initiator.
		 *
		 * @return object initialized object of class.
		 */
		public static function get_instance() {

			if ( ! isset( self::$instance ) ) :

				self::$instance = new self();

			endif;

			return self::$instance;

		}

		/**
		 * Constructor.
		 */
		public function __construct() {

			add_action( 'wp_ajax_nopriv_load_more', [ $this, 'load_more' ] );
			add_action( 'wp_ajax_load_more', [ $this, 'load_more' ] );

		}

		/**
		 * Loads more posts.
		 */
		public function load_more() {

			$params          = ( isset( $_POST['query'], $_POST['nonce'] ) && wp_verify_nonce( sanitize_key( $_POST['nonce'] ), 'ajax-nonce' ) ) ? json_decode( sanitize_text_field( wp_unslash( $_POST['query'] ) ), true ) : '';
			$params['paged'] = isset( $_POST['page'], $_POST['nonce'] ) ? sanitize_text_field( wp_unslash( $_POST['page'] + 1 ) ) : ''; // We need next page to be loaded.
			$query           = new WP_Query( $params );

			/**
			 * Displays portfolio images content.
			 *
			 * @see wpstarter_portfolio_items - 10, 2
			 */

			do_action( 'wpstarter_portfolio_items', $query, [
				'is_ajax_request' => true,
				'img_class'       => 'portfolio-container__img' . ( strpos( wpstarter_get_options_portfolio( get_the_id() )['layout'], 'grid' ) !== false ? ' portfolio-container__img--covered' : '' )
			] );

			die;

		}

	}

	// Get instance.
	Wpstarter_Portfolio_Load_more::get_instance();

endif;
