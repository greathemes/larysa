<?php
/**
 * Custom hooks for wpstarter-portfolio plugin.
 */

// Front Page.
add_action( 'wpstarter_front_page', 'wpstarter_front_page_portfolio', 15 );

// Portfolio.
add_action( 'wpstarter_portfolio', 'wpstarter_site_content_area_start', 10 );
add_action( 'wpstarter_portfolio', 'wpstarter_site_content_header_area', 15 );
add_action( 'wpstarter_portfolio', 'wpstarter_site_content_primary_area_start', 20 );
add_action( 'wpstarter_portfolio', 'wpstarter_portfolio_content', 30 );
add_action( 'wpstarter_portfolio', 'wpstarter_site_content_primary_area_end', 40 );
add_action( 'wpstarter_portfolio', 'wpstarter_site_content_area_end', 50 );

// Portfolio Content.
add_action( 'wpstarter_portfolio_items', 'wpstarter_portfolio_items', 10, 2 );

// Single Portfolio.
add_action( 'wpstarter_single_portfolio', 'wpstarter_site_content_area_start', 10 );
add_action( 'wpstarter_single_portfolio', 'wpstarter_site_content_primary_area_start', 20, 1 );
add_action( 'wpstarter_single_portfolio', 'wpstarter_site_content_header_area', 20 );
add_action( 'wpstarter_single_portfolio', 'wpstarter_single_portfolio_content', 30 );
add_action( 'wpstarter_single_portfolio', 'wpstarter_single_nav', 50 );
add_action( 'wpstarter_single_portfolio', 'wpstarter_site_content_primary_area_end', 60, 1 );
add_action( 'wpstarter_single_portfolio', 'wpstarter_releated_albums', 70 );
add_action( 'wpstarter_single_portfolio', 'wpstarter_site_content_area_end', 80 );
