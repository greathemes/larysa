<?php
/**
 * Portfolio template & tag functions.
 *
 * @since 1.0.0
 *
 * @package wpstarter
 */

if ( ! function_exists( 'wpstarter_front_page_portfolio' ) ) :

	/**
	 * Displays front page portfolio.
	 */
	function wpstarter_front_page_portfolio() {

		$portfolio = wpstarter_get_options_front_page_portfolio();

		if ( class_exists( 'Kirki' ) && $portfolio['visibility'] ) :

			$heading        = $portfolio['heading_text'];
			$albums         = $portfolio['albums'];
			$btn_visibility = $portfolio['btn_visibility'];
			$btn_text       = $portfolio['btn_text'];
			$btn_page_url   = $portfolio['btn_page_url'];
			$class   = 'front-page-portfolio';

			if ( wpstarter_kirki_repeater_check_required_fields( $albums, [ 'album', 'columns' ] ) ) : ?>

				<section class='<?php echo esc_attr( "$class" ); ?>'>

					<?php if ( $heading ): ?>
						<header class='<?php echo esc_attr( "{$class}__header {$class}__header--wrapper" ); ?>'>
							<h2 class='<?php echo esc_attr( "{$class}__heading" ); ?>'><?php echo esc_html( $heading ); ?></h2>
						</header>
					<?php endif; ?>

					<div class='<?php echo esc_attr( "{$class}__gallery-container" ); ?>'>

						<ul class='<?php echo esc_attr( "{$class}__gallery {$class}__gallery--unstyled" ); ?>'>

							<?php foreach ( $albums as $album ) :

								$id      = $album['album'];
								$columns = $album['columns'];

								if ( $id && $columns ) :

									wpstarter_portfolio_item_content( $id, [
										'class' => $class,
										'modifiers'    => "$columns is-hidden",
										'size'         => $columns === 'col-1' ? 'wpstarter_portfolio' : null,
										'sizes'        => $columns === 'col-1' ? [ 'wpstarter_portfolio', 'wpstarter_portfolio_medium', 'wpstarter_portfolio_large' ] : null,
										'img_class'    => "{$class}__img {$class}__img--covered",
										'lazy'         => true,
									] );

								endif;

							endforeach;

							if ( $btn_visibility && $btn_text && $btn_page_url ) : ?>

								<li class='<?php echo esc_attr( "{$class}__item {$class}__item--last {$class}__item--is-hidden" ); ?>'>
									<a class='<?php echo esc_attr( "{$class}__link {$class}__link--last {$class}__link--button" ); ?>' href='<?php echo esc_url( get_permalink( $btn_page_url ) ); ?>'>
										<?php echo esc_html( $btn_text ); ?>
									</a>
								</li>

							<?php endif;

							wp_reset_postdata(); ?>

						</ul>

					</div>

				</section>

			<?php endif;

		endif;

	}

endif;

if ( ! function_exists( 'wpstarter_portfolio_content' ) ) :

	/**
	 * Displays portfolio content.
	 */
	function wpstarter_portfolio_content() {

		$options             = wpstarter_get_options_portfolio( get_the_id() );
		$filter_all_btn_text = $options['filter_all_btn_text'];
		$images_num          = $options['custom_images_num'] ? $options['images_num'] : get_option( 'posts_per_page' );
		$layout              = $options['layout'];
		$paged               = get_query_var( 'paged' );
		$terms               = get_terms( 'portfolio_categories' );
		$query               = new WP_Query(
			[
				'post_type'      => 'portfolio',
				'posts_per_page' => $images_num,
				'paged'          => $paged ? $paged : 1,
			]
		); ?>

		<div class='portfolio-area'>

			<?php if ( is_array( $terms ) && $terms && $options['filter_visibility'] ) :

				$class = 'portfolio-filter'; ?>

				<div class='<?php echo esc_attr( $class ); ?>'>

					<ul class='<?php echo esc_attr( "{$class}__list {$class}__list--unstyled" ); ?>'> 

						<li class='<?php echo esc_attr( "{$class}__item" ); ?>'>
							<button class='<?php echo esc_attr( "{$class}__button {$class}__button--button-transparent {$class}__button--is-active" ); ?>' type='button' disabled value=''><?php echo esc_html( $filter_all_btn_text ); ?></button>
						</li>

						<?php foreach ( $terms as $term ) : ?>
							<li class='<?php echo esc_attr( "{$class}__item" ); ?>'>
								<button class='<?php echo esc_attr( "{$class}__button {$class}__button--button-transparent {$class}__button--is-not-active" ); ?>' type='button' disabled value='<?php echo esc_attr( $term->slug ); ?>'><?php echo esc_html( $term->name ); ?></button>
							</li>
						<?php endforeach; ?>

					</ul>

				</div>

			<?php endif;

			$class   = 'portfolio-container';
			$more_btn_class = $query->max_num_pages > 1 ? "{$class}--has-more-button" : ''; ?>

			<div class='<?php echo esc_attr( "$class $more_btn_class transition" ); ?>' data-layout='<?php echo esc_attr( $layout ); ?>'>

				<ul class='<?php echo esc_attr( "{$class}__list {$class}__list--unstyled {$class}__list--is-hidden" ); ?>'>

					<?php
					/**
					 * Displays portfolio image.
					 *
					 * @see wpstarter_portfolio_items - 10, 2
					 */

					do_action( 'wpstarter_portfolio_items', $query, [
						'img_class' => "{$class}__img" . ( strpos( $layout, 'grid' ) !== false ? " {$class}__img--covered" : '' )
					] ); ?>

				</ul>

			</div>

			<?php if ( $query->max_num_pages > 1 ) :

				$class = 'portfolio-load-more'; ?>

				<div class='<?php echo esc_attr( $class ) ?>'>
					<button class='<?php echo esc_attr( "{$class}__button" ) ?>' type='button'>
						<span class='<?php echo esc_attr( "{$class}__spinner {$class}__spinner--is-hidden" ) ?>' aria-hidden='true'></span>
						<?php echo esc_html( $options['load_more_btn_text'] ); ?>
					</button>
				</div>

			<?php endif;

			wp_reset_postdata(); ?>

		</div>

	<?php }

endif;

if ( ! function_exists( 'wpstarter_portfolio_items' ) ) :

	/**
	 * Displays portfolio images content.
	 *
	 * @param WP_Query $query The WordPress Query object.
	 */
	function wpstarter_portfolio_items( $query, $args = [] ) {

		if ( $query->have_posts() ) :

			while ( $query->have_posts() ) :

				$query->the_post();

				$id               = get_the_id();
				$img_id           = get_post_thumbnail_id();
				$album_categories = wp_get_post_terms( $id, 'portfolio_categories', [ 'fields' => 'slugs' ] );
				$album_categories = implode( '|', $album_categories );

				if ( $img_id ) :
					$img_src          = wp_get_attachment_image_src( $img_id, 'wpstarter_portfolio_pswp_large' );
					$img_aspect_ratio = $img_src[1] / $img_src[2];
					$has_img          = '';
				else :
					$img_aspect_ratio = 1;
					$has_img          = 'has-no-image';
				endif;

				$img_class       = isset( $args['img_class'] ) ? $args['img_class'] : '';
				$is_ajax_request = isset( $args['is_ajax_request'] ) ? $args['is_ajax_request'] : '';

				wpstarter_portfolio_item_content( $id, [
					'class'         => 'portfolio-container',
					'img_class'            => $img_class,
					'img_aspect_ratio'     => $img_aspect_ratio,
					'modifiers'            => "is-hidden $has_img",
					'item_data_categories' => $album_categories,
					'is_ajax_request'      => $is_ajax_request && class_exists( 'Jetpack' ) && Jetpack::is_module_active( 'lazy-images' ),
				] );

			endwhile;

			wp_reset_postdata();

		endif;

	}

endif;

if ( ! function_exists( 'wpstarter_portfolio_item_content' ) ) :

	/**
	 * Displays portfolio image content.
	 */
	function wpstarter_portfolio_item_content( $id, array $args = [] ) {

		$title      = get_the_title( $id );
		$link_title = sprintf(
			// translators: %s: Title of the album.
			esc_html__( 'See more about %s', 'TRANSLATE' ),
			$title
		);

		$class    = isset( $args['class'] ) ? $args['class'] : 'portfolio';
		$img_class       = isset( $args['img_class'] ) ? $args['img_class'] : '';
		$modifiers       = isset( $args['modifiers'] ) ? $args['modifiers'] : '';
		$modifiers_class = '';

		if ( $modifiers ) :
			$modifiers = explode( ' ', $modifiers );

			foreach ( $modifiers as $modifier ) :
				$modifiers_class .= $modifier ? "{$class}__item--{$modifier} " : '';
			endforeach;
		endif;

		$img_aspect_ratio     = isset( $args['img_aspect_ratio'] ) ? 'data-aspect-ratio=' . $args['img_aspect_ratio'] : '';
		$is_ajax_request      = isset( $args['is_ajax_request'] ) ? $args['is_ajax_request'] : '';
		$item_data_categories = isset( $args['item_data_categories'] ) ? 'data-categories=' . $args['item_data_categories'] : '';
		$size                 = isset( $args['size'] ) ? $args['size'] : 'wpstarter_portfolio_large';
		$sizes                = isset( $args['sizes'] ) ? $args['sizes'] : [ 'wpstarter_portfolio', 'wpstarter_portfolio_medium', 'wpstarter_portfolio_large', 'wpstarter_portfolio_very_large', 'wpstarter_portfolio_pswp' ]; ?>

		<li class='<?php echo esc_attr( "{$class}__item $modifiers_class" ); ?>' <?php echo esc_attr( $item_data_categories ); ?> <?php echo esc_attr( $img_aspect_ratio ); ?>>

			<a class='<?php echo esc_attr( "{$class}__link" ); ?>' href='<?php echo esc_url( get_the_permalink( $id ) ); ?>' title='<?php echo esc_attr( $link_title ); ?>'>

				<?php if ( has_post_thumbnail( $id ) ) :

					$img_args = [
						'size'  => $size,
						'sizes' => $sizes,
						'lazy'  => isset( $args['lazy'] ) ? $args['lazy'] : false,
						'class' => $img_class
					];

					$img_class ? $img_args['class'] = $img_class : '';

					wpstarter_post_thumbnail( get_post_thumbnail_id( $id ), $img_args );

				endif;

				$has_categories = ! has_term( '', 'portfolio_categories', $id ) ? "{$class}__content-container--no-categories" : ''; ?>

				<div class='<?php echo esc_attr( "{$class}__content-container $has_categories" ); ?>'>
					<div class='<?php echo esc_attr( "{$class}__content-inner-container" ); ?>'>
							
						<?php wpstarter_post_categories( $id, [
							'categories_are_links' => false,
							'class'         => $class,
							'category_slug'        => 'portfolio_categories',
							'is_unstyled'          => true,
						] ); ?>

						<?php if ( is_front_page() ) : ?>
							<h3 class='<?php echo esc_attr( "{$class}__title" ); ?>'><?php echo esc_html( $title ); ?></h3>
						<?php else : ?>
							<h4 class='<?php echo esc_attr( "{$class}__title" ); ?>'><?php echo esc_html( $title ); ?></h4>
						<?php endif; ?>

					</div>
				</div>

			</a>

		</li>

	<?php }

endif;

if ( ! function_exists( 'wpstarter_single_portfolio_content' ) ) :

	/**
	 * Displays single portfolio content.
	 */
	function wpstarter_single_portfolio_content() {

		if ( class_exists( 'ACF' ) ) :

			$id      = get_the_id();
			$options = wpstarter_get_options_portfolio( $id );
			$img_ids = $options['single_gallery'];
			$layout  = $options['single_layout'] !== 'default' ? 'data-layout=' . $options['single_layout'] : '';
			$classes = "portfolio-imgs-container single-portfolio-imgs-container";
			$i       = 1;
			$class   = 'single-portfolio-area';

			if ( $img_ids ) : ?>

				<div class='<?php echo esc_attr( $class ); ?>' <?php echo esc_attr( $layout ); ?>>

					<ul class='<?php echo esc_attr( "{$class}__list spotlight-group" . ( $layout ? " {$class}__list--is-hidden" : '' ) ); ?>' data-theme='false'>

						<?php foreach ( explode( ',', $img_ids ) as $img_id ) :

							$img_src          = $img_id ? wp_get_attachment_image_src( $img_id, 'wpstarter_portfolio_pswp_large' ) : '';
							$img_size         = $img_id ? $img_src[1] . 'x' . $img_src[2] : '';
							$img_aspect_ratio = $img_id ? $img_src[1] / $img_src[2] : 1; ?>

							<li data-src='<?php echo esc_url( $img_src[0] ); ?>' class='<?php echo esc_attr( "{$class}__item spotlight" ); ?>' title='<?php esc_attr_e( wp_get_attachment_caption( $img_id ) ); ?>' data-size='<?php echo esc_attr( $img_size ); ?>' data-index='<?php echo esc_attr( $i ); ?>'>
								<?php wpstarter_post_thumbnail( $img_id, [
									'size'  => 'wpstarter_portfolio_medium',
									'sizes' => [ 'wpstarter_portfolio_medium', 'wpstarter_portfolio_large', 'wpstarter_portfolio_very_large', 'wpstarter_portfolio_pswp', 'wpstarter_portfolio_pswp_large' ],
									'class' => "{$class}__img {$class}__img--zoom-animation" . ( strpos( $layout, 'grid' ) !== false ? " {$class}__img--covered" : '' )
								] ); ?>
							</li>

						<?php $i++; endforeach; ?>

					</ul>

				</div>

			<?php endif;

		endif;

	}

endif;

if ( ! function_exists( 'wpstarter_releated_albums' ) ) :

	/**
	 * Displays releated albums.
	 */
	function wpstarter_releated_albums() {

		$portfolio = wpstarter_get_options_portfolio();

		if ( $portfolio['single_releated_albums_visibility'] && is_singular( 'portfolio' ) ) :

			$heading      = $portfolio['single_releated_albums_heading_text'];
			$taxonomy     = 'portfolio_categories';
			$id           = get_the_id();
			$cats         = wp_get_object_terms( $id, $taxonomy );
			$cat_ids      = [];
			$class = 'releated-albums-area';

			foreach ( $cats as $cat ) :
				$cat_ids[] = $cat->term_id;
			endforeach;

			$tax_query_args = sizeof( $cat_ids ) ? [ 'taxonomy' => $taxonomy, 'field' => 'term_id', 'terms' => $cat_ids ] : '';
			$wp_query       = new WP_query(
				[
					'post_type'      => 'portfolio',
					'post__not_in'   => [ $id ],
					'orderby'        => 'rand',
					'posts_per_page' => 4,
					'tax_query'      => [ $tax_query_args ],
				]
			);

			if ( $wp_query->have_posts() ) : ?>

				<section class='<?php echo esc_attr( $class ); ?>'>

					<div class='<?php echo esc_attr( "{$class}__container" ); ?>'>
						
						<?php if ( $heading ) : ?>
							<header class='<?php echo esc_attr( "{$class}__header" ); ?>'>
								<h3 class='<?php echo esc_attr( "{$class}__heading" ); ?>'><?php echo esc_html( $heading ); ?></h3>
							</header>
						<?php endif; ?>

						<ul class='<?php echo esc_attr( "{$class}__list {$class}__list--unstyled" ); ?>'>

							<?php while ( $wp_query->have_posts() ) :

								$wp_query->the_post();

								wpstarter_portfolio_item_content( get_the_id(),
									[
										'class'     => 'releated-albums-area',
										'img_class' => "{$class}__img {$class}__img--covered",
									]
								);

							endwhile; ?>

						</ul>

					</div>

				</section>

			<?php wp_reset_postdata();

		endif;

	endif;

	}

endif;
