<?php
/**
 * Register a portfolio post type.
 *
 * @since 1.0.0
 *
 * @link http://codex.wordpress.org/Function_Reference/register_post_type
 * @package wpstarter
 */

if ( ! class_exists( 'Wpstarter_Portfolio_Cpt' ) ) :

	/**
	 * Wpstarter_Portfolio_Cpt.
	 */
	class Wpstarter_Portfolio_Cpt {

		/**
		 * Instance.
		 *
		 * @access private
		 * @var object Class object.
		 */
		private static $instance;

		/**
		 * Initiator.
		 *
		 * @return object initialized object of class.
		 */
		public static function get_instance() {

			if ( ! isset( self::$instance ) ) :

				self::$instance = new self();

			endif;

			return self::$instance;

		}

		/**
		 * Constructor.
		 */
		public function __construct() {

			add_action( 'init', [ $this, 'custom_post_type' ] );

			register_activation_hook( __FILE__, 'wpstarter_portfolio_flush_rewrites' );
			register_uninstall_hook( __FILE__, 'wpstarter_portfolio_uninstall' );

		}

		/**
		 * Deregister post type.
		 */
		public function wpstarter_portfolio_uninstall() {

			unregister_post_type( 'portfolio' );

		}

		/**
		 * Rewrite rules.
		 */
		public function wpstarter_portfolio_flush_rewrites() {

			$this->custom_post_type();
			flush_rewrite_rules();

		}

		/**
		 * Register a portfolio post type.
		 */
		public function custom_post_type() {

			$labels = [
				'name'               => esc_html_x( 'Portfolio', 'post type general name', 'TRANSLATE' ),
				'singular_name'      => esc_html_x( 'Album', 'post type singular name', 'TRANSLATE' ),
				'menu_name'          => esc_html_x( 'Portfolio', 'admin menu', 'TRANSLATE' ),
				'name_admin_bar'     => esc_html_x( 'Portfolio', 'add new on admin bar', 'TRANSLATE' ),
				'add_new'            => esc_html_x( 'Add New', 'album', 'TRANSLATE' ),
				'add_new_item'       => esc_html__( 'Add New Album', 'TRANSLATE' ),
				'new_item'           => esc_html__( 'New Album', 'TRANSLATE' ),
				'edit_item'          => esc_html__( 'Edit Album', 'TRANSLATE' ),
				'view_item'          => esc_html__( 'View Album', 'TRANSLATE' ),
				'all_items'          => esc_html__( 'All Albums', 'TRANSLATE' ),
				'search_items'       => esc_html__( 'Search Albums', 'TRANSLATE' ),
				'parent_item_colon'  => esc_html__( 'Parent Albums:', 'TRANSLATE' ),
				'not_found'          => esc_html__( 'No Albums Found.', 'TRANSLATE' ),
				'not_found_in_trash' => esc_html__( 'No Albums Found In Trash.', 'TRANSLATE' ),
			];

			$args = [
				'labels'             => $labels,
				'description'        => esc_html__( 'Description', 'TRANSLATE' ),
				'public'             => true,
				'publicly_queryable' => true,
				'show_ui'            => true,
				'show_in_menu'       => true,
				'query_var'          => true,
				'rewrite'            => [ 'slug' => 'portfolio' ],
				'capability_type'    => 'post',
				'has_archive'        => false,
				'menu_icon'          => 'dashicons-images-alt',
				'hierarchical'       => false,
				'menu_position'      => null,
				'supports'           => [ 'title', 'author', 'thumbnail' ],
				'show_in_rest'       => true,
			];

			register_post_type( 'portfolio', $args );

		}

	}

	// Get instance.
	Wpstarter_Portfolio_Cpt::get_instance();

endif;
