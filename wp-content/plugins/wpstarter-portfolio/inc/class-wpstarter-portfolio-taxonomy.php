<?php
/**
 * Register a portfolio taxonomy.
 *
 * @since 1.0.0
 *
 * @package wpstarter
 */

if ( ! class_exists( 'Wpstarter_Portfolio_Taxonomy' ) ) :

	/**
	 * Wpstarter_Portfolio_Taxonomy.
	 */
	class Wpstarter_Portfolio_Taxonomy {

		/**
		 * Instance.
		 *
		 * @access private
		 * @var object Class object.
		 */
		private static $instance;

		/**
		 * Initiator.
		 *
		 * @return object initialized object of class.
		 */
		public static function get_instance() {

			if ( ! isset( self::$instance ) ) :

				self::$instance = new self();

			endif;

			return self::$instance;

		}

		/**
		 * Constructor.
		 */
		public function __construct() {

			add_action( 'init', [ $this, 'taxonomy' ], 0 );

		}

		/**
		 * Adds panels.
		 */
		public function taxonomy() {

			$labels = [
				'name'                       => esc_html_x( 'Album Categories', 'taxonomy general name', 'TRANSLATE' ),
				'singular_name'              => esc_html_x( 'Album Category', 'taxonomy singular name', 'TRANSLATE' ),
				'search_items'               => esc_html__( 'Search Album Categories', 'TRANSLATE' ),
				'popular_items'              => esc_html__( 'Popular Album Categories', 'TRANSLATE' ),
				'all_items'                  => esc_html__( 'All Album Categories', 'TRANSLATE' ),
				'parent_item'                => null,
				'parent_item_colon'          => null,
				'edit_item'                  => esc_html__( 'Edit Album Category', 'TRANSLATE' ),
				'update_item'                => esc_html__( 'Update Album Category', 'TRANSLATE' ),
				'add_new_item'               => esc_html__( 'Add New Album Category', 'TRANSLATE' ),
				'new_item_name'              => esc_html__( 'New Album Category Name', 'TRANSLATE' ),
				'separate_items_with_commas' => esc_html__( 'Separate Album Categories With Commas', 'TRANSLATE' ),
				'add_or_remove_items'        => esc_html__( 'Add Or Remove Album Categories', 'TRANSLATE' ),
				'choose_from_most_used'      => esc_html__( 'Choose From The Most Used Album Categories', 'TRANSLATE' ),
				'not_found'                  => esc_html__( 'No Album Categories Found.', 'TRANSLATE' ),
				'menu_name'                  => esc_html__( 'Album Categories', 'TRANSLATE' ),
			];

			$args = [
				'labels'                => $labels,
				'hierarchical'          => false,
				'show_ui'               => true,
				'show_admin_column'     => true,
				'update_count_callback' => '_update_post_term_count',
				'query_var'             => true,
				'show_in_rest'          => true,
			];

			register_taxonomy( 'portfolio_categories', 'portfolio', $args );

		}

	}

	// Get instance.
	Wpstarter_Portfolio_Taxonomy::get_instance();

endif;
