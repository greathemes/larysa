/**
 * Portfolio for Grid layout.
 *
 * @since 1.0.0
 *
 * @package wpstarter
 */

jQuery( document ).ready( function( $ ) {

	'use strict';

	// Classes and modifiers.
	const portfolioContainerClass = 'portfolio-container',
	listClass                     = 'portfolio-container__list',
	itemClass                     = 'portfolio-container__item',
	loadMoreButtonClass           = 'portfolio-load-more__button',
	loadMoreSpinnerClass          = 'portfolio-load-more__spinner',
	filterButtonClass             = 'portfolio-filter__button',
	jetpackLazyImgClass           = 'jetpack-lazy-image',
	jetpackLazyImgHiddenClass     = 'jetpack-lazy-image-hidden',
	masonryInitializedClass       = 'masonry-initialized',
	hiddenModifier                = '--is-hidden',
	visibleModifier               = '--is-visible',
	activeModifier                = '--is-active',
	notActiveModifier             = '--is-not-active',
	moreButtonModifier            = '--has-more-button',
	loadingModifier               = '--is-loading',
	noImageModifier               = '--has-no-image';

	const $portfolio = $( '.' + portfolioContainerClass );

	if ( ! $portfolio.length ) {
		return;
	}

	const $list                   = $( '.' + listClass ),
	$itemsArr                     = $list.find( '.' + itemClass ).toArray(),
	$filterButtons                = $( '.' + filterButtonClass ),
	$filterButtonsActive          = $( '.' + filterButtonClass + activeModifier  ),
	$filterButtonsNotActiveArr    = $( '.' + filterButtonClass + notActiveModifier ).toArray(),
	$loadMoreButton               = $( '.' + loadMoreButtonClass ),
	$loadMoreSpinner              = $( '.' + loadMoreSpinnerClass ),
	$body                         = $( document.body ),
	root                          = document.documentElement,
	transitionDurationCssVar      = getComputedStyle( root ).getPropertyValue( '--transition-duration' ),
	timeoutTime                   = parseFloat( transitionDurationCssVar ) * 1000,
	isLazy                        = $( 'html' ).hasClass( 'jetpack-lazy-images-js-enabled' ),
	isGrid                        = $portfolio.data( 'layout' ).indexOf( 'grid' ) !== -1,
	isMasonry                     = $portfolio.data( 'layout' ).indexOf( 'masonry' ) !== -1;

	let category        = '',
	firstInitialization = true;

	/**
	 * Masonry reloading.
	 */
	function reloadMasonry( complete = true, timeout = true, selector = false ) {

		setTimeout( function() {

			! isLazy ? setHeight() : '';

			if ( complete ) {
				$list.masonry( 'reloadItems' ).one( 'layoutComplete', masonryComplete ).masonry( 'layout' );
			} else {
				if ( selector ) {
					$list.masonry( {
						itemSelector: '.' + itemClass + visibleModifier,
						columnWidth: '.' + itemClass + visibleModifier,
					} ).masonry( 'reloadItems' ).masonry( 'layout' );
				} else {
					$list.masonry( 'reloadItems' ).masonry( 'layout' );
				}
			}

		}, ( timeout ? timeoutTime : 0 ) );

	}

	/**
	 * Filter buttons disable toggling.
	 */
	function filterButtonsDisableToggling( status ) {

		$filterButtons.each( function( i, el ) {
			$( el ).prop( 'disabled', status );
		} );

	}

	/**
	 * Shows images.
	 */
	function showImgs() {

		const $hidden_imgs = category ? $( '.' + itemClass + hiddenModifier ) : $( '.' + itemClass );
		// const $hidden_imgs = $( '.' + itemClass );

		reloadMasonry( false, false, true );
		filterButtonsDisableToggling( false );

		if ( category ) {

			$hidden_imgs.each( function( i, el ) {

				const $el      = $( el ),
				itemCategories = $el.data( 'categories' ).split( '|' );

				if ( itemCategories.indexOf( category ) > -1 ) {
					$el.addClass( itemClass + visibleModifier ).removeClass( itemClass + hiddenModifier );
				}

			} );

		} else {

			$hidden_imgs.each( function( i, el ) {
				$( el ).addClass( itemClass + visibleModifier ).removeClass( itemClass + hiddenModifier );
			} );

		}

		$body.trigger( 'post-load' );

	}

	/**
	 * Reorganizes images.
	 */
	function reorganizeImgs() {

		if ( category ) {

			$.each( $itemsArr, function( i, el ) {

				const $el      = $( el ),
				itemCategories = $el.data( 'categories' ).split( '|' );

				if ( itemCategories.indexOf( category ) === -1 ) {
					$el.addClass( itemClass + hiddenModifier ).removeClass( itemClass + visibleModifier );
				} else if ( isLazy ) {
					$el.find( 'img' ).addClass( jetpackLazyImgClass );
				}

			} );

		} else if ( isLazy ) {

			$.each( $itemsArr, function( i, el ) {
				$( el ).find( 'img' ).addClass( jetpackLazyImgClass );
			} );

		}

		showImgs();

	}

	/**
	 * Filter.
	 */
	function filter() {

		const $this = $( this );

		if ( ! $this.hasClass( filterButtonClass + activeModifier ) ) {

			category = $this.val();

			filterButtonsDisableToggling( true );

			if ( $this.hasClass( filterButtonClass ) ) {
				$filterButtons.removeClass( filterButtonClass + activeModifier );
				$this.addClass( filterButtonClass + activeModifier );
			}

			reorganizeImgs();

		}

	}

	/**
	 * Filter buttons activation toggling.
	 */
	function filterButtonsState( data, no_null ) {

		const btnsToActivationArr = [];

		$( data ).each( function( i, el ) {

			const $el = $( el );

			if ( $el.hasClass( itemClass ) ) {

				const itemCategories = $el.data( 'categories' ).split( '|' );

				if ( no_null ) {

					$.each( $filterButtonsNotActiveArr, function( i, button ) {

						if ( button !== null && itemCategories.indexOf( button.value ) !== -1 ) {
							$( button ).on( 'click', filter ).removeClass( filterButtonClass + notActiveModifier );
							$filterButtonsNotActiveArr.splice( $filterButtonsNotActiveArr.indexOf( button ), 1, null );
						}

					} );

				}

			}

		} );

	}

	/**
	 * Load more before send.
	 */
	function loadMoreBeforeSend() {

		$loadMoreSpinner.addClass( loadMoreSpinnerClass + visibleModifier ).removeClass( loadMoreSpinnerClass + hiddenModifier );
		$loadMoreButton.addClass( loadMoreButtonClass + loadingModifier );

	}

	/**
	 * Load more success.
	 */
	function loadMoreSuccess( data ) {

		if ( data ) {

			setTimeout( function() {
				$loadMoreSpinner.removeClass( loadMoreSpinnerClass + visibleModifier ).addClass( loadMoreSpinnerClass + hiddenModifier );
				$loadMoreButton.removeClass( loadMoreButtonClass + loadingModifier );
			}, timeoutTime );

			wpstarter_portfolio_params.current_page++;

		}

		loadMoreButtonState();

	}

	/**
	 * Load more complete.
	 */
	function loadMoreComplete( jqXHR ) {

		const $data = $( jqXHR.responseText ),
		no_null     = $filterButtonsNotActiveArr.some( function ( el ) { return el !== null; } ); // Check if array contains at least one value different than null.

		filterButtonsState( $data, no_null );

		$list.append( $data ).imagesLoaded( function() {

			$data.each( function( i, el ) {

				const $el = $( el );

				if ( $el.is( 'li' ) ) {

					const $img = $el.find( 'img' );

					$itemsArr.push( el );

					if ( isLazy && ! $el.hasClass( itemClass + noImageModifier ) ) {

						if ( ! category || $el.hasClass( category ) ) {
							$img.addClass( jetpackLazyImgClass );
						}

						$el.addClass( jetpackLazyImgHiddenClass );
					}

				}

			} );

			setHeight();

			$list.masonry( 'appended', $data );

			if ( isLazy ) {

				$.each( $itemsArr, function( i, el ) {

					const $el = $( el );
					$el.hasClass( itemClass + noImageModifier ) ? $el.addClass( itemClass + visibleModifier ).removeClass( itemClass + hiddenModifier ) : '';

				} );

			}

			$body.trigger( 'post-load' );

			reorganizeImgs();

		} );

	}

	/**
	 * Load more.
	 */
	function loadMore() {

		$.ajax(
			{
				url : wpstarter_portfolio_params.ajax_url,
				type: 'POST',
				data: {
					'action': 'load_more',
					'query' : wpstarter_portfolio_params.posts,
					'page'  : wpstarter_portfolio_params.current_page,
					'nonce' : wpstarter_portfolio_params.nonce,
				},
				beforeSend: loadMoreBeforeSend,
				success   : loadMoreSuccess,
				complete  : loadMoreComplete
			}
		);

		return false;

	}

	/**
	 * Load more button status toggling.
	 */
	function loadMoreButtonState() {

		if ( wpstarter_portfolio_params.current_page < wpstarter_portfolio_params.max_page ) {

			// Show.
			setTimeout( function() {
				$loadMoreButton.removeClass( loadMoreButtonClass + hiddenModifier );
				$portfolio.addClass( portfolioContainerClass + moreButtonModifier );
			}, timeoutTime );

		} else {

			// Hide.
			setTimeout( function() {
				$loadMoreButton.addClass( loadMoreButtonClass + hiddenModifier ).removeClass( loadMoreButtonClass + loadingModifier ).parent().addClass( hiddenModifier );
			}, timeoutTime );

			$portfolio.removeClass( portfolioContainerClass + moreButtonModifier );

		}

	};

	/**
	 * Sets the images height the same as the width if grid layout is choosen.
	 */
	function setHeight( timeout ) {

		$.each( $itemsArr, function( i, el ) {
			el.style.height = isGrid ? el.clientWidth + 'px' : ( el.clientWidth / el.dataset.aspectRatio ) + 'px';
		} );

	}

	/**
	 * Masonry complete.
	 */
	function masonryComplete() {

		$list.removeClass( listClass + hiddenModifier );
		root.style.setProperty( '--transition-duration', transitionDurationCssVar );

		! isLazy && firstInitialization ? showImgs() : '';

		if ( isMasonry ) {

			if ( ! isLazy ) {
				setTimeout( function() {
					$.each( $itemsArr, function( i, el ) {
						if ( ! el.classList.contains( itemClass + noImageModifier ) ) {
							el.style.height = '';
						}
					} );
				}, timeoutTime );
			}

		}

	}

	/**
	 * Fix the masonry layout by previous adding height to the elements before lazy loading.
	 */
	function lazyLoadingInitMasonry() {

		setHeight();

		$.each( $itemsArr, function( i, el ) {

			const $el = $( el );

			if ( $el.hasClass( itemClass + noImageModifier ) ) {
				$el.addClass( itemClass + visibleModifier ).removeClass( itemClass + hiddenModifier ).removeClass( itemClass + noImageModifier );
			}

		} );

		$list.hasClass( masonryInitializedClass ) ? reloadMasonry() : initMasonry();

	}

	/**
	 * Orientation change Masonry.
	 */
	function windowChangeMasonry() {

		$list.addClass( listClass + hiddenModifier );
		root.style.setProperty( '--transition-duration', '0s' );

		isLazy ? setTimeout( lazyLoadingInitMasonry, timeoutTime ) : reloadMasonry();

	}

	/**
	 * Masonry initializer.
	 */
	function initMasonry() {

		$list.addClass( masonryInitializedClass );

		setHeight();

		const args = {
			itemSelector      : firstInitialization && isLazy ? '.' + itemClass : '.portfolio-container__item--is-hidden',
			columnWidth       : firstInitialization && isLazy ? '.' + itemClass : '.portfolio-container__item--is-hidden',
			percentPosition   : true,
			isInitLayout      : false,
			isResizable       : true,
			transitionDuration: 0,
			isAnimated        : false,
		}

		$list.masonry( args ).one( 'layoutComplete', masonryComplete ).masonry( 'layout' );

		setTimeout( filterButtonsDisableToggling, timeoutTime, false );
		firstInitialization = false;

	}

	/**
	 * Jetpack lazy images module.
	 */
	function jetpackLazyImages( e ) {

		const $el = $( e.target );

		$el.imagesLoaded( function() {
			$el.closest( '.' + itemClass ).addClass( itemClass + visibleModifier ).removeClass( itemClass + hiddenModifier ).removeClass( jetpackLazyImgHiddenClass );
		} );

	}

	/**
	 * Initializer.
	 */
	function init() {

		window.addEventListener( 'orientationchange', windowChangeMasonry, false );
		window.addEventListener( 'resize', windowChangeMasonry, false );

		if ( isLazy ) {
			lazyLoadingInitMasonry();
			$body.on( 'jetpack-lazy-loaded-image', jetpackLazyImages );
		} else {
			$list.imagesLoaded( initMasonry() );
		}

		loadMoreButtonState();
		filterButtonsState( $itemsArr, true );

		$loadMoreButton.on( 'click', loadMore );

		$filterButtonsActive.each( function( i, el ) {
			$( el ).on( 'click', filter );
		} );

	}

	init();

} );
