/**
 * Single portfolio for Masonry layout.
 *
 * @since 1.0.0
 *
 * @package wpstarter
 */

jQuery( document ).ready( function( $ ) {

	'use strict';

	// Classes and modifiers.
	const portfolioContainerClass = 'single-portfolio-area',
	listClass                     = 'single-portfolio-area__list',
	itemClass                     = 'single-portfolio-area__item',
	masonryInitializedClassName   = 'masonry-initialized',
	hiddenModifier                = '--is-hidden',
	visibleModifier               = '--is-visible',
	activeModifier                = '--is-active',
	notActiveModifier             = '--is-not-active',
	moreButtonModifier            = '--has-more-button',
	loadingModifier               = '--is-loading',
	noImageModifier               = '--has-no-image';

	const $portfolio = $( '.' + portfolioContainerClass ),
	layout           = $portfolio.data( 'layout' );

	if ( ! $portfolio.length || ! layout ) {
		return;
	}

	const $list              = $( '.' + listClass ),
	$itemsArr                = $list.find( '.' + itemClass ).toArray(),
	$body                    = $( document.body ),
	root                     = document.documentElement,
	transitionDurationCssVar = getComputedStyle( root ).getPropertyValue( '--transition-duration' ),
	timeoutTime              = parseFloat( transitionDurationCssVar ) * 1000,
	isLazy                   = $( 'html' ).hasClass( 'jetpack-lazy-images-js-enabled' ),
	isGrid                   = layout.indexOf( 'grid' ) !== -1,
	isMasonry                = layout.indexOf( 'masonry' ) !== -1;

	let firstInitialization = true;

		/**
		 * Masonry reloading.
		 */
		function reloadMasonry() {

			setTimeout( function() {
				$list.masonry( 'reloadItems' ).one( 'layoutComplete', masonryComplete ).masonry( 'layout' );
			}, timeoutTime );

		}

		/**
		 * Sets the items height when there is no image.
		 */
		function setHeight() {

			$.each( $itemsArr, function( i, el ) {
				el.style.height = isGrid ? el.clientWidth + 'px' : ( el.clientWidth / el.firstElementChild.getAttribute( 'aspect-ratio' ) ) + 'px';
			} );

		}

		/**
		 * Shows images if no lazy loading.
		 */
		function showImgs() {

			$.each( $itemsArr, function( i, el ) {
				$( el ).addClass( itemClass + visibleModifier );
			} );

			firstInitialization = false;

		}

		/**
		 * Fix the masonry layout by previous adding height to the elements before lazy loading.
		 */
		function lazyLoadingInitMasonry() {

			setHeight();

			if ( $list.hasClass( masonryInitializedClassName ) ) {
				reloadMasonry();
			} else {
				initMasonry();
			}

		}

		/**
		 * Window change Masonry.
		 */
		function windowChangeMasonry() {

			$list.addClass( listClass + hiddenModifier );
			root.style.setProperty( '--transition-duration', '0s' );

			isLazy ? setTimeout( lazyLoadingInitMasonry, timeoutTime ) : reloadMasonry();

		}

		/**
		 * Masonry complete.
		 */
		function masonryComplete() {

			if ( ! isLazy && isMasonry ) {
				$.each( $itemsArr, function( i, el ) {
					el.style.height = '';
				} );	
			}

			$list.removeClass( listClass + hiddenModifier );
			root.style.setProperty( '--transition-duration', transitionDurationCssVar );

			! isLazy && firstInitialization ? showImgs() : '';

		}

		/**
		 * Masonry initializer.
		 */
		function initMasonry() {

			$list.addClass( masonryInitializedClassName );

			setHeight();

			const args = {
				itemSelector      : '.' + itemClass,
				columnWidth       : '.' + itemClass,
				percentPosition   : true,
				isInitLayout      : false,
				isResizable       : true,
				transitionDuration: 0,
				isAnimated        : false,
			}

			$list.masonry( args ).one( 'layoutComplete', masonryComplete ).masonry( 'layout' );	

		}

		/**
		 * Jetpack lazy images module.
		 */
		function jetpackLazyImages( e ) {

			const $el = $( e.target );

			$el.imagesLoaded( function() {
				$el.closest( '.' + itemClass ).addClass( itemClass + visibleModifier ).removeClass( itemClass + hiddenModifier );
			} );

		}

		/**
		 * Initializer.
		 */
		function init() {

			if ( isLazy ) {
				lazyLoadingInitMasonry();
				$body.on( 'jetpack-lazy-loaded-image', jetpackLazyImages );
			} else {
				$list.imagesLoaded( initMasonry() );
			}

			window.addEventListener( 'orientationchange', windowChangeMasonry, false );
			window.addEventListener( 'resize', windowChangeMasonry, false );

		}

		init();

} );
