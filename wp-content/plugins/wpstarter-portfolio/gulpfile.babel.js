/**
 * Gulp file.
 */

import gulp from 'gulp';
import yargs from 'yargs';
import gulpif from 'gulp-if';
import sourcemaps from 'gulp-sourcemaps';
import webpack from 'webpack-stream';
import del from 'del';
import uglify from 'gulp-uglify-es';
import concat from 'gulp-concat';
import rename from 'gulp-rename';
import named from 'vinyl-named';
import browsersync from 'browser-sync';
import zip from 'gulp-zip';
import replace from 'gulp-replace';
import wpPot from 'gulp-wp-pot';
import info from './package.json';

const dev_name        = 'wpstarter',
capitalize_dev_name   = 'Wpstarter',
uppercase_dev_name    = 'WPSTARTER',
translate_string      = 'TRANSLATE',
theme_name            = info.name,
plugin_name           = theme_name + '-' + __dirname.split( dev_name + '-' ).pop(),
plugin_dir            = '../' + plugin_name,
capitalize_theme_name = theme_name.charAt( 0 ).toUpperCase() + theme_name.slice( 1 ),
uppercase_theme_name  = theme_name.toUpperCase(),
assets                = 'assets/',
js                    = 'assets/scripts/js/',
admin_js              = 'assets/scripts/admin-js/',
php                   = '**/*.php',
server                = browsersync.create(),
PRODUCTION            = yargs.argv.prod;

// Server.
export const serve = ( done ) => {

	server.init( { proxy: 'http://localhost:8888/' + theme_name } );
	done();

}

// Reload.
export const reload = ( done ) => {

	server.reload();
	done();

}

// Create .pot file.
export const pot = () => {

	return gulp.src( plugin_dir + '/' + php )
	.pipe(
		wpPot(
			{
				domain: plugin_name,
				package: plugin_name
			}
		)
	)
	.pipe( gulp.dest( 'languages/' + plugin_name + '.pot' ) )
	.pipe( gulp.dest( plugin_dir + '/languages/' + plugin_name + '.pot' ) )

}

// Remove production directory & .zip file.
export const clean = () => del( [ plugin_dir, plugin_dir + '.zip' ], { force: true } );

// Translation.
export const translate = ( to_translate ) => {

	return to_translate
	.pipe( replace( dev_name, theme_name ) )
	.pipe( replace( capitalize_dev_name, capitalize_theme_name ) )
	.pipe( replace( uppercase_dev_name, uppercase_theme_name ) )
	.pipe( replace( translate_string, plugin_name ) );

}

// Scripts.
export const scripts = ( done ) => {

	return translate( gulp.src( 'scripts/front/**/*.js' ) )
		.pipe( named() )
		.pipe( concat( 'scripts.js' ) )
		.pipe( gulpif( PRODUCTION, uglify() ) )
		.pipe( rename( { suffix: '.min' } ) )
		.pipe( gulp.dest( plugin_dir + '/assets/js' ) )
		.pipe( server.stream() )

	done();

}

// Views (php).
export const views = ( done ) => {

	return translate( gulp.src( php ) )
	.pipe(
		rename( ( opt ) => {
			opt.basename = opt.basename.replace( dev_name, theme_name );
			opt.basename = opt.basename.replace( capitalize_dev_name, capitalize_theme_name );
			return opt;
		} )
	)
	.pipe( gulp.dest( plugin_dir ) );

}

// Watch.
export const watch = () => {

	gulp.watch( 'scripts/front/**/*.js', scripts );
	gulp.watch( '**/*.php', gulp.series( views, reload ) );

}

// Compress to .zip file.
export const compress = () => {

	return gulp.src( plugin_dir + '/**/*' )
		.pipe( zip( plugin_name + '.zip' ) )
		.pipe( gulp.dest( '../' ) )

}

// Tasks.
export const dev        = gulp.series( clean, gulp.parallel( scripts, views ), serve, watch );
export const build      = gulp.series( clean, gulp.parallel(  scripts, views ), pot );
export const production = gulp.series( build, compress );

export default dev;
