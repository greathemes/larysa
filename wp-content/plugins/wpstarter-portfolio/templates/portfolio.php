<?php
/**
 * Template Name: Portfolio
 *
 * @since 1.0.0
 *
 * @package wpstarter
 */

get_header();

/**
 * Displays portfolio.
 *
 * @see wpstarter_site_content_area_start         - 10
 * @see wpstarter_site_content_primary_area_start - 20
 * @see wpstarter_portfolio_content               - 30
 * @see wpstarter_site_content_primary_area_end   - 40
 * @see wpstarter_site_content_area_end           - 50
 */

do_action( 'wpstarter_portfolio' );

get_footer();
