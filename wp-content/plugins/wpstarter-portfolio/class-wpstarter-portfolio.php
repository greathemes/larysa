<?php
/**
 * Portfolio initializer.
 *
 * @since 1.0.0
 *
 * @package wpstarter
 */

if ( ! class_exists( 'Wpstarter_Portfolio' ) ) :

	/**
	 * Wpstarter_Portfolio.
	 */
	class Wpstarter_Portfolio {

		/**
		 * Instance.
		 *
		 * @access private
		 * @var object Class object.
		 */
		private static $instance;

		/**
		 * Initiator.
		 *
		 * @return object initialized object of class.
		 */
		public static function get_instance() {

			if ( ! isset( self::$instance ) ) :

				self::$instance = new self();

			endif;

			return self::$instance;

		}

		/**
		 * Constructor.
		 */
		public function __construct() {

			add_action( 'wp_enqueue_scripts', [ $this, 'enqueue_scripts' ] );
			add_filter( 'theme_page_templates', [ $this, 'add_page_template' ] );
			add_filter( 'page_template', [ $this, 'redirect_page_template' ] );
			add_filter( 'single_template', [ $this, 'single_portfolio_template' ] );
			add_action( 'plugins_loaded', [ $this, 'load_plugin_lang' ] );

		}

		/**
		 * Enqueue scripts.
		 */
		public function enqueue_scripts() {

			if ( is_page_template( 'portfolio.php' ) ) :

				$portfolio = wpstarter_get_options_portfolio();
				$paged     = get_query_var( 'paged' );
				$query     = new WP_Query(
					[
						'post_type'      => 'portfolio',
						'posts_per_page' => $portfolio['custom_images_num'] ? $portfolio['images_num'] : get_option( 'posts_per_page' ),
						'paged'          => $paged ? $paged : 1
					]
				);

				wp_register_script( 'wpstarter_portfolio', '', [], '1.0.0', true );

				wp_localize_script(
					'wpstarter_portfolio',
					'wpstarter_portfolio_params',
					[
						'ajax_url'     => admin_url( 'admin-ajax.php' ),
						'nonce'        => wp_create_nonce( 'ajax-nonce' ),
						'posts'        => wp_json_encode( $query->query_vars ),
						'current_page' => $paged ? $paged : 1,
						'max_page'     => $query->max_num_pages,
					]
				);

				wp_enqueue_script( 'wpstarter_portfolio' );

			endif;

			if ( is_page_template( 'portfolio.php' ) || is_singular( 'portfolio' ) ) :

				wp_enqueue_script( 'masonry', '', [], '3.0.0', true );
				wp_enqueue_script( 'wpstarter-scripts-portfolio-min', WPSTARTER_PORTFOLIO_URL . 'assets/js/scripts.min.js', [], '1.0.0', true );

			endif;

		}

		/**
		 * Adds portfolio template.
		 *
		 * @param array $templates Page templates.
		 * @return array Modified page templates array.
		 */
		public function add_page_template( $templates ) {

			$templates['portfolio.php'] = 'Portfolio';

			return $templates;

		}

		/**
		 * Redirects to the portfolio template.
		 *
		 * @param string $template Page template path.
		 * @return string Absolute portfolio template path.
		 */
		public function redirect_page_template( $template ) {

			$post          = get_post();
			$page_template = get_post_meta( $post->ID, '_wp_page_template', true );

			if ( 'portfolio.php' === basename( $page_template ) ) :

				$template = WPSTARTER_PORTFOLIO_TEMPLATES . 'portfolio.php';

			endif;

			return $template;

		}

		/**
		 * Redirects to the portfolio template.
		 *
		 * @param string $single Single template path.
		 * @return string Absolute single portfolio template path.
		 */
		public function single_portfolio_template( $single ) {

			global $post;

			// Checks for single template by post type.
			if ( 'portfolio' === $post->post_type && ! is_page_template() ) :

				if ( file_exists( WPSTARTER_PORTFOLIO_TEMPLATE_PARTS . 'single-portfolio.php' ) ) :

					return WPSTARTER_PORTFOLIO_TEMPLATE_PARTS . 'single-portfolio.php';

				endif;

			endif;

			return $single;

		}

		/**
		 * Rewrite rules.
		 */
		public function rewrite() {

			flush_rewrite_rules( true );

		}

		/**
		 * Load text domain.
		 */
		public function load_plugin_lang() {

			load_plugin_textdomain( 'TRANSLATE', FALSE, basename( dirname( __FILE__ ) ) . '/languages/' );

		}

	}

	// Get instance.
	Wpstarter_Portfolio::get_instance();

endif;
