<?php
/**
 * Plugin uninstaller.
 *
 * @since 1.0.0
 *
 * @package wpstarter
 */

if ( ! defined( 'WP_UNINSTALL_PLUGIN' ) ) : die; endif;

// Removes Front Page Portfolio theme mods.
$theme_mods = wpstarter_get_options_front_page_portfolio();

foreach ( $theme_mods as $k => $v ) :
	remove_theme_mod( 'wpstarter_front_page_portfolio_' . $k );
endforeach;

// Removes Portfolio theme mods.
$theme_mods = wpstarter_get_options_portfolio();

foreach ( $theme_mods as $k => $v ) :
	remove_theme_mod( 'wpstarter_portfolio_' . $k );
endforeach;

// Removes 'portfolio' Posts Type.
$query = new WP_Query ( [ 'post_type' => 'portfolio', 'posts_per_page' => -1 ] );

if ( $query->have_posts() ) :

	while ( $query->have_posts() ) :

		$query->the_post();

		$id = get_the_ID();

		wp_delete_post( $id, true );

	endwhile;

	wp_reset_postdata();

endif;

unregister_post_type( 'portfolio' );

// Removes 'portfolio_categories' terms.
function delete_taxonomy_terms( $taxonomy ) {

	global $wpdb;

	$query = 'SELECT t.name, t.term_id FROM ' . $wpdb->terms . ' AS t INNER JOIN ' . $wpdb->term_taxonomy . ' AS tt ON t.term_id = tt.term_id WHERE tt.taxonomy = "' . $taxonomy . '"';

	$terms = $wpdb->get_results( $query );

	foreach ( $terms as $term ) :
		wp_delete_term( $term->term_id, $taxonomy );
	endforeach;

}

// Delete all custom terms for this taxonomy.
delete_taxonomy_terms( 'portfolio_categories' );
unregister_taxonomy( 'portfolio_categories' );
