<?php
/**
 * Widgets initializer.
 *
 * @since 1.0.0
 *
 * @package wpstarter
 */

if ( ! class_exists( 'Wpstarter_Widgets_Init' ) ) :

	/**
	 * Wpstarter_Widgets_Init.
	 */
	class Wpstarter_Widgets_Init {

		/**
		 * Instance.
		 *
		 * @access private
		 * @var object Class object.
		 */
		private static $instance;

		/**
		 * Initiator.
		 *
		 * @return object initialized object of class.
		 */
		public static function get_instance() {

			if ( ! isset( self::$instance ) ) :

				self::$instance = new self();

			endif;

			return self::$instance;

		}

		/**
		 * Constructor.
		 */
		public function __construct() {

			add_action( 'plugins_loaded', [ $this, 'load_plugin_lang' ] );
			add_action( 'activated_plugin', 'flush_rewrite_rules'  );
			add_action( 'deactivated_plugin', 'flush_rewrite_rules' );

		}

		/**
		 * Load text domain.
		 */
		public function load_plugin_lang() {

			load_plugin_textdomain( 'TRANSLATE', FALSE, basename( dirname( __FILE__ ) ) . '/languages/' );

		}

	}

	// Get instance.
	Wpstarter_Widgets_Init::get_instance();

endif;
