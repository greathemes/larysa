<?php
/**
 * Plugin Name: Wpstarter Widgets
 * Plugin URI: https://wpstarter.greathemes.com/
 * Description: Custom widgets for Wpstarter theme.
 * Version: 1.0.0
 * Author: Greathemes
 * Author URI: https://greathemes.com/
 * License: GNU General Public License v2.
 * License URI: http://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain: wpstarter-widgets
 * Domain Path: /languages
 *
 * @package wpstarter
 * @author Greathemes
 * @license GPL-2.0+
 */

if ( ! defined( 'ABSPATH' ) ) : exit; endif;

// Constants.
define( 'WPSTARTER_WIDGETS_DIR', trailingslashit( plugin_dir_path( __FILE__ ) ) );
define( 'WPSTARTER_WIDGETS_INCLUDES', trailingslashit( plugin_dir_path( __FILE__ ) . '/inc' ) );
define( 'WPSTARTER_WIDGETS_URL', trailingslashit( plugins_url() . '/wpstarter-widgets' ) );

// Init.
require_once WPSTARTER_WIDGETS_DIR . 'class-wpstarter-widgets-init.php';

// Includes.
require_once WPSTARTER_WIDGETS_INCLUDES . 'custom-functions.php';
require_once WPSTARTER_WIDGETS_INCLUDES . 'class-wpstarter-widgets-latest-posts.php';
require_once WPSTARTER_WIDGETS_INCLUDES . 'class-wpstarter-widgets-popular-posts.php';
require_once WPSTARTER_WIDGETS_INCLUDES . 'class-wpstarter-widgets-instagram.php';

if ( class_exists( 'Wpstarter_Portfolio_Init' ) ) :
	require_once WPSTARTER_WIDGETS_INCLUDES . 'class-wpstarter-widgets-latest-albums.php';
endif;
