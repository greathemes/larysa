<?php
/**
 * Register a widget 'Most Popular Posts'.
 *
 * @since 1.0.0
 *
 * @link http://codex.wordpress.org/Function_Reference/register_post_type
 * @package wpstarter
 */

if ( ! class_exists( 'Wpstarter_Widgets_Popular_Posts' ) && class_exists( 'WP_Widget' ) ) :

	/**
	 * Wpstarter_Widgets_Popular_Posts.
	 */
	class Wpstarter_Widgets_Popular_Posts extends WP_Widget {

		/**
		 * Instance.
		 *
		 * @access private
		 * @var object Class object.
		 */
		private static $instance;

		/**
		 * Initiator.
		 *
		 * @return object initialized object of class.
		 */
		public static function get_instance() {

			if ( ! isset( self::$instance ) ) :

				self::$instance = new self();

			endif;

			return self::$instance;

		}

		/**
		 * Constructor.
		 */
		public function __construct() {

			$args = [
				'classname'   => 'widget-popular-posts',
				'description' => esc_html__( 'Your site’s popular Posts.', 'TRANSLATE' ),
			];

			parent::__construct( 'wpstarter_popular_posts', esc_html__( 'Popular Posts (Wpstarter)', 'TRANSLATE' ), $args );

			add_action( 'widgets_init', [ $this, 'register' ] );

		}

		/**
		 * Register widget.
		 */
		public function register() {

			register_widget( 'Wpstarter_Widgets_Popular_Posts' );

		}

		/**
		 * Outputs the content of the widget.
		 *
		 * @param array $args
		 * @param array $instance
		 */
		public function widget( $args, $instance ) {

			extract( $args );

			// Check the widget options.
			$title            = isset( $instance['title'] ) && ! empty( $instance['title'] ) ? apply_filters( 'widget_title', $instance['title'] ) : '';
			$posts_num        = isset( $instance['posts_num'] ) && ! empty( $instance['posts_num'] ) ? $instance['posts_num'] : 5;
			$sort_by          = isset( $instance['sort_by'] ) && ! empty( $instance['sort_by'] ) ? $instance['sort_by'] : '';
			$time_range       = isset( $instance['time_range'] ) && ! empty( $instance['time_range'] ) ? $instance['time_range'] : '';

			echo $args['before_widget'];

			echo ( ! empty( $title ) ) ? $args['before_title'] . $title . $args['after_title'] : ''; ?>

			<div class='widget-popular-posts'>

				<?php global $post;

				$sort_by_views = $sort_by === 'views_count' ? true : false;
				$i             = 1;
				$posts         = get_posts(
					[
						'post_type'      => 'post',
						'posts_per_page' => $posts_num,
						'orderby'        => $sort_by_views ? 'meta_value_num' : 'comment_count',
						'order'          => 'DESC',
						'meta_key'       => $sort_by_views ? 'post_views_count' : '',
						'date_query'     => [ [ 'after' => str_replace( '_', ' ', $time_range ) ] ],
					]
				);

				if ( $posts ) : ?>

					<ul class='widget-popular-posts__list widget-popular-posts__list--unstyled'>

						<?php foreach ( $posts as $post ) :

							setup_postdata( $post );

							$id = $post->ID; ?>

							<li class='widget-popular-posts__item'>
								<a class='widget-popular-posts__link' href='<?php echo esc_url( get_the_permalink( $id ) ); ?>'>

									<span class='widget-popular-posts__num'><?php echo esc_html( $i < 10 ? 0 . $i : $i ); ?></span>
									<div class='widget-popular-posts__content-container'>
										<?php if ( $post->post_title ) : ?>
											<h4 class='widget-popular-posts__title'><?php echo esc_html( $post->post_title ); ?></h4>
										<?php endif; ?>
										<?php if ( wpstarter_excerpt( 10, $id ) ) : ?>
											<p class='widget-popular-posts__excerpt'><?php echo wpstarter_excerpt( 10, $id ); ?></p>
										<?php endif; ?>
										<div class='widget-popular-posts__details-container'>
											<span class='widget-popular-posts__views-icon fas fa-eye' aria-hidden='true'></span>
											<span class='widget-popular-posts__views'>
												<?php echo esc_html( sprintf(
													// translators: %s: Views number ( First is text to be used if the number is singular, second is text to be used if the number is plural ).
													_n( '%s View', '%s Views', get_post_meta( $id, 'post_views_count', true ), 'TRANSLATE' ), get_post_meta( $id, 'post_views_count', true )
												) ); ?>,
											</span>
											<span class='widget-popular-posts__comments'>
												<span><?php echo esc_html( sprintf(
													// translators: %s: Comments number ( First is text to be used if the number is singular, second is text to be used if the number is plural ).
													_n( '%s Comment', '%s Comments', get_comments_number(), 'TRANSLATE' ), get_comments_number()
												) ); ?></span>
											</span>
										</div>
									</div>

								</a>
							</li>

							<?php if ( $i === 10 ) :
								break;
							else :
								$i++;
							endif;

						endforeach;

						wp_reset_postdata(); ?>

					</ul>

				<?php endif; ?>

			</div>

			<?php echo $args['after_widget'];

		}

		/**
		 * Outputs the options form on admin
		 *
		 * @param array $instance The widget options
		 */
		public function form( $instance ) {

			$defaults = [
				'title'      => '',
				'posts_num'  => 5,
				'sort_by'    => '',
				'time_range' => '',
			];

			// Parse current settings with defaults.
			extract( wp_parse_args( ( array ) $instance, $defaults ) ); ?>

			<div class='media-widget-control'>

				<p>
					<label for='<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>'><?php esc_html_e( 'Title:', 'TRANSLATE' ); ?></label> 
					<input class='widefat' id='<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>' name='<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>' type='text' value='<?php echo esc_attr( $title ); ?>' />
				</p>

				<p>
					<label for='<?php echo esc_attr( $this->get_field_id( 'posts_num' ) ); ?>'><?php esc_html_e( 'Number of posts to show ( maxiumum 10 ):', 'TRANSLATE' ); ?></label> 
					<input class='widefat' id='<?php echo esc_attr( $this->get_field_id( 'posts_num' ) ); ?>' name='<?php echo esc_attr( $this->get_field_name( 'posts_num' ) ); ?>' type='number' min='0' max='10' value='<?php echo esc_attr( $posts_num ); ?>' />
				</p>

				<p>
					<label for='<?php echo esc_attr( $this->get_field_id( 'sort_by' ) ); ?>'><?php esc_html_e( 'Sort posts by:', 'TRANSLATE' ); ?></label>
					<select name='<?php echo esc_attr( $this->get_field_name( 'sort_by' ) ); ?>' id='<?php echo esc_attr( $this->get_field_id( 'sort_by' ) ); ?>' class='widefat'>

						<?php $options = [
							'views_count' => esc_html__( 'Number of post views', 'TRANSLATE' ),
							'comment_count' => esc_html__( 'Number of post comments', 'TRANSLATE' ),
						];

						foreach ( $options as $key => $name ) :
							echo '<option value="' . esc_attr( $key ) . '" id="' . esc_attr( $key ) . '" '. selected( $sort_by, $key, false ) . '>'. esc_html( $name ) . '</option>';
						endforeach; ?>

					</select>
				</p>

				<p>
					<label for='<?php echo esc_attr( $this->get_field_id( 'time_range' ) ); ?>'><?php esc_html_e( 'Time range:', 'TRANSLATE' ); ?></label>
					<select name='<?php echo esc_attr( $this->get_field_name( 'time_range' ) ); ?>' id='<?php echo esc_attr( $this->get_field_id( 'time_range' ) ); ?>' class='widefat'>

						<?php $options = [
							''             => esc_html__( 'All the time', 'TRANSLATE' ),
							'2_years_ago'  => esc_html__( '2 years', 'TRANSLATE' ),
							'1_year_ago'   => esc_html__( 'Year', 'TRANSLATE' ),
							'6_months_ago' => esc_html__( '6 months', 'TRANSLATE' ),
							'3_months_ago' => esc_html__( '3 months', 'TRANSLATE' ),
							'1_month_ago'  => esc_html__( 'Month', 'TRANSLATE' ),
							'2_weeks_ago'  => esc_html__( '2 weeks', 'TRANSLATE' ),
							'1_week_ago'   => esc_html__( 'Week', 'TRANSLATE' ),
						];

						foreach ( $options as $key => $name ) :
							echo '<option value="' . esc_attr( $key ) . '" id="' . esc_attr( $key ) . '" '. selected( $time_range, $key, false ) . '>'. esc_html( $name ) . '</option>';
						endforeach; ?>

					</select>
				</p>

			</div>

		<?php }

		/**
		 * Processing widget options on save
		 *
		 * @param array $new_instance The new options
		 * @param array $old_instance The previous options
		 *
		 * @return array
		 */
		public function update( $new_instance, $old_instance ) {

			$settings = [
				'title',
				'posts_num',
				'sort_by',
				'time_range'
			];

			$instance = $old_instance;

			foreach ( $settings as $setting ) :

				$instance[ $setting ] = isset( $new_instance[ $setting ] ) ? wp_strip_all_tags( $new_instance[ $setting ] ) : '';

			endforeach;

			return $instance;

		}

	}

	// Get instance.
	Wpstarter_Widgets_Popular_Posts::get_instance();

endif;
