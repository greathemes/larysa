<?php
/**
 * Helper functions & filters.
 *
 * @since 1.0.0
 *
 * @package wpstarter
 */

if ( ! function_exists( 'wpstarter_set_post_views' ) ) :

	/**
	 * Sets the number of visits to the post.
	 */
	function wpstarter_set_post_views( $post_id ) {

			$count_key = 'post_views_count';
			$count     = get_post_meta( $post_id, $count_key, true );

			if ( $count === '' ) :

				$count = 0;
				delete_post_meta( $post_id, $count_key );
				add_post_meta( $post_id, $count_key, '1' );

			else :

				$count++;
				update_post_meta( $post_id, $count_key, $count );

			endif;

	}

endif;