<?php
/**
 * Register a widget 'Recent Portfolio'.
 *
 * @since 1.0.0
 *
 * @link http://codex.wordpress.org/Function_Reference/register_post_type
 * @package wpstarter
 */

if ( ! class_exists( 'Wpstarter_Widgets_Latest_Albums' ) && class_exists( 'WP_Widget' ) ) :

	/**
	 * Wpstarter_Widgets_Latest_Albums.
	 */
	class Wpstarter_Widgets_Latest_Albums extends WP_Widget {

		/**
		 * Instance.
		 *
		 * @access private
		 * @var object Class object.
		 */
		private static $instance;

		/**
		 * Initiator.
		 *
		 * @return object initialized object of class.
		 */
		public static function get_instance() {

			if ( ! isset( self::$instance ) ) :

				self::$instance = new self();

			endif;

			return self::$instance;

		}

		/**
		 * Constructor.
		 */
		public function __construct() {

			$args = [
				'classname'   => 'widget-latest-albums',
				'description' => esc_html__( 'Your last albums.', 'TRANSLATE' ),
			];

			parent::__construct( 'wpstarter_latest_albums', esc_html__( 'Latest Albums (Wpstarter)', 'TRANSLATE' ), $args );

			add_action( 'widgets_init', [ $this, 'register' ] );

		}

		/**
		 * Register widget.
		 */
		public function register() {

			register_widget( 'Wpstarter_Widgets_Latest_Albums' );

		}

		/**
		 * Outputs the content of the widget.
		 *
		 * @param array $args
		 * @param array $instance
		 */
		public function widget( $args, $instance ) {

			extract( $args );

			// Check the widget options.
			$title     = isset( $instance['title'] ) && ! empty( $instance['title'] ) ? apply_filters( 'widget_title', $instance['title'] ) : '';
			$posts_num = isset( $instance['posts_num'] ) && ! empty( $instance['posts_num'] ) ? $instance['posts_num'] : 5;

			echo $args['before_widget'];

			echo ( ! empty( $title ) ) ? $args['before_title'] . $title . $args['after_title'] : ''; ?>

			<ul class='widget-latest-albums__list widget-latest-albums__list--unstyled'>

				<?php $query = new WP_Query(
					[
						'post_type'      => 'portfolio',
						'posts_per_page' => $posts_num,
					]
				);

				if ( $query->have_posts() ) :

					while ( $query->have_posts() ) :

						$query->the_post();

						wpstarter_portfolio_item_content( get_the_id(), [
							'parent_class' => 'widget-latest-albums',
							'img_class'    => 'widget-latest-albums__img widget-latest-albums__img--covered',
						] );

					endwhile;

					wp_reset_postdata();

				endif; ?>

			</ul>

			<?php echo $args['after_widget'];

		}

		/**
		 * Outputs the options form on admin
		 *
		 * @param array $instance The widget options
		 */
		public function form( $instance ) {

			$defaults = [
				'title'     => '',
				'posts_num' => 5,
			];

			// Parse current settings with defaults.
			extract( wp_parse_args( ( array ) $instance, $defaults ) ); ?>

			<div class='media-widget-control'>

				<p>
					<label for='<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>'><?php esc_html_e( 'Title:', 'TRANSLATE' ); ?></label> 
					<input class='widefat' id='<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>' name='<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>' type='text' value='<?php echo esc_attr( $title ); ?>' />
				</p>

				<p>
					<label for='<?php echo esc_attr( $this->get_field_id( 'posts_num' ) ); ?>'><?php esc_html_e( 'Number of posts to show:', 'TRANSLATE' ); ?></label> 
					<input class='widefat' id='<?php echo esc_attr( $this->get_field_id( 'posts_num' ) ); ?>' name='<?php echo esc_attr( $this->get_field_name( 'posts_num' ) ); ?>' type='number' min='0' value='<?php echo esc_attr( $posts_num ); ?>' />
				</p>

			</div>

		<?php }

		/**
		 * Processing widget options on save
		 *
		 * @param array $new_instance The new options
		 * @param array $old_instance The previous options
		 *
		 * @return array
		 */
		public function update( $new_instance, $old_instance ) {

			$settings = [ 'title', 'posts_num' ];
			$instance = $old_instance;

			foreach ( $settings as $setting ) :

				$instance[ $setting ] = isset( $new_instance[ $setting ] ) ? wp_strip_all_tags( $new_instance[ $setting ] ) : '';

			endforeach;

			return $instance;

		}

	}

	// Get instance.
	Wpstarter_Widgets_Latest_Albums::get_instance();

endif;
