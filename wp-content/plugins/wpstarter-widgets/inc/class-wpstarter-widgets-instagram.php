<?php
/**
 * Register a widget 'Instagram'.
 *
 * @since 1.0.0
 *
 * @link http://codex.wordpress.org/Function_Reference/register_post_type
 * @package wpstarter
 */

if ( ! class_exists( 'Wpstarter_Widgets_Instagram' ) && class_exists( 'WP_Widget' ) ) :

	/**
	 * Wpstarter_Widgets_Instagram.
	 */
	class Wpstarter_Widgets_Instagram extends WP_Widget {

		/**
		 * Instance.
		 *
		 * @access private
		 * @var object Class object.
		 */
		private static $instance;

		/**
		 * Initiator.
		 *
		 * @return object initialized object of class.
		 */
		public static function get_instance() {

			if ( ! isset( self::$instance ) ) :

				self::$instance = new self();

			endif;

			return self::$instance;

		}

		/**
		 * Constructor.
		 */
		public function __construct() {

			$args = [
				'classname'   => 'widget-instagram',
				'description' => esc_html__( 'Your photos from Instagram.', 'TRANSLATE' ),
			];

			parent::__construct( 'wpstarter_instagram', esc_html__( 'Instagram (Wpstarter)', 'TRANSLATE' ), $args );

			add_action( 'widgets_init', [ $this, 'register' ] );

		}

		/**
		 * Register widget.
		 */
		public function register() {

			register_widget( 'Wpstarter_Widgets_Instagram' );

		}

		/**
		 * Outputs the content of the widget.
		 *
		 * @param array $args
		 * @param array $instance
		 */
		public function widget( $args, $instance ) {

			extract( $args );

			// Check the widget options.
			$title   = isset( $instance['title'] ) ? apply_filters( 'widget_title', $instance['title'] ) : '';
			$token   = isset( $instance['token'] ) ? $instance['token'] : '';
			$count   = isset( $instance['count'] ) && ! empty( $instance['count'] ) ? $instance['count'] : 12;
			$new_tab = isset( $instance['new_tab'] ) && ! empty( $instance['new_tab'] ) ? $instance['new_tab'] : '';
			$columns = isset( $instance['columns'] ) && ! empty( $instance['columns'] ) ? $instance['columns'] : '4';

			echo $args['before_widget'];

			echo ( ! empty( $title ) ) ? $args['before_title'] . $title . $args['after_title'] : ''; ?>

			<div class='widget-instagram'>

				<?php if ( $token ) :

					$transient = 'instagram_widget_feed' . $this->id;

					if ( false === ( $feed = get_transient( $transient ) ) ) :

						$remote = wp_remote_get( 'https://api.instagram.com/v1/users/self/media/recent/?access_token=' . $token . '&count=' . $count );

						if ( $remote['response']['code'] === 200 ) :

							$body = json_decode( $remote['body'] );
							$feed = $body->data;

							set_transient( $transient, $feed, 60 * 60 );

						endif;

					endif;

					if ( isset( $feed ) ) : ?>

							<div class='widget-instagram__gallery widget-instagram__gallery--col-<?php echo esc_attr( $columns ); ?>'>
								<?php foreach ( $feed as $feed_item ) : ?>
									<a href='<?php echo esc_url( $feed_item->link ); ?>' class='<?php echo esc_attr( 'widget-instagram__item' ); ?>' id='<?php echo esc_attr( 'media-' . $feed_item->id ); ?>' <?php echo esc_attr( $new_tab ? 'target="_blank"' : '' ); ?>>
										<img class='<?php echo esc_attr( 'widget-instagram__img widget-instagram__img--covered widget-instagram__img--zoom-animation' ); ?>' src='<?php echo esc_url( $feed_item->images->low_resolution->url ); ?>' title='<?php echo esc_attr( $feed_item->caption->text ); ?>'>
									</a>
								<?php endforeach; ?>
							</div>

					<?php endif; ?>

				<?php endif; ?>

			</div>

			<?php echo $args['after_widget'];

		}

		/**
		 * Outputs the options form on admin.
		 *
		 * @param array $instance The widget options
		 */
		public function form( $instance ) {

			$defaults = [
				'title'            => '',
				'token'            => '',
				'count'            => 12,
				'new_tab'          => '',
				'columns'          => 4,
			];

			// Parse current settings with defaults.
			extract( wp_parse_args( ( array ) $instance, $defaults ) );

			$id = $this->id; ?>

			<div class='media-widget-control'>

				<p>
					<label for='<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>'><?php esc_html_e( 'Title:', 'TRANSLATE' ); ?></label> 
					<input class='widefat' id='<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>' name='<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>' type='text' value='<?php echo esc_attr( $title ); ?>' />
				</p>

				<p>
					<p class='small'><?php esc_html_e( 'Thanks to the Access Token, we can send a request to Instagram and securely retrieve our photos. This is required to show your photos.', 'TRANSLATE' ); ?></p>
					<p class='small'>
						<?php printf(
							// translators: %1$s: Start of the html link tag.
							// translators: %2$s: End of the html link tag.
							esc_html__( '%1$sClick here%2$s to generate an Instagram Access Token and then paste it in the field below.', 'TRANSLATE' ), '<a href="https://greathemes.com/get-instagram-token/" target="_blank">', '</a>'
						); ?>
					</p>
					<label for='<?php echo esc_attr( $this->get_field_id( 'token' ) ); ?>'><?php esc_html_e( 'Token:', 'TRANSLATE' ); ?></label> 
					<input class='widefat' id='<?php echo esc_attr( $this->get_field_id( 'token' ) ); ?>' name='<?php echo esc_attr( $this->get_field_name( 'token' ) ); ?>' type='text' value='<?php echo esc_attr( $token ); ?>' />
				</p>

				<p>
					<label for='<?php echo esc_attr( $this->get_field_id( 'count' ) ); ?>'><?php esc_html_e( 'Number of images to show (max 20):', 'TRANSLATE' ); ?></label> 
					<input class='widefat' id='<?php echo esc_attr( $this->get_field_id( 'count' ) ); ?>' name='<?php echo esc_attr( $this->get_field_name( 'count' ) ); ?>' type='number' min='0' max='20' value='<?php echo esc_attr( $count ); ?>' />
				</p>

				<p>
					<input id='<?php echo esc_attr( $this->get_field_id( 'new_tab' ) ); ?>' name='<?php echo esc_attr( $this->get_field_name( 'new_tab' ) ); ?>' type='checkbox' value='1' <?php checked( '1', $new_tab ); ?> />
					<label for='<?php echo esc_attr( $this->get_field_id( 'new_tab' ) ); ?>'><?php esc_html_e( "After clicking on Instagram's image or username link, open the website in a new window.", 'TRANSLATE' ); ?></label> 
				</p>

				<p>
					<label for='<?php echo esc_attr( $this->get_field_id( 'columns' ) ); ?>'><?php esc_html_e( 'Columns:', 'TRANSLATE' ); ?></label>
					<select name='<?php echo esc_attr( $this->get_field_name( 'columns' ) ); ?>' id='<?php echo esc_attr( $this->get_field_id( 'columns' ) ); ?>' class='widefat'>

						<?php $options = [
							'1' => esc_html__( '1 column', 'TRANSLATE' ),
							'2' => esc_html__( '2 columns', 'TRANSLATE' ),
							'3' => esc_html__( '3 columns', 'TRANSLATE' ),
							'4' => esc_html__( '4 columns', 'TRANSLATE' ),
						];

						foreach ( $options as $key => $name ) :

							echo '<option value="' . esc_attr( $key ) . '" id="' . esc_attr( $key ) . '" '. selected( $columns, $key, false ) . '>'. esc_html( $name ) . '</option>';

						endforeach; ?>

					</select>
				</p>

			</div>

		<?php }

		/**
		 * Processing widget options on save.
		 *
		 * @param array $new_instance The new options
		 * @param array $old_instance The previous options
		 *
		 * @return array
		 */
		public function update( $new_instance, $old_instance ) {

			if ( ( $new_instance['token'] !== $old_instance['token'] ) || ( $new_instance['count'] !== $old_instance['count'] ) ) :
				delete_transient( 'instagram_widget_feed' . $this->id );
			endif;

			$settings  = [ 'title', 'token', 'count', 'new_tab', 'columns' ];
			$instance  = $old_instance;

			foreach ( $settings as $setting ) :
				$instance[ $setting ] = isset( $new_instance[ $setting ] ) ? wp_strip_all_tags( $new_instance[ $setting ] ) : '';
			endforeach;

			return $instance;

		}

	}

	// Get instance.
	Wpstarter_Widgets_Instagram::get_instance();

endif;
