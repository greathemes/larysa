<?php
/**
 * Register a widget 'Recent Posts'.
 *
 * @since 1.0.0
 *
 * @link http://codex.wordpress.org/Function_Reference/register_post_type
 * @package wpstarter
 */

if ( ! class_exists( 'Wpstarter_Widgets_Latest_Posts' ) && class_exists( 'WP_Widget' ) ) :

	/**
	 * Wpstarter_Widgets_Latest_Posts.
	 */
	class Wpstarter_Widgets_Latest_Posts extends WP_Widget {

		/**
		 * Instance.
		 *
		 * @access private
		 * @var object Class object.
		 */
		private static $instance;

		/**
		 * Initiator.
		 *
		 * @return object initialized object of class.
		 */
		public static function get_instance() {

			if ( ! isset( self::$instance ) ) :

				self::$instance = new self();

			endif;

			return self::$instance;

		}

		/**
		 * Constructor.
		 */
		public function __construct() {

			$args = [
				'classname'   => 'widget-latest-posts',
				'description' => esc_html__( 'Your site’s most latest posts with thumbnail.', 'TRANSLATE' ),
			];

			parent::__construct( 'wpstarter_latest_posts', esc_html__( 'Latest Posts With Thumbnail (Wpstarter)', 'TRANSLATE' ), $args );

			add_action( 'widgets_init', [ $this, 'register' ] );

		}

		/**
		 * Register widget.
		 */
		public function register() {

			register_widget( 'Wpstarter_Widgets_Latest_Posts' );

		}

		/**
		 * Outputs the content of the widget.
		 *
		 * @param array $args
		 * @param array $instance
		 */
		public function widget( $args, $instance ) {

			extract( $args );

			// Check the widget options.
			$title     = isset( $instance['title'] ) && ! empty( $instance['title'] ) ? apply_filters( 'widget_title', $instance['title'] ) : '';
			$posts_num = isset( $instance['posts_num'] ) && ! empty( $instance['posts_num'] ) ?$instance['posts_num'] : 5;

			echo $args['before_widget'];

			echo ( ! empty( $title ) ) ? $args['before_title'] . $title . $args['after_title'] : ''; ?>

			<div class='widget-latest-posts'>

				<?php global $post;

				$posts = get_posts(
					[
						'posts_per_page' => $posts_num,
						'order'          => 'DESC',
						'post_type'      => 'post',
					]
				);

				if ( $posts ) : ?>

					<ul class='widget-latest-posts__list widget-latest-posts__list--unstyled'>

						<?php foreach ( $posts as $post ) :

							setup_postdata( $post );

							$id          = $post->ID;
							$has_excerpt = ( wpstarter_excerpt( 11, $id ) ) ? 'has-excerpt' : ''; ?>

							<li class='widget-latest-posts__item <?php echo esc_attr( $has_excerpt ); ?>'>
								<a class='widget-latest-posts__link' href='<?php echo esc_url( get_the_permalink( $id ) ); ?>'>

									<div class='widget-latest-posts__img-container'>
										<?php if ( has_post_thumbnail( $id ) ) :
											wpstarter_post_thumbnail( get_post_thumbnail_id( $id ),
												[
													'size'  => 'wpstarter_widget',
													'sizes' => [ 'wpstarter_widget', 'wpstarter_widget_retina' ],
													'class' => 'widget-latest-posts__img widget-latest-posts__img--covered'
												]
											);		
										endif; ?>
									</div>

									<div class='widget-latest-posts__content-container'>
										<?php if ( 'gallery' === get_post_format( $id ) ) : ?>
											<span class='<?php echo esc_attr( "widget-latest-posts__gallery-icon fa fa-images" ); ?>'></span>
										<?php endif; ?>
										<?php if ( get_the_title( $id ) ) : ?>
											<h4 class='widget-latest-posts__title'><?php echo esc_html( $post->post_title ); ?></h4>
										<?php endif; ?>
										<?php if ( $has_excerpt ) : ?>
											<p class='widget-latest-posts__excerpt'><?php echo esc_html( wpstarter_excerpt( 10, $id ) ); ?></p>
										<?php endif; ?>
										<span class='widget-latest-posts__date'>
											<span class='widget-latest-posts__date-icon fas fa-calendar-alt' aria-hidden='true'></span>
											<span class='widget-latest-posts__date-text'><?php printf(
												// translators: %s: Date of the post.
												esc_html__( 'Date: %s', 'TRANSLATE' ), get_the_date()
											); ?></span>
										</span>

									</div>

								</a>
							</li>

						<?php endforeach;

						wp_reset_postdata(); ?>

					</ul>

				<?php endif; ?>

			</div>

			<?php echo $args['after_widget'];

		}

		/**
		 * Outputs the options form on admin
		 *
		 * @param array $instance The widget options
		 */
		public function form( $instance ) {

			$defaults = [
				'title'     => '',
				'posts_num' => 5,
			];

			// Parse current settings with defaults.
			extract( wp_parse_args( ( array ) $instance, $defaults ) ); ?>

			<div class='media-widget-control'>

				<p>
					<label for='<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>'><?php esc_html_e( 'Title:', 'TRANSLATE' ); ?></label> 
					<input class='widefat' id='<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>' name='<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>' type='text' value='<?php echo esc_attr( $title ); ?>' />
				</p>

				<p>
					<label for='<?php echo esc_attr( $this->get_field_id( 'posts_num' ) ); ?>'><?php esc_html_e( 'Number of posts to show:', 'TRANSLATE' ); ?></label> 
					<input class='widefat' id='<?php echo esc_attr( $this->get_field_id( 'posts_num' ) ); ?>' name='<?php echo esc_attr( $this->get_field_name( 'posts_num' ) ); ?>' type='number' min='0' value='<?php echo esc_attr( $posts_num ); ?>' />
				</p>

			</div>

		<?php }

		/**
		 * Processing widget options on save
		 *
		 * @param array $new_instance The new options
		 * @param array $old_instance The previous options
		 *
		 * @return array
		 */
		public function update( $new_instance, $old_instance ) {

			$settings = [ 'title', 'posts_num' ];
			$instance = $old_instance;

			foreach ( $settings as $setting ) :

				$instance[ $setting ] = isset( $new_instance[ $setting ] ) ? wp_strip_all_tags( $new_instance[ $setting ] ) : '';

			endforeach;

			return $instance;

		}

	}

	// Get instance.
	Wpstarter_Widgets_Latest_Posts::get_instance();

endif;
