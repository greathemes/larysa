<?php
/**
 * Plugin uninstaller.
 *
 * @since 1.0.0
 *
 * @package wpstarter
 */

if ( ! defined( 'WP_UNINSTALL_PLUGIN' ) ) : die; endif;

delete_option( 'widget_wpstarter_about' );
delete_option( 'widget_wpstarter_thumbnail_recent_posts' );
delete_option( 'widget_wpstarter_popular_posts' );
delete_option( 'widget_wpstarter_popular_portfolio' );
