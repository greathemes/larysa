<?php
/**
 * Plugin Name: Wpstarter Fields
 * Plugin URI: https://wpstarter.greathemes.com/
 * Description: Custom fields for Wpstarter theme based on Carbon Fields.
 * Version: 1.0.0
 * Author: Greathemes
 * Author URI: https://greathemes.com/
 * License: GNU General Public License v2.
 * License URI: http://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain: wpstarter-fields
 * Domain Path: /languages
 *
 * @see https://carbonfields.net/
 * @package wpstarter
 * @author Greathemes
 * @license GPL-2.0
 */

if ( ! defined( 'ABSPATH' ) ) : exit; endif;

// Constants.
define( 'WPSTARTER_FIELDS_DIR', trailingslashit( plugin_dir_path( __FILE__ ) ) );
define( 'WPSTARTER_FIELDS_INCLUDES', trailingslashit( plugin_dir_path( __FILE__ ) . '/inc' ) );

// Init.
require_once WPSTARTER_FIELDS_DIR . 'class-wpstarter-fields-init.php';

// Includes.
require_once WPSTARTER_FIELDS_INCLUDES . 'class-wpstarter-fields-single-post.php';
require_once WPSTARTER_FIELDS_INCLUDES . 'class-wpstarter-fields-single-portfolio.php';
