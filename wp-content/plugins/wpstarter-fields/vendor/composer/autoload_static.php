<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit11286491bb6ea93cb2d8673bdc855b94
{
    public static $prefixLengthsPsr4 = array (
        'C' => 
        array (
            'Carbon_Fields\\' => 14,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Carbon_Fields\\' => 
        array (
            0 => __DIR__ . '/..' . '/htmlburger/carbon-fields/core',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit11286491bb6ea93cb2d8673bdc855b94::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit11286491bb6ea93cb2d8673bdc855b94::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
