<?php
/**
 * Custom fields for single post.
 *
 * @since 1.0.0
 *
 * @package wpstarter
 */

use Carbon_Fields\Container;
use Carbon_Fields\Field;

if ( ! class_exists( 'Wpstarter_Fields_Single_Post' ) ) :

	/**
	 * Wpstarter_Fields_Single_Post.
	 */
	class Wpstarter_Fields_Single_Post extends WP_Widget {

		/**
		 * Instance.
		 *
		 * @access private
		 * @var object Class object.
		 */
		private static $instance;

		/**
		 * Initiator.
		 *
		 * @return object initialized object of class.
		 */
		public static function get_instance() {

			if ( ! isset( self::$instance ) ) :

				self::$instance = new self();

			endif;

			return self::$instance;

		}

		/**
		 * Constructor.
		 */
		public function __construct() {

			add_action( 'carbon_fields_register_fields', [ $this, 'add_images_to_gallery_post_format' ] );

		}

		public function add_images_to_gallery_post_format() {

			Container::make( 'post_meta', esc_html__( 'Photos of the gallery', 'TRANSLATE' ) )
				->where( 'post_type', '=', 'post' )
				->where( 'post_format', '=', 'gallery' )
				->set_context( 'side' )
				->add_fields( [
					Field::make( 'media_gallery', 'wpstarter_fields_post_format_gallery', esc_html__( 'Choose at least two photos to create a gallery.', 'TRANSLATE' ) )
					->set_type( 'image' )
					->set_duplicates_allowed( false )
				] );

		}

	}

	// Get instance.
	Wpstarter_Fields_Single_Post::get_instance();

endif;
