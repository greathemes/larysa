<?php
/**
 * Custom fields for single portfolio.
 *
 * @since 1.0.0
 *
 * @package wpstarter
 */

use Carbon_Fields\Container;
use Carbon_Fields\Field;

if ( ! class_exists( 'Wpstarter_Fields_Single_Portfolio' ) ) :

	/**
	 * Wpstarter_Fields_Single_Portfolio.
	 */
	class Wpstarter_Fields_Single_Portfolio extends WP_Widget {

		/**
		 * Instance.
		 *
		 * @access private
		 * @var object Class object.
		 */
		private static $instance;

		/**
		 * Initiator.
		 *
		 * @return object initialized object of class.
		 */
		public static function get_instance() {

			if ( ! isset( self::$instance ) ) :

				self::$instance = new self();

			endif;

			return self::$instance;

		}

		/**
		 * Constructor.
		 */
		public function __construct() {

			add_action( 'carbon_fields_register_fields', [ $this, 'add_textarea' ] );
			add_action( 'carbon_fields_register_fields', [ $this, 'add_media_gallery' ] );
			add_action( 'carbon_fields_register_fields', [ $this, 'add_layouts' ] );

		}

		public function add_textarea() {

			Container::make( 'post_meta', esc_html__( 'Content', 'TRANSLATE' ) )
				->where( 'post_type', '=', 'portfolio' )
				->add_fields( [
					Field::make( 'textarea', 'wpstarter_fields_single_portfolio_content', '' )
					->set_help_text( esc_html__( 'Content above photos. You can describe interesting information related to the album.', 'TRANSLATE' ) ),
				] );

		}

		public function add_media_gallery() {
					Container::make( 'post_meta', esc_html__( 'Photos of the gallery', 'TRANSLATE' ) )
				->where( 'post_type', '=', 'portfolio' )
				->add_fields( [
					Field::make( 'media_gallery', 'wpstarter_fields_single_portfolio_gallery', '' )
					->set_help_text( esc_html__( 'Your photo gallery. Choose the best!', 'TRANSLATE' ) )
					->set_type( 'image' )
					->set_duplicates_allowed( false ),
				] );

		}

		public function add_layouts() {

			Container::make( 'post_meta', esc_html__( 'Gallery layout', 'TRANSLATE' ) )
				->where( 'post_type', '=', 'portfolio' )
				->add_fields( [
					Field::make( 'radio', 'wpstarter_fields_single_portfolio_layout', '' )
					->set_help_text( esc_html__( 'The layout of your photo gallery.', 'TRANSLATE' ) )
					->set_options( [
						''              => esc_html__( 'Default', 'TRANSLATE' ),
						'grid-2-col'    => esc_html__( 'Grid 2 columns', 'TRANSLATE' ),
						'grid-3-col'    => esc_html__( 'Grid 3 columns', 'TRANSLATE' ),
						'masonry-2-col' => esc_html__( 'Masonry 2 columns', 'TRANSLATE' ),
						'masonry-3-col' => esc_html__( 'Masonry 3 columns', 'TRANSLATE' ),
					] ),
				] );

		}

	}

	// Get instance.
	Wpstarter_Fields_Single_Portfolio::get_instance();

endif;
