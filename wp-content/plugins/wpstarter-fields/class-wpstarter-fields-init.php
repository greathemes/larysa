<?php
/**
 * Custom fields initializer.
 *
 * @since 1.0.0
 *
 * @package wpstarter
 */

if ( ! class_exists( 'Wpstarter_Fields_Init' ) ) :

	/**
	 * Wpstarter_Fields_Init.
	 */
	class Wpstarter_Fields_Init {

		/**
		 * Instance.
		 *
		 * @access private
		 * @var object Class object.
		 */
		private static $instance;

		/**
		 * Initiator.
		 *
		 * @return object initialized object of class.
		 */
		public static function get_instance() {

			if ( ! isset( self::$instance ) ) :

				self::$instance = new self();

			endif;

			return self::$instance;

		}

		/**
		 * Constructor.
		 */
		public function __construct() {

			add_action( 'plugins_loaded', [ $this, 'load_plugin_lang' ] );
			add_action( 'activated_plugin', 'flush_rewrite_rules'  );
			add_action( 'deactivated_plugin', 'flush_rewrite_rules' );
			add_action( 'after_setup_theme', [ $this, 'crb_load' ] );

			add_filter( 'ajax_query_attachments_args', function ( $query ) {

				if ( isset( $query['post__in'] ) && is_array( $query['post__in'] ) ) :
					$query['posts_per_page'] = count( $query['post__in'] );
				endif;

				return $query;

			} );

		}

		public function crb_load() {

			require_once( 'vendor/autoload.php' );
			\Carbon_Fields\Carbon_Fields::boot();

		}

		/**
		 * Load text domain.
		 */
		public function load_plugin_lang() {

			load_plugin_textdomain( 'TRANSLATE', FALSE, basename( dirname( __FILE__ ) ) . '/languages/' );

		}

	}

	// Get instance.
	Wpstarter_Fields_Init::get_instance();

endif;
